@startuml
    namespace it.tidalwave.accounting.model {

        interface Invoice [[Invoice.html]] {
            {abstract} +findJobEvents(): Finder<JobEvent>
            {abstract} +toBuilder(): Builder
        }

        class it.tidalwave.accounting.model.Invoice.Builder [[Invoice.Builder.html]] {
            +Builder()
            +Builder(Callback)
            +with(Builder): Builder
            +create(): Invoice
        }

        interface it.tidalwave.accounting.model.Invoice.Builder.Callback [[Invoice.Builder.Callback.html]] {
            {abstract} +register(Invoice): void
        }

        interface Customer [[Customer.html]] {
            {static} +builder(): Builder
            {abstract} +findProjects(): ProjectFinder
            {abstract} +toBuilder(): Builder
        }

        class it.tidalwave.accounting.model.Customer.Builder [[Customer.Builder.html]] {
            +Builder()
            +Builder(Callback)
            +with(Builder): Builder
            +create(): Customer
        }

        interface it.tidalwave.accounting.model.Customer.Builder.Callback [[Customer.Builder.Callback.html]] {
            {abstract} +register(Customer): void
        }

        class HourlyReport [[HourlyReport.html]] {
            +asString(): String
        }

        interface ProjectRegistry [[ProjectRegistry.html]] {
            {static} +CustomerRegistry: Class<ProjectRegistry>
            {abstract} +findProjects(): ProjectFinder
            {abstract} +findJobEvents(): JobEventFinder
            {abstract} +addProject(): Builder
        }

        interface it.tidalwave.accounting.model.ProjectRegistry.ProjectFinder [[ProjectRegistry.ProjectFinder.html]] {
            {abstract} +withId(Id): ProjectFinder
        }

        interface it.tidalwave.accounting.model.ProjectRegistry.JobEventFinder [[ProjectRegistry.JobEventFinder.html]] {
            {abstract} +withId(Id): JobEventFinder
            {abstract} +getDuration(): Duration
            {abstract} +getEarnings(): Money
        }

        interface HourlyReportGenerator [[HourlyReportGenerator.html]] {
            {static} +_HourlyReportGenerator_: Class<HourlyReportGenerator>
            {abstract} +createReport(): HourlyReport
        }

        interface JobEventGroup [[JobEventGroup.html]] {
            {abstract} +findChildren(): JobEventFinder
        }

        interface Project [[Project.html]] {
            {static} +builder(): Builder
            {abstract} +getCustomer(): Customer
            {abstract} +findChildren(): JobEventFinder
            {abstract} +toBuilder(): Builder
        }

        enum it.tidalwave.accounting.model.Project.Status [[Project.Status.html]] {
            {static} +OPEN
            {static} +CLOSED
        }

        class it.tidalwave.accounting.model.Project.Builder [[Project.Builder.html]] {
            +Builder()
            +Builder(Callback)
            +with(Builder): Builder
            +create(): Project
        }

        interface it.tidalwave.accounting.model.Project.Builder.Callback [[Project.Builder.Callback.html]] {
            {abstract} +register(Project): void
        }

        interface JobEvent [[JobEvent.html]] {
            {static} +builder(): Builder
            {abstract} +toBuilder(): Builder
        }

        enum it.tidalwave.accounting.model.JobEvent.Type [[JobEvent.Type.html]] {
            {static} +TIMED
            {static} +FLAT
        }

        class it.tidalwave.accounting.model.JobEvent.Builder [[JobEvent.Builder.html]] {
            +create(): JobEvent
        }

        interface CustomerRegistry [[CustomerRegistry.html]] {
            {abstract} +findCustomers(): Finder
            {abstract} +addCustomer(): Builder
        }

        interface it.tidalwave.accounting.model.CustomerRegistry.Finder [[CustomerRegistry.Finder.html]] {
            {abstract} +withId(Id): Finder
        }

        interface JobEventRegistry [[JobEventRegistry.html]] {
            {static} +_JobEventRegistry_: Class<JobEventRegistry>
            {abstract} +findJobEvent(): Finder<JobEvent>
            {abstract} +addJobEvent(): JobEvent
        }

        interface Accounting [[Accounting.html]] {
            {static} +createNew(): Accounting
            {abstract} +getCustomerRegistry(): CustomerRegistry
            {abstract} +getProjectRegistry(): ProjectRegistry
            {abstract} +getInvoiceRegistry(): InvoiceRegistry
        }

        interface InvoiceRegistry [[InvoiceRegistry.html]] {
            {abstract} +findInvoices(): Finder
            {abstract} +addInvoice(): Builder
        }

        interface it.tidalwave.accounting.model.InvoiceRegistry.Finder [[InvoiceRegistry.Finder.html]] {
            {abstract} +withId(Id): Finder
            {abstract} +withProject(Project): Finder
            {abstract} +getEarnings(): Money
        }

        it.tidalwave.role.Identifiable <|-- Invoice
        it.tidalwave.util.As <|-- Invoice
        Invoice +-- it.tidalwave.accounting.model.Invoice.Builder
        it.tidalwave.accounting.model.Invoice.Builder +-- it.tidalwave.accounting.model.Invoice.Builder.Callback
        it.tidalwave.accounting.model.Invoice.Builder.Callback --> it.tidalwave.accounting.model.Invoice.Builder.Callback: DEFAULT
        it.tidalwave.role.Identifiable <|-- Customer
        it.tidalwave.util.As <|-- Customer
        Customer +-- it.tidalwave.accounting.model.Customer.Builder
        it.tidalwave.accounting.model.Customer.Builder +-- it.tidalwave.accounting.model.Customer.Builder.Callback
        it.tidalwave.accounting.model.Customer.Builder.Callback --> it.tidalwave.accounting.model.Customer.Builder.Callback: DEFAULT
        it.tidalwave.util.Finder <|-- it.tidalwave.accounting.model.ProjectRegistry.ProjectFinder
        it.tidalwave.util.spi.ExtendedFinderSupport <|-- it.tidalwave.accounting.model.ProjectRegistry.ProjectFinder
        ProjectRegistry +-- it.tidalwave.accounting.model.ProjectRegistry.ProjectFinder
        it.tidalwave.util.Finder <|-- it.tidalwave.accounting.model.ProjectRegistry.JobEventFinder
        it.tidalwave.util.spi.ExtendedFinderSupport <|-- it.tidalwave.accounting.model.ProjectRegistry.JobEventFinder
        ProjectRegistry +-- it.tidalwave.accounting.model.ProjectRegistry.JobEventFinder
        JobEvent <|-- JobEventGroup
        it.tidalwave.role.SimpleComposite <|-- JobEventGroup
        it.tidalwave.role.SimpleComposite <|-- Project
        it.tidalwave.role.Identifiable <|-- Project
        it.tidalwave.util.As <|-- Project
        Project +-- it.tidalwave.accounting.model.Project.Status
        Project +-- it.tidalwave.accounting.model.Project.Builder
        it.tidalwave.accounting.model.Project.Builder +-- it.tidalwave.accounting.model.Project.Builder.Callback
        it.tidalwave.accounting.model.Project.Builder.Callback --> it.tidalwave.accounting.model.Project.Builder.Callback: DEFAULT
        it.tidalwave.role.Identifiable <|-- JobEvent
        it.tidalwave.util.As <|-- JobEvent
        JobEvent +-- it.tidalwave.accounting.model.JobEvent.Type
        JobEvent +-- it.tidalwave.accounting.model.JobEvent.Builder
        it.tidalwave.util.Finder <|-- it.tidalwave.accounting.model.CustomerRegistry.Finder
        it.tidalwave.util.spi.ExtendedFinderSupport <|-- it.tidalwave.accounting.model.CustomerRegistry.Finder
        CustomerRegistry +-- it.tidalwave.accounting.model.CustomerRegistry.Finder
        it.tidalwave.util.As <|-- Accounting
        it.tidalwave.util.Finder <|-- it.tidalwave.accounting.model.InvoiceRegistry.Finder
        it.tidalwave.util.spi.ExtendedFinderSupport <|-- it.tidalwave.accounting.model.InvoiceRegistry.Finder
        InvoiceRegistry +-- it.tidalwave.accounting.model.InvoiceRegistry.Finder
    }

    namespace it.tidalwave.role {
        interface Identifiable {
            {static} +_Identifiable_: Class<Identifiable>
            {abstract} +getId(): Id
            {static} +of(Id): Identifiable
        }
        interface SimpleComposite<T> {
            {static} +_SimpleComposite_: Class<SimpleComposite>
            {static} +of(Finder<​U>): SimpleComposite<​U>
            {static} +ofCloned(Collection<? extends U>): SimpleComposite<​U>
        }
    }

    namespace it.tidalwave.util {
        interface As {
            {static} +forObject(Object): As
            {static} +forObject(Object, Object): As
            {static} +forObject(Object, Collection<Object>): As
            +as(Class<? extends T>): T
            {abstract} +maybeAs(Class<? extends T>): Optional<T>
            {abstract} +asMany(Class<? extends T>): Collection<T>
            {static} +type(Class<?>): Type<T>
            +as(Type<? extends T>): T
            +maybeAs(Type<? extends T>): Optional<T>
            +asMany(Type<? extends T>): Collection<T>
        }
        interface Finder<T> {
            {abstract} +from(int): Finder<T>
            {abstract} +max(int): Finder<T>
            +withContext(Object): Finder<T>
            +ofType(Class<​U>): Finder<​U>
            +sort(SortCriterion): Finder<T>
            {abstract} +sort(SortCriterion, SortDirection): Finder<T>
            {abstract} +results(): List<T>
            {abstract} +count(): int
            +optionalResult(): Optional<T>
            +optionalFirstResult(): Optional<T>
            +stream(): Stream<T>
            +iterator(): Iterator<T>
            +--result--(): T
            +--firstResult--(): T
            {static} +empty(): Finder<​U>
            {static} +ofCloned(Collection<? extends U>): Finder<​U>
            {static} +ofSupplier(Supplier<? extends Collection<? extends U>>): Finder<​U>
            {static} +ofProvider(BiFunction<Integer, Integer, ? extends Collection<? extends U>>): Finder<​U>
            {static} +mapping(Finder<V>, Function<? super V, ? extends U>): Finder<​U>
        }
    }

    namespace it.tidalwave.util.spi {
        interface ExtendedFinderSupport<T, F extends Finder<T>> {
            {abstract} +from(int): F extends Finder<T>
            {abstract} +max(int): F extends Finder<T>
            {abstract} +sort(SortCriterion): F extends Finder<T>
            {abstract} +sort(SortCriterion, SortDirection): F extends Finder<T>
            {abstract} +withContext(Object): F extends Finder<T>
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
