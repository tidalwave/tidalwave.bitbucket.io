@startuml
    namespace it.tidalwave.accounting.model.impl {

        class InMemoryCustomer [[InMemoryCustomer.html]] {
            +InMemoryCustomer(Builder)
            +findProjects(): ProjectFinder
            +toBuilder(): Builder
        }

        class InMemoryJobEventGroup [[InMemoryJobEventGroup.html]] {
            +InMemoryJobEventGroup(Builder)
            +toBuilder(): Builder
            +getDateTime(): LocalDateTime
            +getEarnings(): Money
            +getDuration(): Duration
            +findChildren(): JobEventFinder
        }

        class InMemoryProject [[InMemoryProject.html]] {
            +InMemoryProject(Builder)
            +findChildren(): JobEventFinder
            +getEarnings(): Money
            +getDuration(): Duration
            +getInvoicedEarnings(): Money
            +toBuilder(): Builder
        }

        class InMemoryInvoiceFinderFromMap [[InMemoryInvoiceFinderFromMap.html]] {
            +InMemoryInvoiceFinderFromMap(Map<Id, InMemoryInvoice>)
            +InMemoryInvoiceFinderFromMap(InMemoryInvoiceFinderFromMap, Object)
            +withProject(Project): Finder
            #computeResults(): List<Invoice>
            +getEarnings(): Money
        }

        class InMemoryProjectRegistry [[InMemoryProjectRegistry.html]] {
            +findProjects(): ProjectFinder
            +findJobEvents(): JobEventFinder
            +addProject(): Builder
        }

        abstract class InMemoryJobEvent [[InMemoryJobEvent.html]] {
            #id: Id
            #name: String
            #description: String
            #InMemoryJobEvent(Builder)
            {static} +builder(): Builder
            {abstract} +getDateTime(): LocalDateTime
            {abstract} +getDuration(): Duration
            {abstract} +getEarnings(): Money
            {abstract} +toBuilder(): Builder
        }

        class InMemoryCustomerRegistry [[InMemoryCustomerRegistry.html]] {
            +findCustomers(): Finder
            +addCustomer(): Builder
        }

        class InMemoryObjectFactory [[InMemoryObjectFactory.html]] {
            +createCustomer(Builder): Customer
            +createInvoice(Builder): Invoice
            +createProject(Builder): Project
            +createJobEvent(Builder): JobEvent
        }

        class InMemoryAccounting [[InMemoryAccounting.html]] {
        }

        class InMemoryInvoice [[InMemoryInvoice.html]] {
            +InMemoryInvoice(Builder)
            +findJobEvents(): Finder<JobEvent>
            +toBuilder(): Builder
        }

        class InMemoryJobEventFinderSupport [[InMemoryJobEventFinderSupport.html]] {
            +InMemoryJobEventFinderSupport(InMemoryJobEventFinderSupport, Object)
            #findById(Id): Optional<JobEvent>
            +getDuration(): Duration
            +getEarnings(): Money
        }

        class InMemoryJobEventFinder [[InMemoryJobEventFinder.html]] {
            +InMemoryJobEventFinder(InMemoryJobEventFinder, Object)
            #findAll(): List<JobEvent>
        }

        class InMemoryInvoiceRegistry [[InMemoryInvoiceRegistry.html]] {
            +findInvoices(): Finder
            +addInvoice(): Builder
        }

        class InMemoryFlatJobEvent [[InMemoryFlatJobEvent.html]] {
            +InMemoryFlatJobEvent(Builder)
            +toBuilder(): Builder
            +getDateTime(): LocalDateTime
            +getDuration(): Duration
        }

        class InMemoryJobEventFinderFromList [[InMemoryJobEventFinderFromList.html]] {
            +InMemoryJobEventFinderFromList()
            +InMemoryJobEventFinderFromList(InMemoryJobEventFinderFromList, Object)
            #findAll(): List<JobEvent>
        }

        class InMemoryTimedJobEvent [[InMemoryTimedJobEvent.html]] {
            +InMemoryTimedJobEvent(Builder)
            +toBuilder(): Builder
            +getDateTime(): LocalDateTime
            +getDuration(): Duration
        }

        it.tidalwave.accounting.model.spi.CustomerSpi <|.. InMemoryCustomer
        InMemoryJobEvent <|-- InMemoryJobEventGroup
        it.tidalwave.accounting.model.spi.JobEventGroupSpi <|.. InMemoryJobEventGroup
        it.tidalwave.accounting.model.spi.ProjectSpi <|.. InMemoryProject
        it.tidalwave.util.spi.FinderWithIdMapSupport <|-- InMemoryInvoiceFinderFromMap
        it.tidalwave.accounting.model.InvoiceRegistry.Finder <|.. InMemoryInvoiceFinderFromMap
        it.tidalwave.accounting.model.ProjectRegistry <|.. InMemoryProjectRegistry
        it.tidalwave.accounting.model.JobEvent <|.. InMemoryJobEvent
        it.tidalwave.accounting.model.CustomerRegistry <|.. InMemoryCustomerRegistry
        it.tidalwave.accounting.model.spi.ObjectFactory <|.. InMemoryObjectFactory
        it.tidalwave.accounting.model.Accounting <|.. InMemoryAccounting
        it.tidalwave.accounting.model.spi.InvoiceSpi <|.. InMemoryInvoice
        it.tidalwave.util.spi.FinderWithIdSupport <|-- InMemoryJobEventFinderSupport
        it.tidalwave.accounting.model.ProjectRegistry.JobEventFinder <|.. InMemoryJobEventFinderSupport
        InMemoryJobEventFinderSupport <|-- InMemoryJobEventFinder
        it.tidalwave.accounting.model.InvoiceRegistry <|.. InMemoryInvoiceRegistry
        InMemoryJobEvent <|-- InMemoryFlatJobEvent
        it.tidalwave.accounting.model.spi.FlatJobEventSpi <|.. InMemoryFlatJobEvent
        InMemoryJobEventFinderSupport <|-- InMemoryJobEventFinderFromList
        InMemoryJobEvent <|-- InMemoryTimedJobEvent
        it.tidalwave.accounting.model.spi.TimedJobEventSpi <|.. InMemoryTimedJobEvent
    }

    namespace it.tidalwave.accounting.model.spi {
        interface CustomerSpi [[http://bluehour.tidalwave.it/bluehour-modules/it-tidalwave-accounting-model/apidocs/it/tidalwave/accounting/model/spi/CustomerSpi.html?is-external=true]] {
            {abstract} +getName(): String
        }
        interface JobEventGroupSpi [[http://bluehour.tidalwave.it/bluehour-modules/it-tidalwave-accounting-model/apidocs/it/tidalwave/accounting/model/spi/JobEventGroupSpi.html?is-external=true]]
        interface ProjectSpi [[http://bluehour.tidalwave.it/bluehour-modules/it-tidalwave-accounting-model/apidocs/it/tidalwave/accounting/model/spi/ProjectSpi.html?is-external=true]] {
            {abstract} +getName(): String
            {abstract} +getNumber(): String
            {abstract} +getNotes(): String
            {abstract} +getStartDate(): LocalDate
            {abstract} +getEndDate(): LocalDate
            {abstract} +getDuration(): Duration
            {abstract} +getStatus(): Status
            {abstract} +getEarnings(): Money
            {abstract} +getBudget(): Money
            {abstract} +getInvoicedEarnings(): Money
            {abstract} +getHourlyRate(): Money
        }
        interface ObjectFactory [[http://bluehour.tidalwave.it/bluehour-modules/it-tidalwave-accounting-model/apidocs/it/tidalwave/accounting/model/spi/ObjectFactory.html?is-external=true]] {
            {static} +getInstance(): ObjectFactory
            {abstract} +createCustomer(Builder): Customer
            {abstract} +createInvoice(Builder): Invoice
            {abstract} +createProject(Builder): Project
            {abstract} +createJobEvent(Builder): JobEvent
        }
        interface InvoiceSpi [[http://bluehour.tidalwave.it/bluehour-modules/it-tidalwave-accounting-model/apidocs/it/tidalwave/accounting/model/spi/InvoiceSpi.html?is-external=true]] {
            {abstract} +getNumber(): String
            {abstract} +getProject(): Project
            {abstract} +getEarnings(): Money
            {abstract} +getTax(): Money
        }
        interface FlatJobEventSpi [[http://bluehour.tidalwave.it/bluehour-modules/it-tidalwave-accounting-model/apidocs/it/tidalwave/accounting/model/spi/FlatJobEventSpi.html?is-external=true]] {
            {abstract} +getDate(): LocalDate
        }
        interface TimedJobEventSpi [[http://bluehour.tidalwave.it/bluehour-modules/it-tidalwave-accounting-model/apidocs/it/tidalwave/accounting/model/spi/TimedJobEventSpi.html?is-external=true]] {
            {abstract} +getStartDateTime(): LocalDateTime
            {abstract} +getEndDateTime(): LocalDateTime
            {abstract} +getDuration(): Duration
            {abstract} +getHourlyRate(): Money
        }
    }

    namespace it.tidalwave.util.spi {
        class FinderWithIdMapSupport<T, I extends T, F extends ExtendedFinderSupport<T, F>> {
        }
        class FinderWithIdSupport<T, I extends T, F extends ExtendedFinderSupport<T, F>> {
        }
    }

    namespace it.tidalwave.accounting.model {
        interface it.tidalwave.accounting.model.InvoiceRegistry.Finder [[http://bluehour.tidalwave.it/bluehour-modules/it-tidalwave-accounting-model/apidocs/it/tidalwave/accounting/model/InvoiceRegistry.Finder.html?is-external=true]] {
            {abstract} +withId(Id): Finder
            {abstract} +withProject(Project): Finder
            {abstract} +getEarnings(): Money
        }
        interface ProjectRegistry [[http://bluehour.tidalwave.it/bluehour-modules/it-tidalwave-accounting-model/apidocs/it/tidalwave/accounting/model/ProjectRegistry.html?is-external=true]] {
            {static} +CustomerRegistry: Class<ProjectRegistry>
            {abstract} +findProjects(): ProjectFinder
            {abstract} +findJobEvents(): JobEventFinder
            {abstract} +addProject(): Builder
        }
        interface JobEvent [[http://bluehour.tidalwave.it/bluehour-modules/it-tidalwave-accounting-model/apidocs/it/tidalwave/accounting/model/JobEvent.html?is-external=true]] {
            {static} +builder(): Builder
            {abstract} +toBuilder(): Builder
        }
        interface CustomerRegistry [[http://bluehour.tidalwave.it/bluehour-modules/it-tidalwave-accounting-model/apidocs/it/tidalwave/accounting/model/CustomerRegistry.html?is-external=true]] {
            {abstract} +findCustomers(): Finder
            {abstract} +addCustomer(): Builder
        }
        interface Accounting [[http://bluehour.tidalwave.it/bluehour-modules/it-tidalwave-accounting-model/apidocs/it/tidalwave/accounting/model/Accounting.html?is-external=true]] {
            {static} +createNew(): Accounting
            {abstract} +getCustomerRegistry(): CustomerRegistry
            {abstract} +getProjectRegistry(): ProjectRegistry
            {abstract} +getInvoiceRegistry(): InvoiceRegistry
        }
        interface it.tidalwave.accounting.model.ProjectRegistry.JobEventFinder [[http://bluehour.tidalwave.it/bluehour-modules/it-tidalwave-accounting-model/apidocs/it/tidalwave/accounting/model/ProjectRegistry.JobEventFinder.html?is-external=true]] {
            {abstract} +withId(Id): JobEventFinder
            {abstract} +getDuration(): Duration
            {abstract} +getEarnings(): Money
        }
        interface InvoiceRegistry [[http://bluehour.tidalwave.it/bluehour-modules/it-tidalwave-accounting-model/apidocs/it/tidalwave/accounting/model/InvoiceRegistry.html?is-external=true]] {
            {abstract} +findInvoices(): Finder
            {abstract} +addInvoice(): Builder
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
