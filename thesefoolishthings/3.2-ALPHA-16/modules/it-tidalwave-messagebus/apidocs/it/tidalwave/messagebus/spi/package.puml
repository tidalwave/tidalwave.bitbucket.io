@startuml
    namespace it.tidalwave.messagebus.spi {

        interface MessageDelivery [[MessageDelivery.html]] {
            {abstract} +initialize(SimpleMessageBus): void
            {abstract} +deliverMessage(Class<T>, T): void
        }

        class MultiQueue [[MultiQueue.html]] {
            +add(Class<T>, T): void
            +remove(): TopicAndMessage<T>
        }

        class ReflectionUtils [[ReflectionUtils.html]] {
            {static} +forEachMethodInTopDownHierarchy(Object, MethodProcessor): void
            {static} +forEachMethodInBottomUpHierarchy(Object, MethodProcessor): void
            {static} +containsAnnotation(Annotation[], Class<?>): boolean
        }

        interface it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessor [[ReflectionUtils.MethodProcessor.html]] {
            {abstract} +filter(Class<?>): FilterResult
            {abstract} +process(Method): void
        }

        enum it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessor.FilterResult [[ReflectionUtils.MethodProcessor.FilterResult.html]] {
            {static} +ACCEPT
            {static} +IGNORE
        }

        class it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessorSupport [[ReflectionUtils.MethodProcessorSupport.html]] {
            +filter(Class<?>): FilterResult
            +process(Method): void
        }

        class RoundRobinAsyncMessageDelivery [[RoundRobinAsyncMessageDelivery.html]] {
            +initialize(SimpleMessageBus): void
            +deliverMessage(Class<T>, T): void
        }

        class SimpleAsyncMessageDelivery [[SimpleAsyncMessageDelivery.html]] {
            +initialize(SimpleMessageBus): void
            +deliverMessage(Class<T>, T): void
        }

        class SimpleMessageBus [[SimpleMessageBus.html]] {
            +SimpleMessageBus()
            +SimpleMessageBus(Executor)
            +SimpleMessageBus(Executor, MessageDelivery)
            +publish(T): void
            +publish(Class<T>, T): void
            +subscribe(Class<T>, Listener<T>): void
            +unsubscribe(Listener<?>): void
            #dispatchMessage(Class<T>, T): void
        }

        ReflectionUtils +-- it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessor
        it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessor +-- it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessor.FilterResult
        it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessor <|.. it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessorSupport
        ReflectionUtils +-- it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessorSupport
        MessageDelivery <|.. RoundRobinAsyncMessageDelivery
        MessageDelivery <|.. SimpleAsyncMessageDelivery
        it.tidalwave.messagebus.MessageBus <|.. SimpleMessageBus
    }

    namespace it.tidalwave.messagebus {
        interface MessageBus [[../MessageBus.html]] {
            {abstract} +publish(T): void
            {abstract} +publish(Class<T>, T): void
            {abstract} +subscribe(Class<T>, Listener<T>): void
            {abstract} +unsubscribe(Listener<?>): void
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
