@startuml
    namespace it.tidalwave.util {

        interface As [[As.html]] {
            {static} +forObject(Object): As
            {static} +forObject(Object, Object): As
            {static} +forObject(Object, Collection<Object>): As
            +as(Class<? extends T>): T
            {abstract} +maybeAs(Class<? extends T>): Optional<T>
            {abstract} +asMany(Class<? extends T>): Collection<T>
            {static} +type(Class<?>): Type<T>
            +as(Type<? extends T>): T
            +maybeAs(Type<? extends T>): Optional<T>
            +asMany(Type<? extends T>): Collection<T>
        }

        class it.tidalwave.util.As.Type<T> [[As.Type.html]] {
            #getType(): Class<T>
        }

        class AsException [[AsException.html]] {
            +AsException(Class<?>)
            +AsException(Class<?>, Throwable)
        }

        class AsExtensions [[AsExtensions.html]] {
            {static} +as(Object, Class<T>): T
            {static} +maybeAs(Object, Class<? extends T>): Optional<T>
            {static} +asMany(Object, Class<? extends T>): Collection<T>
            {static} +as(Object, Type<? extends T>): T
            {static} +maybeAs(Object, Type<? extends T>): Optional<T>
            {static} +asMany(Object, Type<? extends T>): Collection<T>
        }

        class BundleUtilities [[BundleUtilities.html]] {
            {static} +getMessage(Class<?>, String, Object...): String
            {static} +getMessage(Class<?>, Locale, String, Object...): String
        }

        interface Callback [[Callback.html]] {
            {abstract} +call(): void
        }

        class CollectionUtils [[CollectionUtils.html]] {
            {static} +concat(List<? extends T>, T): List<T>
            {static} +concat(List<? extends T>, List<? extends T>): List<T>
            {static} +reversed(List<? extends T>): List<T>
            {static} +sorted(List<? extends Comparable<? super T>>): List<T extends Comparable<? super T>>
            {static} +sorted(List<? extends T>, Comparator<? super T>): List<T>
            {static} +optionalHead(List<? extends T>): Optional<T>
            {static} +head(List<? extends T>): T
            {static} +tail(List<? extends T>): List<T>
        }

        class ConcurrentHashMapWithOptionals<K, V> [[ConcurrentHashMapWithOptionals.html]] {
            +putIfAbsentAndGetNewKey(Optional<? extends K>, V): Optional<K>
            +putIfAbsentAndGetNewKey(K, V): Optional<K>
        }

        class DuplicateException [[DuplicateException.html]] {
            +DuplicateException()
            +DuplicateException(String)
        }

        interface Finder<T> [[Finder.html]] {
            {abstract} +from(int): Finder<T>
            {abstract} +max(int): Finder<T>
            +withContext(Object): Finder<T>
            +ofType(Class<​U>): Finder<​U>
            +sort(SortCriterion): Finder<T>
            {abstract} +sort(SortCriterion, SortDirection): Finder<T>
            {abstract} +results(): List<T>
            {abstract} +count(): int
            +optionalResult(): Optional<T>
            +optionalFirstResult(): Optional<T>
            +stream(): Stream<T>
            +iterator(): Iterator<T>
            +--result--(): T
            +--firstResult--(): T
            {static} +empty(): Finder<​U>
            {static} +ofCloned(Collection<? extends U>): Finder<​U>
            {static} +ofSupplier(Supplier<? extends Collection<? extends U>>): Finder<​U>
            {static} +ofProvider(BiFunction<Integer, Integer, ? extends Collection<? extends U>>): Finder<​U>
            {static} +mapping(Finder<V>, Function<? super V, ? extends U>): Finder<​U>
        }

        interface it.tidalwave.util.Finder.SortCriterion [[Finder.SortCriterion.html]] {
            {static} +_SortCriterion_: Class<SortCriterion>
        }

        interface it.tidalwave.util.Finder.InMemorySortCriterion<​U> [[Finder.InMemorySortCriterion.html]] {
            +sort(List<? extends U>): void
            {abstract} +sort(List<? extends U>, SortDirection): void
            {static} +of(Comparator<? super U>): InMemorySortCriterion<​U>
            {static} +of(Comparator<? super U>, String): InMemorySortCriterion<​U>
        }

        class it.tidalwave.util.Finder.InMemorySortCriterion.DefaultInMemorySortCriterion<​U> [[Finder.InMemorySortCriterion.DefaultInMemorySortCriterion.html]] {
            +sort(List<? extends U>, SortDirection): void
        }

        enum it.tidalwave.util.Finder.SortDirection [[Finder.SortDirection.html]] {
            {static} +ASCENDING
            {static} +DESCENDING
            +intValue(): int
        }

        class FunctionalCheckedExceptionWrappers [[FunctionalCheckedExceptionWrappers.html]] {
            {static} +_f(FunctionWithException<? super T, ? extends R>): Function<T, R>
            {static} +_c(ConsumerWithException<? super T>): Consumer<T>
            {static} +_s(SupplierWithException<? extends T>): Supplier<T>
            {static} +_p(PredicateWithException<? super T>): Predicate<T>
            {static} +_r(RunnableWithException): Runnable
            {static} +wrappedException(Throwable): RuntimeException
        }

        interface it.tidalwave.util.FunctionalCheckedExceptionWrappers.FunctionWithException<T, R> {
            {abstract} +apply(T): R
        }

        interface it.tidalwave.util.FunctionalCheckedExceptionWrappers.ConsumerWithException<T> {
            {abstract} +accept(T): void
        }

        interface it.tidalwave.util.FunctionalCheckedExceptionWrappers.SupplierWithException<T> {
            {abstract} +get(): T
        }

        interface it.tidalwave.util.FunctionalCheckedExceptionWrappers.PredicateWithException<T> {
            {abstract} +test(T): boolean
        }

        interface it.tidalwave.util.FunctionalCheckedExceptionWrappers.RunnableWithException {
            {abstract} +run(): void
        }

        class Id [[Id.html]] {
            {static} +of(Object): Id
            {static} +ofUuid(): Id
            +stringValue(): String
            +compareTo(Id): int
        }

        interface Initializer<T> [[Initializer.html]] {
            {abstract} +initialize(T): T
            {static} +empty(): Initializer<T>
        }

        class it.tidalwave.util.Initializer.EmptyInitializer<K> [[Initializer.EmptyInitializer.html]] {
            +initialize(K): K
        }

        class Key<T> [[Key.html]] {
            +--Key--(String)
            +--Key--(StringValue)
            {static} +of(String, Class<T>): Key<T>
            {static} +of(String): Key<Object>
            {static} +allKeys(): Set<Key<?>>
            +stringValue(): String
            +compareTo(Key<?>): int
        }

        class LazySupplier<T> [[LazySupplier.html]] {
            +get(): T
            +clear(): void
            +set(T): void
        }

        class LocalizedDateTimeFormatters [[LocalizedDateTimeFormatters.html]] {
            {static} +getDateTimeFormatterFor(FormatStyle, Locale): DateTimeFormatter
        }

        class NamedCallback [[NamedCallback.html]] {
            {static} +_NamedCallback_: Class<NamedCallback>
        }

        class NotFoundException [[NotFoundException.html]] {
            +NotFoundException()
            +NotFoundException(String)
            +NotFoundException(Throwable)
            +NotFoundException(String, Throwable)
            {static} +throwWhenNull(T, String): T
            {static} +throwWhenNull(T, String, Object...): T
            {static} +throwWhenEmpty(T extends Collection<?>, String): T extends Collection<?>
            {static} +throwWhenEmpty(T extends Collection<?>, String, Object...): T extends Collection<?>
        }

        class Pair<A, B> [[Pair.html]] {
            {static} +BASE_0: IntUnaryOperator
            {static} +BASE_1: IntUnaryOperator
            +a: A
            +b: B
            {static} +pairStream(T, Stream<? extends U>): Stream<Pair<T, U>>
            {static} +pairRange(T, int, int): Stream<Pair<T, Integer>>
            {static} +pairRangeClosed(T, int, int): Stream<Pair<T, Integer>>
            {static} +indexedPairStream(T[]): Stream<Pair<Integer, T>>
            {static} +indexedPairStream(T[], IntUnaryOperator): Stream<Pair<Integer, T>>
            {static} +indexedPairStream(T[], IntFunction<? extends I>): Stream<Pair<I, T>>
            {static} +indexedPairStream(T[], IntUnaryOperator, IntFunction<? extends I>): Stream<Pair<I, T>>
            {static} +indexedPairStream(Iterable<? extends T>): Stream<Pair<Integer, T>>
            {static} +indexedPairStream(Iterable<? extends T>, IntUnaryOperator): Stream<Pair<Integer, T>>
            {static} +indexedPairStream(Iterable<? extends T>, IntFunction<? extends I>): Stream<Pair<I, T>>
            {static} +indexedPairStream(Iterable<? extends T>, IntUnaryOperator, IntFunction<? extends I>): Stream<Pair<I, T>>
            {static} +indexedPairStream(Stream<? extends T>): Stream<Pair<Integer, T>>
            {static} +indexedPairStream(Stream<? extends T>, IntUnaryOperator): Stream<Pair<Integer, T>>
            {static} +indexedPairStream(Stream<? extends T>, IntFunction<? extends I>): Stream<Pair<I, T>>
            {static} +indexedPairStream(Stream<? extends T>, IntUnaryOperator, IntFunction<? extends I>): Stream<Pair<I, T>>
            {static} +indexedPairStream(int, int, IntFunction<? extends T>): Stream<Pair<Integer, T>>
            {static} +indexedPairStream(int, int, IntFunction<? extends T>, IntUnaryOperator): Stream<Pair<Integer, T>>
            {static} +indexedPairStream(int, int, IntFunction<? extends T>, IntUnaryOperator, IntFunction<? extends I>): Stream<Pair<I, T>>
            {static} +pairsToMap(): Collector<Pair<A, B>, ?, Map<A, B>>
        }

        class Parameters [[Parameters.html]] {
            {static} +r(Object...): Collection<Object>
            {static} +find(Class<? extends T>, T, Object...): T
            {static} +find(Class<? extends T>, Object...): Collection<T>
            {static} +mustNotBeArrayOrCollection(Object, String): Object
        }

        interface ProcessExecutor [[ProcessExecutor.html]] {
            {abstract} +withArguments(String...): ProcessExecutor
            {abstract} +withArgument(String): ProcessExecutor
            {abstract} +start(): ProcessExecutor
            {abstract} +stop(): void
            {abstract} +waitForCompletion(): ProcessExecutor
            {abstract} +send(String): ProcessExecutor
            {abstract} +getStdout(): ConsoleOutput
            {abstract} +getStderr(): ConsoleOutput
        }

        interface it.tidalwave.util.ProcessExecutor.ConsoleOutput [[ProcessExecutor.ConsoleOutput.html]] {
            {abstract} +latestLineMatches(String): boolean
            {abstract} +filteredBy(String): List<String>
            {abstract} +filteredAndSplitBy(String, String): Scanner
            {abstract} +waitFor(String): ConsoleOutput
            {abstract} +clear(): void
            {abstract} +setListener(Listener): void
            {abstract} +getListener(): Listener
        }

        interface it.tidalwave.util.ProcessExecutor.ConsoleOutput.Listener [[ProcessExecutor.ConsoleOutput.Listener.html]] {
            {abstract} +onReceived(String): void
        }

        class ReflectionUtils [[ReflectionUtils.html]] {
            {static} +getTypeArguments(Class<T>, Class<? extends T>): List<Class<?>>
            {static} +getClass(Type): Class<?>
        }

        interface RoleFactory<T> [[RoleFactory.html]] {
            {abstract} +createRoleFor(T): Object
        }

        abstract class SimpleTask [[SimpleTask.html]]

        class StreamOperations [[StreamOperations.html]] {
            {static} +zip(Stream<? extends A>, Stream<? extends B>): Stream<Pair<A, B>>
            {static} +zip(Stream<A>, Stream<​B>, BiFunction<? super A, ? super B, ? extends R>): Stream<R>
        }

        interface StringValue [[StringValue.html]] {
            {abstract} +stringValue(): String
        }

        abstract class Task<T, E extends Throwable> [[Task.html]] {
            +Task()
            +Task(String)
            {abstract} +run(): T
            {static} +ofRunnable(Runnable): Task<Void, RuntimeException>
            {static} +ofCallable(Callable<? extends T>): Task<T, Exception>
            {static} +ofCallback(Callback): Task<Void, Throwable>
        }

        interface TimeProvider [[TimeProvider.html]] {
            {static} +__INSTANCE: AtomicReference<TimeProvider>
            {abstract} +currentInstant(): Instant
            +get(): Instant
            +currentZonedDateTime(): ZonedDateTime
            +currentLocalDateTime(): LocalDateTime
            {static} +getInstance(): TimeProvider
        }

        class it.tidalwave.util.TimeProvider.DefaultTimeProvider [[TimeProvider.DefaultTimeProvider.html]] {
            +currentInstant(): Instant
        }

        class Triple<A, B, C> [[Triple.html]] {
            +a: A
            +b: B
            +c: C
            {static} +of(Pair<T, U>, V): Triple<T, U, V>
            {static} +tripleStream(Pair<T, U>, Stream<? extends V>): Stream<Triple<T, U, V>>
            {static} +tripleRange(Pair<T, U>, int, int): Stream<Triple<T, U, Integer>>
            {static} +tripleRangeClosed(Pair<T, U>, int, int): Stream<Triple<T, U, Integer>>
        }

        interface TypeSafeMap [[TypeSafeMap.html]] {
            {abstract} +--get--(Key<T>): T
            +getOptional(Key<? extends T>): Optional<T>
            {abstract} +containsKey(Key<?>): boolean
            {abstract} +keySet(): Set<Key<?>>
            {abstract} +values(): Collection<Object>
            {abstract} +entrySet(): Set<Entry<Key<?>, Object>>
            {abstract} +size(): int
            {abstract} +asMap(): Map<Key<?>, Object>
            {abstract} +forEach(BiConsumer<? super Key<?>, ? super Object>): void
            {abstract} +with(Key<T>, T): TypeSafeMap
            {static} +newInstance(): TypeSafeMap
            {static} +ofCloned(Map<? extends Key<?>, Object>): TypeSafeMap
            +--getSize--(): int
        }

        interface TypeSafeMultiMap [[TypeSafeMultiMap.html]] {
            {abstract} +get(Key<T>): Collection<T>
            {abstract} +containsKey(Key<?>): boolean
            {abstract} +keySet(): Set<Key<?>>
            {abstract} +values(): Collection<Collection<?>>
            {abstract} +entrySet(): Set<Entry<Key<?>, Collection<?>>>
            {abstract} +size(): int
            {abstract} +forEach(BiConsumer<? super Key<?>, ? super Collection<?>>): void
            {abstract} +asMap(): Map<Key<?>, Collection<?>>
            {abstract} +with(Key<T>, T): TypeSafeMultiMap
            {static} +ofCloned(Map<? extends Key<?>, ? extends Collection<?>>): TypeSafeMultiMap
            {static} +newInstance(): TypeSafeMultiMap
            +--getSize--(): int
        }

        As +-- it.tidalwave.util.As.Type
        java.lang.RuntimeException <|-- AsException
        Callback --> Callback: EMPTY
        java.util.concurrent.ConcurrentHashMap <|-- ConcurrentHashMapWithOptionals
        java.lang.Exception <|-- DuplicateException
        java.lang.Cloneable <|-- Finder
        java.io.Serializable <|-- Finder
        Finder +-- it.tidalwave.util.Finder.SortCriterion
        it.tidalwave.util.Finder.SortCriterion --> it.tidalwave.util.Finder.SortCriterion: UNSORTED\nDEFAULT
        it.tidalwave.util.Finder.SortCriterion <|-- it.tidalwave.util.Finder.InMemorySortCriterion
        Finder +-- it.tidalwave.util.Finder.InMemorySortCriterion
        it.tidalwave.util.Finder.InMemorySortCriterion <|.. it.tidalwave.util.Finder.InMemorySortCriterion.DefaultInMemorySortCriterion
        java.io.Serializable <|.. it.tidalwave.util.Finder.InMemorySortCriterion.DefaultInMemorySortCriterion
        it.tidalwave.util.Finder.InMemorySortCriterion +-- it.tidalwave.util.Finder.InMemorySortCriterion.DefaultInMemorySortCriterion
        Finder +-- it.tidalwave.util.Finder.SortDirection
        FunctionalCheckedExceptionWrappers +-- it.tidalwave.util.FunctionalCheckedExceptionWrappers.FunctionWithException
        FunctionalCheckedExceptionWrappers +-- it.tidalwave.util.FunctionalCheckedExceptionWrappers.ConsumerWithException
        FunctionalCheckedExceptionWrappers +-- it.tidalwave.util.FunctionalCheckedExceptionWrappers.SupplierWithException
        FunctionalCheckedExceptionWrappers +-- it.tidalwave.util.FunctionalCheckedExceptionWrappers.PredicateWithException
        FunctionalCheckedExceptionWrappers +-- it.tidalwave.util.FunctionalCheckedExceptionWrappers.RunnableWithException
        java.io.Serializable <|.. Id
        java.lang.Comparable <|.. Id
        StringValue <|.. Id
        Initializer <|.. it.tidalwave.util.Initializer.EmptyInitializer
        java.io.Serializable <|.. it.tidalwave.util.Initializer.EmptyInitializer
        Initializer +-- it.tidalwave.util.Initializer.EmptyInitializer
        StringValue <|.. Key
        java.lang.Comparable <|.. Key
        java.io.Serializable <|.. Key
        java.util.function.Supplier <|.. LazySupplier
        Callback <|.. NamedCallback
        java.lang.Exception <|-- NotFoundException
        ProcessExecutor +-- it.tidalwave.util.ProcessExecutor.ConsoleOutput
        it.tidalwave.util.ProcessExecutor.ConsoleOutput +-- it.tidalwave.util.ProcessExecutor.ConsoleOutput.Listener
        Task <|-- SimpleTask
        java.util.function.Supplier <|-- TimeProvider
        TimeProvider <|.. it.tidalwave.util.TimeProvider.DefaultTimeProvider
        TimeProvider +-- it.tidalwave.util.TimeProvider.DefaultTimeProvider
        java.lang.Iterable <|-- TypeSafeMap
        TypeSafeMap --> "*" Key: keys
        java.lang.Iterable <|-- TypeSafeMultiMap
        TypeSafeMultiMap --> "*" Key: keys
    }

    namespace java.lang {
        class RuntimeException {
        }
        class Exception {
        }
        interface Cloneable
        interface Comparable<T> {
            {abstract} +compareTo(T): int
        }
        interface Iterable<T> {
            {abstract} +iterator(): Iterator<T>
            +forEach(Consumer<? super T>): void
            +spliterator(): Spliterator<T>
        }
    }

    namespace java.util.concurrent {
        class ConcurrentHashMap<K, V> {
        }
    }

    namespace java.io {
        interface Serializable
    }

    namespace java.util.function {
        interface Supplier<T> {
            {abstract} +get(): T
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
