@startuml
    namespace it.tidalwave.util {

        class StreamUtils [[StreamUtils.html]] {
            {static} +zip(Stream<? extends A>, Stream<? extends B>, BiFunction<? super A, ? super B, ? extends R>): Stream<R>
        }

        class AsException [[AsException.html]] {
            +AsException(Class<?>)
            +AsException(Class<?>, Throwable)
        }

        interface ProcessExecutor [[ProcessExecutor.html]] {
            {abstract} +withArguments(String...): ProcessExecutor
            {abstract} +withArgument(String): ProcessExecutor
            {abstract} +start(): ProcessExecutor
            {abstract} +stop(): void
            {abstract} +waitForCompletion(): ProcessExecutor
            {abstract} +send(String): ProcessExecutor
            {abstract} +getStdout(): ConsoleOutput
            {abstract} +getStderr(): ConsoleOutput
        }

        interface it.tidalwave.util.ProcessExecutor.ConsoleOutput [[ProcessExecutor.ConsoleOutput.html]] {
            {abstract} +latestLineMatches(String): boolean
            {abstract} +filteredBy(String): List<String>
            {abstract} +filteredAndSplitBy(String, String): Scanner
            {abstract} +waitFor(String): ConsoleOutput
            {abstract} +clear(): void
            {abstract} +setListener(Listener): void
            {abstract} +getListener(): Listener
        }

        interface it.tidalwave.util.ProcessExecutor.ConsoleOutput.Listener [[ProcessExecutor.ConsoleOutput.Listener.html]] {
            {abstract} +onReceived(String): void
        }

        class LazySupplier<T> [[LazySupplier.html]] {
            +get(): T
            +clear(): void
            +set(T): void
        }

        interface Initializer<T> [[Initializer.html]] {
            {abstract} +initialize(T): T
            {static} +empty(): Initializer<T>
        }

        class it.tidalwave.util.Initializer.EmptyInitializer<K> [[Initializer.EmptyInitializer.html]] {
            +initialize(K): K
        }

        class Id [[Id.html]] {
            {static} +of(Object): Id
            {static} +ofUuid(): Id
            +stringValue(): String
            +compareTo(Id): int
        }

        interface As [[As.html]] {
            {static} +forObject(Object): As
            {static} +forObject(Object, Object): As
            {static} +forObject(Object, Collection<Object>): As
            +as(Class<? extends T>): T
            {abstract} +maybeAs(Class<? extends T>): Optional<T>
            {abstract} +asMany(Class<? extends T>): Collection<T>
            {static} +type(Class<?>): Type<T>
            +as(Type<? extends T>): T
            +maybeAs(Type<? extends T>): Optional<T>
            +asMany(Type<? extends T>): Collection<T>
        }

        class it.tidalwave.util.As.Type<T> [[As.Type.html]] {
        }

        class Pair<A, B> [[Pair.html]] {
            {static} +BASE_0: IntUnaryOperator
            {static} +BASE_1: IntUnaryOperator
            +a: A
            +b: B
            {static} +pairStream(T, Stream<? extends U>): Stream<Pair<T, U>>
            {static} +pairRange(T, int, int): Stream<Pair<T, Integer>>
            {static} +pairRangeClosed(T, int, int): Stream<Pair<T, Integer>>
            {static} +indexedPairStream(T[]): Stream<Pair<Integer, T>>
            {static} +indexedPairStream(T[], IntUnaryOperator): Stream<Pair<Integer, T>>
            {static} +indexedPairStream(T[], IntFunction<? extends I>): Stream<Pair<I, T>>
            {static} +indexedPairStream(T[], IntUnaryOperator, IntFunction<? extends I>): Stream<Pair<I, T>>
            {static} +indexedPairStream(Iterable<? extends T>): Stream<Pair<Integer, T>>
            {static} +indexedPairStream(Iterable<? extends T>, IntUnaryOperator): Stream<Pair<Integer, T>>
            {static} +indexedPairStream(Iterable<? extends T>, IntFunction<? extends I>): Stream<Pair<I, T>>
            {static} +indexedPairStream(Iterable<? extends T>, IntUnaryOperator, IntFunction<? extends I>): Stream<Pair<I, T>>
            {static} +indexedPairStream(Stream<? extends T>): Stream<Pair<Integer, T>>
            {static} +indexedPairStream(Stream<? extends T>, IntUnaryOperator): Stream<Pair<Integer, T>>
            {static} +indexedPairStream(Stream<? extends T>, IntFunction<? extends I>): Stream<Pair<I, T>>
            {static} +indexedPairStream(Stream<? extends T>, IntUnaryOperator, IntFunction<? extends I>): Stream<Pair<I, T>>
            {static} +indexedPairStream(int, int, IntFunction<? extends T>): Stream<Pair<Integer, T>>
            {static} +indexedPairStream(int, int, IntFunction<? extends T>, IntUnaryOperator): Stream<Pair<Integer, T>>
            {static} +indexedPairStream(int, int, IntFunction<? extends T>, IntUnaryOperator, IntFunction<? extends I>): Stream<Pair<I, T>>
            {static} +pairsToMap(): Collector<Pair<A, B>, ?, Map<A, B>>
            {static} +zip(Stream<? extends A>, Stream<? extends B>): Stream<Pair<A, B>>
        }

        class ConcurrentHashMapWithOptionals<K, V> [[ConcurrentHashMapWithOptionals.html]] {
            +putIfAbsentAndGetNewKey(Optional<? extends K>, V): Optional<K>
            +putIfAbsentAndGetNewKey(K, V): Optional<K>
        }

        interface PreferencesHandler [[PreferencesHandler.html]] {
            {static} +PROP_APP_NAME: String
            {static} +__BASE_NAME: String
            {static} +getInstance(): PreferencesHandler
            {abstract} +getAppFolder(): Path
            {abstract} +getLogFolder(): Path
            {static} +setAppName(String): void
            {abstract} +getProperty(Key<T>): Optional<T>
            {abstract} +setProperty(Key<T>, T): void
            {abstract} +setDefaultProperty(Key<T>, T): void
        }

        class it.tidalwave.util.PreferencesHandler.Inner [[PreferencesHandler.Inner.html]] {
        }

        class CollectionUtils [[CollectionUtils.html]] {
            {static} +concat(List<? extends T>, T): List<T>
            {static} +concatAll(Collection<? extends T>...): List<T>
            {static} +reversed(List<? extends T>): List<T>
            {static} +sorted(List<? extends Comparable<? super T>>): List<T extends Comparable<? super T>>
            {static} +sorted(List<? extends T>, Comparator<? super T>): List<T>
            {static} +optionalHead(List<? extends T>): Optional<T>
            {static} +head(List<? extends T>): T
            {static} +tail(List<? extends T>): List<T>
            {static} +safeSubList(List<? extends T>, int, int): List<T>
            {static} +split(List<? extends T>, int...): List<List<T>>
        }

        interface TypeSafeMultiMap [[TypeSafeMultiMap.html]] {
            {abstract} +get(Key<T>): Collection<T>
            {abstract} +containsKey(Key<?>): boolean
            {abstract} +keySet(): Set<Key<?>>
            {abstract} +values(): Collection<Collection<?>>
            {abstract} +entrySet(): Set<Entry<Key<?>, Collection<?>>>
            {abstract} +size(): int
            {abstract} +forEach(BiConsumer<? super Key<?>, ? super Collection<?>>): void
            {abstract} +asMap(): Map<Key<?>, Collection<?>>
            {abstract} +with(Key<T>, T): TypeSafeMultiMap
            {static} +ofCloned(Map<? extends Key<?>, ? extends Collection<?>>): TypeSafeMultiMap
            {static} +newInstance(): TypeSafeMultiMap
            +--getSize--(): int
        }

        class ShortNames [[ShortNames.html]] {
            {static} +shortName(Class<?>): String
            {static} +shortName(Class<?>, boolean): String
            {static} +shortNames(Iterable<Class<?>>): String
            {static} +shortId(Object): String
            {static} +shortIds(Iterable<?>): String
            {static} +shortIds(Object...): String
        }

        class NamedCallback [[NamedCallback.html]] {
            {static} +_NamedCallback_: Class<NamedCallback>
        }

        interface ContextManager [[ContextManager.html]] {
            {static} +getInstance(): ContextManager
            {static} +set(ContextManagerProvider): void
            {static} +reset(): void
            {abstract} +getContexts(): List<Object>
            {abstract} +findContextOfType(Class<T>): Optional<T>
            {abstract} +addGlobalContext(Object): void
            {abstract} +removeGlobalContext(Object): void
            {abstract} +addLocalContext(Object): void
            {abstract} +removeLocalContext(Object): void
            +--runWithContext--(Object, Task<V, T extends Throwable>): V
            +--runWithContexts--(List<Object>, Task<V, T extends Throwable>): V
            +--runWithContext--(Object, Supplier<V>): V
            +--runWithContexts--(List<Object>, Supplier<V>): V
            +runWithContexts(Runnable, Object...): void
            +runWithContexts(Supplier<T>, Object...): T
            +runEWithContexts(RunnableWithException<E extends Throwable>, Object...): void
            {abstract} +runEWithContexts(SupplierWithException<T, E extends Throwable>, Object...): T
            +binder(Object...): Binder
        }

        class it.tidalwave.util.ContextManager.Inner [[ContextManager.Inner.html]] {
        }

        interface it.tidalwave.util.ContextManager.RunnableWithException<E extends Throwable> [[ContextManager.RunnableWithException.html]] {
            {abstract} +run(): void
        }

        interface it.tidalwave.util.ContextManager.SupplierWithException<T, E extends Throwable> [[ContextManager.SupplierWithException.html]] {
            {abstract} +get(): T
        }

        class it.tidalwave.util.ContextManager.Binder [[ContextManager.Binder.html]] {
            +close(): void
        }

        class DuplicateException [[DuplicateException.html]] {
            +DuplicateException()
            +DuplicateException(String)
        }

        class BundleUtilities [[BundleUtilities.html]] {
            {static} +getMessage(Class<?>, String, Object...): String
            {static} +getMessage(Class<?>, Locale, String, Object...): String
        }

        abstract class SimpleTask [[SimpleTask.html]]

        interface Callback [[Callback.html]] {
            {abstract} +call(): void
        }

        interface TimeProvider [[TimeProvider.html]] {
            {static} +__INSTANCE: AtomicReference<TimeProvider>
            {abstract} +currentInstant(): Instant
            +get(): Instant
            +currentZonedDateTime(): ZonedDateTime
            +currentLocalDateTime(): LocalDateTime
            {static} +getInstance(): TimeProvider
        }

        class it.tidalwave.util.TimeProvider.DefaultTimeProvider [[TimeProvider.DefaultTimeProvider.html]] {
            +currentInstant(): Instant
        }

        abstract class Task<T, E extends Throwable> [[Task.html]] {
            +Task()
            +Task(String)
            {abstract} +run(): T
            {static} +ofRunnable(Runnable): Task<Void, RuntimeException>
            {static} +ofCallable(Callable<? extends T>): Task<T, Exception>
            {static} +ofCallback(Callback): Task<Void, Throwable>
        }

        interface StringValue [[StringValue.html]] {
            {abstract} +stringValue(): String
        }

        interface RoleFactory<T> [[RoleFactory.html]] {
            {abstract} +createRoleFor(T): Object
        }

        class Triple<A, B, C> [[Triple.html]] {
            +a: A
            +b: B
            +c: C
            {static} +of(Pair<T, U>, V): Triple<T, U, V>
            {static} +tripleStream(Pair<T, U>, Stream<? extends V>): Stream<Triple<T, U, V>>
            {static} +tripleRange(Pair<T, U>, int, int): Stream<Triple<T, U, Integer>>
            {static} +tripleRangeClosed(Pair<T, U>, int, int): Stream<Triple<T, U, Integer>>
        }

        class Key<T> [[Key.html]] {
            +--Key--(String)
            +--Key--(StringValue)
            {static} +of(String, Class<T>): Key<T>
            {static} +of(String): Key<Object>
            {static} +allKeys(): Set<Key<?>>
            +stringValue(): String
            +compareTo(Key<?>): int
        }

        class AsExtensions [[AsExtensions.html]] {
            {static} +as(Object, Class<T>): T
            {static} +maybeAs(Object, Class<? extends T>): Optional<T>
            {static} +asMany(Object, Class<? extends T>): Collection<T>
            {static} +as(Object, Type<? extends T>): T
            {static} +maybeAs(Object, Type<? extends T>): Optional<T>
            {static} +asMany(Object, Type<? extends T>): Collection<T>
        }

        class Parameters [[Parameters.html]] {
            {static} +r(Object...): Collection<Object>
            {static} +find(Class<? extends T>, T, Object...): T
            {static} +find(Class<? extends T>, Object...): Collection<T>
            {static} +mustNotBeArrayOrCollection(Object, String): Object
        }

        class NotFoundException [[NotFoundException.html]] {
            +NotFoundException()
            +NotFoundException(String)
            +NotFoundException(Throwable)
            +NotFoundException(String, Throwable)
            {static} +throwWhenNull(T, String): T
            {static} +throwWhenNull(T, String, Object...): T
            {static} +throwWhenEmpty(T extends Collection<?>, String): T extends Collection<?>
            {static} +throwWhenEmpty(T extends Collection<?>, String, Object...): T extends Collection<?>
        }

        class ReflectionUtils [[ReflectionUtils.html]] {
            {static} +getTypeArguments(Class<T>, Class<? extends T>): List<Class<?>>
            {static} +instantiateWithDependencies(Class<? extends T>, Map<Class<?>, Object>): T
            {static} +injectDependencies(Object, Map<Class<?>, Object>): void
            {static} +getClass(Type): Class<?>
        }

        class FunctionalCheckedExceptionWrappers [[FunctionalCheckedExceptionWrappers.html]] {
            {static} +_f(FunctionWithException<? super T, ? extends R>): Function<T, R>
            {static} +_c(ConsumerWithException<? super T>): Consumer<T>
            {static} +_s(SupplierWithException<? extends T>): Supplier<T>
            {static} +_p(PredicateWithException<? super T>): Predicate<T>
            {static} +_r(RunnableWithException): Runnable
            {static} +wrappedException(Throwable): RuntimeException
        }

        interface it.tidalwave.util.FunctionalCheckedExceptionWrappers.FunctionWithException<T, R> {
            {abstract} +apply(T): R
        }

        interface it.tidalwave.util.FunctionalCheckedExceptionWrappers.ConsumerWithException<T> {
            {abstract} +accept(T): void
        }

        interface it.tidalwave.util.FunctionalCheckedExceptionWrappers.SupplierWithException<T> {
            {abstract} +get(): T
        }

        interface it.tidalwave.util.FunctionalCheckedExceptionWrappers.PredicateWithException<T> {
            {abstract} +test(T): boolean
        }

        interface it.tidalwave.util.FunctionalCheckedExceptionWrappers.RunnableWithException {
            {abstract} +run(): void
        }

        class LocalizedDateTimeFormatters [[LocalizedDateTimeFormatters.html]] {
            {static} +getDateTimeFormatterFor(FormatStyle, Locale): DateTimeFormatter
        }

        interface Finder<T> [[Finder.html]] {
            {abstract} +from(int): Finder<T>
            {abstract} +max(int): Finder<T>
            +withContext(Object): Finder<T>
            +ofType(Class<​U>): Finder<​U>
            +sort(SortCriterion): Finder<T>
            {abstract} +sort(SortCriterion, SortDirection): Finder<T>
            {abstract} +results(): List<T>
            {abstract} +count(): int
            +optionalResult(): Optional<T>
            +optionalFirstResult(): Optional<T>
            +stream(): Stream<T>
            +iterator(): Iterator<T>
            +--result--(): T
            +--firstResult--(): T
            {static} +empty(): Finder<​U>
            {static} +ofCloned(Collection<? extends U>): Finder<​U>
            {static} +ofSupplier(Supplier<? extends Collection<? extends U>>): Finder<​U>
            {static} +ofProvider(BiFunction<Integer, Integer, ? extends Collection<? extends U>>): Finder<​U>
            {static} +mapping(Finder<V>, Function<? super V, ? extends U>): Finder<​U>
        }

        interface it.tidalwave.util.Finder.SortCriterion [[Finder.SortCriterion.html]] {
            {static} +_SortCriterion_: Class<SortCriterion>
        }

        interface it.tidalwave.util.Finder.InMemorySortCriterion<​U> [[Finder.InMemorySortCriterion.html]] {
            +sort(List<? extends U>): void
            {abstract} +sort(List<? extends U>, SortDirection): void
            {static} +of(Comparator<? super U>): InMemorySortCriterion<​U>
            {static} +of(Comparator<? super U>, String): InMemorySortCriterion<​U>
        }

        class it.tidalwave.util.Finder.InMemorySortCriterion.DefaultInMemorySortCriterion<​U> [[Finder.InMemorySortCriterion.DefaultInMemorySortCriterion.html]] {
            +sort(List<? extends U>, SortDirection): void
        }

        enum it.tidalwave.util.Finder.SortDirection [[Finder.SortDirection.html]] {
            {static} +ASCENDING
            {static} +DESCENDING
            +intValue(): int
        }

        interface TypeSafeMap [[TypeSafeMap.html]] {
            {abstract} +--get--(Key<T>): T
            +getOptional(Key<? extends T>): Optional<T>
            {abstract} +containsKey(Key<?>): boolean
            {abstract} +keySet(): Set<Key<?>>
            {abstract} +values(): Collection<Object>
            {abstract} +entrySet(): Set<Entry<Key<?>, Object>>
            {abstract} +size(): int
            {abstract} +asMap(): Map<Key<?>, Object>
            {abstract} +forEach(BiConsumer<? super Key<?>, ? super Object>): void
            {abstract} +with(Key<T>, T): TypeSafeMap
            {static} +newInstance(): TypeSafeMap
            {static} +ofCloned(Map<? extends Key<?>, Object>): TypeSafeMap
            +--getSize--(): int
        }

        java.lang.RuntimeException <|-- AsException
        ProcessExecutor +-- it.tidalwave.util.ProcessExecutor.ConsoleOutput
        it.tidalwave.util.ProcessExecutor.ConsoleOutput +-- it.tidalwave.util.ProcessExecutor.ConsoleOutput.Listener
        java.util.function.Supplier <|.. LazySupplier
        Initializer <|.. it.tidalwave.util.Initializer.EmptyInitializer
        java.io.Serializable <|.. it.tidalwave.util.Initializer.EmptyInitializer
        Initializer +-- it.tidalwave.util.Initializer.EmptyInitializer
        java.io.Serializable <|.. Id
        java.lang.Comparable <|.. Id
        StringValue <|.. Id
        As +-- it.tidalwave.util.As.Type
        java.util.concurrent.ConcurrentHashMap <|-- ConcurrentHashMapWithOptionals
        PreferencesHandler --> Key: KEY_INITIAL_SIZE\nKEY_FULL_SCREEN
        PreferencesHandler +-- it.tidalwave.util.PreferencesHandler.Inner
        java.lang.Iterable <|-- TypeSafeMultiMap
        TypeSafeMultiMap --> "*" Key: keys
        Callback <|.. NamedCallback
        ContextManager +-- it.tidalwave.util.ContextManager.Inner
        ContextManager +-- it.tidalwave.util.ContextManager.RunnableWithException
        ContextManager +-- it.tidalwave.util.ContextManager.SupplierWithException
        java.lang.AutoCloseable <|.. it.tidalwave.util.ContextManager.Binder
        ContextManager +-- it.tidalwave.util.ContextManager.Binder
        java.lang.Exception <|-- DuplicateException
        Task <|-- SimpleTask
        Callback --> Callback: EMPTY
        java.util.function.Supplier <|-- TimeProvider
        TimeProvider <|.. it.tidalwave.util.TimeProvider.DefaultTimeProvider
        TimeProvider +-- it.tidalwave.util.TimeProvider.DefaultTimeProvider
        StringValue <|.. Key
        java.lang.Comparable <|.. Key
        java.io.Serializable <|.. Key
        java.lang.Exception <|-- NotFoundException
        FunctionalCheckedExceptionWrappers +-- it.tidalwave.util.FunctionalCheckedExceptionWrappers.FunctionWithException
        FunctionalCheckedExceptionWrappers +-- it.tidalwave.util.FunctionalCheckedExceptionWrappers.ConsumerWithException
        FunctionalCheckedExceptionWrappers +-- it.tidalwave.util.FunctionalCheckedExceptionWrappers.SupplierWithException
        FunctionalCheckedExceptionWrappers +-- it.tidalwave.util.FunctionalCheckedExceptionWrappers.PredicateWithException
        FunctionalCheckedExceptionWrappers +-- it.tidalwave.util.FunctionalCheckedExceptionWrappers.RunnableWithException
        java.lang.Cloneable <|-- Finder
        java.io.Serializable <|-- Finder
        Finder +-- it.tidalwave.util.Finder.SortCriterion
        it.tidalwave.util.Finder.SortCriterion --> it.tidalwave.util.Finder.SortCriterion: UNSORTED\nDEFAULT
        it.tidalwave.util.Finder.SortCriterion <|-- it.tidalwave.util.Finder.InMemorySortCriterion
        Finder +-- it.tidalwave.util.Finder.InMemorySortCriterion
        it.tidalwave.util.Finder.InMemorySortCriterion <|.. it.tidalwave.util.Finder.InMemorySortCriterion.DefaultInMemorySortCriterion
        java.io.Serializable <|.. it.tidalwave.util.Finder.InMemorySortCriterion.DefaultInMemorySortCriterion
        it.tidalwave.util.Finder.InMemorySortCriterion +-- it.tidalwave.util.Finder.InMemorySortCriterion.DefaultInMemorySortCriterion
        Finder +-- it.tidalwave.util.Finder.SortDirection
        java.lang.Iterable <|-- TypeSafeMap
        TypeSafeMap --> "*" Key: keys
    }

    namespace java.lang {
        class RuntimeException {
        }
        interface Comparable<T> {
            {abstract} +compareTo(T): int
        }
        interface Iterable<T> {
            {abstract} +iterator(): Iterator<T>
            +forEach(Consumer<? super T>): void
            +spliterator(): Spliterator<T>
        }
        interface AutoCloseable {
            {abstract} +close(): void
        }
        class Exception {
        }
        interface Cloneable
    }

    namespace java.util.function {
        interface Supplier<T> {
            {abstract} +get(): T
        }
    }

    namespace java.io {
        interface Serializable
    }

    namespace java.util.concurrent {
        class ConcurrentHashMap<K, V> {
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
