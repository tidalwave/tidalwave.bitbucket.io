@startuml
    namespace it.tidalwave.thesefoolishthings.examples.person {

        class ListOfPersons [[ListOfPersons.html]] {
            +ListOfPersons(List<? extends Person>)
            {static} +empty(): ListOfPersons
            {static} +of(Person...): ListOfPersons
        }

        class DefaultPersonRegistry [[DefaultPersonRegistry.html]] {
            +findPerson(): Finder<Person>
            +add(Person): void
        }

        class Person [[Person.html]] {
            +Person(String, String)
            {static} +prototype(): Person
        }

        interface PersonRegistry [[PersonRegistry.html]] {
            {abstract} +findPerson(): Finder<Person>
            {abstract} +add(Person): void
        }

        class PersonRegistryHelper [[PersonRegistryHelper.html]] {
            {static} +populate(Collection<? super Person>): void
            {static} +populate(PersonRegistry): void
        }

        class PersonRenderable [[PersonRenderable.html]] {
            +renderTo(String, Consumer<String>): void
        }

        class PersonDisplayable [[PersonDisplayable.html]] {
            +getDisplayName(): String
        }

        class PersonHtmlRenderable [[PersonHtmlRenderable.html]] {
            +render(Object...): String
        }

        class PersonRegistryTableHeaderDescriptor [[PersonRegistryTableHeaderDescriptor.html]] {
            +getColumnDescriptors(): List<TableColumnDescriptor>
        }

        class PersonRegistryObservableListProvider [[PersonRegistryObservableListProvider.html]] {
            +createObservableList(): ObservableList<? extends Person>
        }

        java.util.List <|.. ListOfPersons
        PersonRegistry <|.. DefaultPersonRegistry
        it.tidalwave.thesefoolishthings.examples.dci.displayable.role.Renderable <|.. PersonRenderable
        it.tidalwave.role.ui.Displayable <|.. PersonDisplayable
        it.tidalwave.role.HtmlRenderable <|.. PersonHtmlRenderable
        it.tidalwave.role.StringRenderable <|.. PersonHtmlRenderable
        it.tidalwave.thesefoolishthings.examples.dci.swing.role.TableHeaderDescriptor <|.. PersonRegistryTableHeaderDescriptor
        it.tidalwave.thesefoolishthings.examples.dci.swing.role.ObservableListProvider <|.. PersonRegistryObservableListProvider
    }

    namespace java.util {
        interface List<E> {
            {abstract} +size(): int
            {abstract} +isEmpty(): boolean
            {abstract} +contains(Object): boolean
            {abstract} +iterator(): Iterator<E>
            {abstract} +toArray(): Object[]
            {abstract} +toArray(T[]): T[]
            {abstract} +add(E): boolean
            {abstract} +remove(Object): boolean
            {abstract} +containsAll(Collection<?>): boolean
            {abstract} +addAll(Collection<? extends E>): boolean
            {abstract} +addAll(int, Collection<? extends E>): boolean
            {abstract} +removeAll(Collection<?>): boolean
            {abstract} +retainAll(Collection<?>): boolean
            +replaceAll(UnaryOperator<E>): void
            +sort(Comparator<? super E>): void
            {abstract} +clear(): void
            {abstract} +get(int): E
            {abstract} +set(int, E): E
            {abstract} +add(int, E): void
            {abstract} +remove(int): E
            {abstract} +indexOf(Object): int
            {abstract} +lastIndexOf(Object): int
            {abstract} +listIterator(): ListIterator<E>
            {abstract} +listIterator(int): ListIterator<E>
            {abstract} +subList(int, int): List<E>
            +spliterator(): Spliterator<E>
            {static} +of(): List<E>
            {static} +of(E): List<E>
            {static} +of(E, E): List<E>
            {static} +of(E, E, E): List<E>
            {static} +of(E, E, E, E): List<E>
            {static} +of(E, E, E, E, E): List<E>
            {static} +of(E, E, E, E, E, E): List<E>
            {static} +of(E, E, E, E, E, E, E): List<E>
            {static} +of(E, E, E, E, E, E, E, E): List<E>
            {static} +of(E, E, E, E, E, E, E, E, E): List<E>
            {static} +of(E, E, E, E, E, E, E, E, E, E): List<E>
            {static} +of(E...): List<E>
            {static} +copyOf(Collection<? extends E>): List<E>
        }
    }

    namespace it.tidalwave.thesefoolishthings.examples.dci.displayable.role {
        interface Renderable [[../dci/displayable/role/Renderable.html]] {
            {static} +_Renderable_: Class<Renderable>
            {abstract} +renderTo(String, Consumer<String>): void
        }
    }

    namespace it.tidalwave.role.ui {
        interface Displayable [[../../../role/ui/Displayable.html]] {
            {static} +_Displayable_: Class<Displayable>
            {static} +DEFAULT: Displayable
            {abstract} +getDisplayName(): String
            +display(Consumer<String>): void
            {static} +of(String): Displayable
            {static} +of(String, String): Displayable
            {static} +of(Supplier<String>): Displayable
            {static} +of(Function<T, String>, T): Displayable
            {static} +fromBundle(Class<?>, String): LocalizedDisplayable
            {static} +comparing(): Comparator<Displayable>
            {static} +asComparing(): Comparator<As>
        }
    }

    namespace it.tidalwave.role {
        interface HtmlRenderable [[../../../role/HtmlRenderable.html]] {
            {static} +_HtmlRenderable_: Class<HtmlRenderable>
        }
        interface StringRenderable [[../../../role/StringRenderable.html]] {
            {static} +_StringRenderable_: Class<StringRenderable>
            {abstract} +render(Object...): String
            +renderTo(StringBuilder, Object...): void
            +renderTo(PrintWriter, Object...): void
        }
    }

    namespace it.tidalwave.thesefoolishthings.examples.dci.swing.role {
        interface TableHeaderDescriptor [[../dci/swing/role/TableHeaderDescriptor.html]] {
            {static} +_TableHeaderDescriptor_: Class<TableHeaderDescriptor>
            {abstract} +getColumnDescriptors(): List<TableColumnDescriptor>
        }
        interface ObservableListProvider [[../dci/swing/role/ObservableListProvider.html]] {
            {static} +_ObservableListProvider_: Class<ObservableListProvider>
            {abstract} +createObservableList(): ObservableList<T>
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
