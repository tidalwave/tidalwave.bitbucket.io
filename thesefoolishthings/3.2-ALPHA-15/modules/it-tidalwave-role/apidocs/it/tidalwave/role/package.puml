@startuml
    namespace it.tidalwave.role {

        interface Identifiable [[Identifiable.html]] {
            {static} +_Identifiable_: Class<Identifiable>
            {abstract} +getId(): Id
            {static} +of(Id): Identifiable
        }

        interface SimpleComposite<T> [[SimpleComposite.html]] {
            {static} +_SimpleComposite_: Class<SimpleComposite>
            {static} +of(Finder<​U>): SimpleComposite<​U>
            {static} +ofCloned(Collection<? extends U>): SimpleComposite<​U>
        }

        interface PlainTextRenderable [[PlainTextRenderable.html]] {
            {static} +_PlainTextRenderable_: Class<PlainTextRenderable>
            +renderTo(StringBuilder, Object...): void
            +renderTo(PrintWriter, Object...): void
        }

        interface ContextManager [[ContextManager.html]] {
            {abstract} +getContexts(): List<Object>
            {abstract} +findContextOfType(Class<T>): T
            {abstract} +addGlobalContext(Object): void
            {abstract} +removeGlobalContext(Object): void
            {abstract} +addLocalContext(Object): void
            {abstract} +removeLocalContext(Object): void
            +--runWithContext--(Object, Task<V, T extends Throwable>): V
            +--runWithContexts--(List<Object>, Task<V, T extends Throwable>): V
            +--runWithContext--(Object, Supplier<V>): V
            +--runWithContexts--(List<Object>, Supplier<V>): V
            +runWithContexts(Runnable, Object...): void
            +runWithContexts(Supplier<T>, Object...): T
            +runEWithContexts(RunnableWithException<E extends Throwable>, Object...): void
            {abstract} +runEWithContexts(SupplierWithException<T, E extends Throwable>, Object...): T
            +binder(Object...): Binder
        }

        class it.tidalwave.role.ContextManager.Locator [[ContextManager.Locator.html]] {
            {static} +find(): ContextManager
            {static} +set(ContextManagerProvider): void
            {static} +reset(): void
        }

        interface it.tidalwave.role.ContextManager.RunnableWithException<E extends Throwable> [[ContextManager.RunnableWithException.html]] {
            {abstract} +run(): void
        }

        interface it.tidalwave.role.ContextManager.SupplierWithException<T, E extends Throwable> [[ContextManager.SupplierWithException.html]] {
            {abstract} +get(): T
        }

        class it.tidalwave.role.ContextManager.Binder [[ContextManager.Binder.html]] {
            +close(): void
        }

        interface Removable [[Removable.html]] {
            {static} +_Removable_: Class<Removable>
            {abstract} +remove(): void
        }

        interface IdFactory [[IdFactory.html]] {
            {static} +_IdFactory_: Class<IdFactory>
            {abstract} +createId(): Id
            {abstract} +createId(Class<?>): Id
            {abstract} +createId(Class<?>, Object): Id
        }

        interface Aggregate<T> [[Aggregate.html]] {
            {static} +_Aggregate_: Class<Aggregate>
            {abstract} +getByName(String): Optional<T>
            +getNames(): Collection<String>
            {static} +of(Map<String, T>): Aggregate<T>
            {static} +newInstance(): Aggregate<T>
            {static} +of(String, T): Aggregate<T>
            +with(String, T): Aggregate<T>
        }

        interface Composite<T, F extends Finder<? extends T>> [[Composite.html]] {
            {static} +_Composite_: Class<Composite>
            {abstract} +findChildren(): F extends Finder<? extends T>
        }

        interface it.tidalwave.role.Composite.Visitor<T, R> [[Composite.Visitor.html]] {
            +preVisit(T): void
            +visit(T): void
            +postVisit(T): void
            +getValue(): Optional<R>
        }

        interface Sortable [[Sortable.html]] {
            {static} +_Sortable_: Class<Sortable>
            {abstract} +setSortCriterion(SortCriterion): void
            {abstract} +setSortDirection(SortDirection): void
            {abstract} +getSortCriterion(): SortCriterion
            {abstract} +getSortDirection(): SortDirection
        }

        interface StringRenderable [[StringRenderable.html]] {
            {static} +_StringRenderable_: Class<StringRenderable>
            {abstract} +render(Object...): String
            +renderTo(StringBuilder, Object...): void
            +renderTo(PrintWriter, Object...): void
        }

        interface HtmlRenderable [[HtmlRenderable.html]] {
            {static} +_HtmlRenderable_: Class<HtmlRenderable>
        }

        Composite <|-- SimpleComposite
        StringRenderable <|-- PlainTextRenderable
        ContextManager +-- it.tidalwave.role.ContextManager.Locator
        ContextManager +-- it.tidalwave.role.ContextManager.RunnableWithException
        ContextManager +-- it.tidalwave.role.ContextManager.SupplierWithException
        java.lang.AutoCloseable <|.. it.tidalwave.role.ContextManager.Binder
        ContextManager +-- it.tidalwave.role.ContextManager.Binder
        Removable --> Removable: DEFAULT
        Composite --> Composite: DEFAULT
        Composite +-- it.tidalwave.role.Composite.Visitor
        Sortable --> Sortable: DEFAULT
        StringRenderable <|-- HtmlRenderable
    }

    namespace java.lang {
        interface AutoCloseable {
            {abstract} +close(): void
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
