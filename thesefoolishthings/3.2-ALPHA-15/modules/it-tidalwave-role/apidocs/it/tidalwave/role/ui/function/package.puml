@startuml
    namespace it.tidalwave.role.ui.function {

        class NonEmptyFunction [[NonEmptyFunction.html]] {
            +NonEmptyFunction(ChangingSource<String>)
            {static} +nonEmpty(ChangingSource<String>): NonEmptyFunction
            #onSourceChange(String, String): void
            #function(String): Boolean
        }

        abstract class UnaryBoundFunctionSupport<DOMAIN_TYPE, CODOMAIN_TYPE> [[UnaryBoundFunctionSupport.html]] {
            #source: ChangingSource<DOMAIN_TYPE>
            #value: CODOMAIN_TYPE
            #UnaryBoundFunctionSupport(ChangingSource<DOMAIN_TYPE>)
            #onSourceChange(DOMAIN_TYPE, DOMAIN_TYPE): void
            {abstract} #function(DOMAIN_TYPE): CODOMAIN_TYPE
            +get(): CODOMAIN_TYPE
        }

        abstract class WeakCopyFunctionSupport<T> [[WeakCopyFunctionSupport.html]] {
            #targetValue: T
            +WeakCopyFunctionSupport(ChangingSource<T>)
            #onSourceChange(T, T): void
            +set(T): void
            {abstract} #shouldChange(T, T): boolean
            #function(T): T
        }

        class OrFunction [[OrFunction.html]] {
            +OrFunction(ChangingSource<Boolean>...)
            {static} +or(ChangingSource<Boolean>...): OrFunction
            #function(): boolean
        }

        abstract class BooleanBoundFunctionSupport [[BooleanBoundFunctionSupport.html]] {
            #sources: ChangingSource<Boolean>[]
            +BooleanBoundFunctionSupport(ChangingSource<Boolean>...)
            {abstract} #function(): boolean
            +get(): Boolean
        }

        class AndFunction [[AndFunction.html]] {
            +AndFunction(ChangingSource<Boolean>...)
            {static} +and(ChangingSource<Boolean>...): AndFunction
            #function(): boolean
        }

        interface BoundFunction<DOMAIN_TYPE, CODOMAIN_TYPE> [[BoundFunction.html]]

        abstract class BoundFunctionSupport<DOMAIN_TYPE, CODOMAIN_TYPE> [[BoundFunctionSupport.html]] {
            +unbindAll(): void
            #fireValueChanged(CODOMAIN_TYPE, CODOMAIN_TYPE): void
            #fireValueChanged(boolean, boolean): void
        }

        class CopyIfEmptyOrConform [[CopyIfEmptyOrConform.html]] {
            +CopyIfEmptyOrConform(ChangingSource<String>)
            {static} +copyIfEmptyOrConform(ChangingSource<String>): CopyIfEmptyOrConform
            #shouldChange(String, String): boolean
        }

        UnaryBoundFunctionSupport <|-- NonEmptyFunction
        BoundFunctionSupport <|-- UnaryBoundFunctionSupport
        UnaryBoundFunctionSupport <|-- WeakCopyFunctionSupport
        it.tidalwave.role.ui.Changeable <|.. WeakCopyFunctionSupport
        BooleanBoundFunctionSupport <|-- OrFunction
        BoundFunctionSupport <|-- BooleanBoundFunctionSupport
        BooleanBoundFunctionSupport <|-- AndFunction
        it.tidalwave.role.ui.ChangingSource <|-- BoundFunction
        BoundFunction <|.. BoundFunctionSupport
        WeakCopyFunctionSupport <|-- CopyIfEmptyOrConform
    }

    namespace it.tidalwave.role.ui {
        interface Changeable<T> [[../Changeable.html]] {
            {abstract} +set(T): void
        }
        interface ChangingSource<T> [[../ChangingSource.html]] {
            {abstract} +getPropertyChangeListeners(): PropertyChangeListener[]
            {abstract} +addPropertyChangeListener(PropertyChangeListener): void
            {abstract} +removePropertyChangeListener(PropertyChangeListener): void
            {abstract} +unbindAll(): void
            {abstract} +get(): T
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
