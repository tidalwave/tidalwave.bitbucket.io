@startuml
    namespace it.tidalwave.actor {

        abstract class MessageSupport [[MessageSupport.html]] {
            #MessageSupport()
            #MessageSupport(Collaboration)
            +send(): Collaboration
            +sendDirectly(): Collaboration
            +sendLater(int, TimeUnit): Collaboration
            +maybeAs(Class<? extends T>): Optional<T>
        }

        interface MessageDecorator [[MessageDecorator.html]] {
            {static} +_MessageDecorator_: Class<MessageDecorator>
            {abstract} +getDecoratedMessage(): MessageSupport
        }

        class it.tidalwave.actor.MessageDecorator.Same<T extends MessageSupport> [[MessageDecorator.Same.html]] {
        }

        interface Collaboration [[Collaboration.html]] {
            {abstract} +getOriginatingMessage(): Object
            {abstract} +isCompleted(): boolean
            {abstract} +waitForCompletion(): void
            {abstract} +getStartTime(): ZonedDateTime
            {abstract} +getDuration(): Duration
            {abstract} +suspend(): Object
            {abstract} +resume(Object, Runnable): void
            {abstract} +resumeAndDie(Object): void
            {abstract} +isSuspended(): boolean
            {abstract} +getDeliveringMessagesCount(): int
            {abstract} +getPendingMessagesCount(): int
            {abstract} +getRunningThreadsCount(): int
        }

        interface it.tidalwave.actor.Collaboration.Provider [[Collaboration.Provider.html]] {
            {abstract} +getCollaboration(): Collaboration
        }

        class CollaborationCompletedMessage [[CollaborationCompletedMessage.html]] {
            {static} +forCollaboration(Collaboration): CollaborationCompletedMessage
            +getStartTime(): ZonedDateTime
            +getEndTime(): ZonedDateTime
            +getDuration(): Duration
        }

        class CollaborationStartedMessage [[CollaborationStartedMessage.html]] {
            {static} +forCollaboration(Collaboration): CollaborationStartedMessage
            +getStartTime(): ZonedDateTime
        }

        it.tidalwave.actor.Collaboration.Provider <|.. MessageSupport
        it.tidalwave.util.As <|.. MessageSupport
        java.io.Serializable <|.. MessageSupport
        MessageSupport --> it.tidalwave.actor.impl.DefaultCollaboration: collaboration
        MessageSupport --> Collaboration: collaboration
        MessageDecorator <|.. it.tidalwave.actor.MessageDecorator.Same
        MessageDecorator +-- it.tidalwave.actor.MessageDecorator.Same
        Collaboration --> Collaboration: NULL_COLLABORATION
        Collaboration +-- it.tidalwave.actor.Collaboration.Provider
        MessageSupport <|-- CollaborationCompletedMessage
        MessageSupport <|-- CollaborationStartedMessage
    }

    namespace it.tidalwave.util {
        interface As [[https://tidalwave.bitbucket.io/thesefoolishthings/modules/it-tidalwave-util/apidocs/it/tidalwave/util/As.html?is-external=true]] {
            {static} +forObject(Object): As
            {static} +forObject(Object, Object): As
            {static} +forObject(Object, Collection<Object>): As
            +as(Class<? extends T>): T
            {abstract} +maybeAs(Class<? extends T>): Optional<T>
            {abstract} +asMany(Class<? extends T>): Collection<T>
            {static} +type(Class<?>): Type<T>
            +as(Type<? extends T>): T
            +maybeAs(Type<? extends T>): Optional<T>
            +asMany(Type<? extends T>): Collection<T>
        }
    }

    namespace java.io {
        interface Serializable
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
