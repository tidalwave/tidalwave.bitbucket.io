@startuml
    namespace it.tidalwave.util.test {

        class FileComparisonUtils [[FileComparisonUtils.html]] {
            {static} +P_TABULAR_OUTPUT: String
            {static} +P_TABULAR_LIMIT: String
            {static} +assertSameContents(File, File): void
            {static} +assertSameContents(Path, Path): void
            {static} +assertSameContents(List<String>, List<String>): void
            {static} +checkSameContents(Path, Path): boolean
            {static} +stringToStrings(String): List<String>
            {static} +fileToStrings(Path): List<String>
            {static} +resourceToStrings(String): List<String>
            {static} +resourceToStrings(InputStream): List<String>
            {static} +commonPrefix(String, String): String
        }

        class TestLogger [[TestLogger.html]] {
            +onStart(ITestContext): void
            +onFinish(ITestContext): void
            +onConfigurationSkip(ITestResult): void
            +onConfigurationFailure(ITestResult): void
            +onTestStart(ITestResult): void
            +onTestFailure(ITestResult): void
            +onTestFailedButWithinSuccessPercentage(ITestResult): void
            +onTestSkipped(ITestResult): void
            +onTestSuccess(ITestResult): void
        }

        class MockTimeProvider [[MockTimeProvider.html]] {
            +currentInstant(): Instant
        }

        class SpringTestHelper [[SpringTestHelper.html]] {
            +SpringTestHelper(Object)
            +createSpringContext(String...): ApplicationContext
            +createSpringContext(Map<String, Object>, String...): ApplicationContext
            +createSpringContext(Consumer<? super GenericApplicationContext>, String...): ApplicationContext
            +createSpringContext(Map<String, Object>, Consumer<? super GenericApplicationContext>, String...): ApplicationContext
        }

        class BaseTestHelper [[BaseTestHelper.html]] {
            #test: Object
            +resourceFileFor(String): Path
            +readStringFromResource(String): String
            +testResourceFor(String): TestResource
        }

        class it.tidalwave.util.test.BaseTestHelper.TestResource [[BaseTestHelper.TestResource.html]] {
            +assertActualFileContentSameAsExpected(): void
            +writeToActualFile(String...): void
            +writeToActualFile(Iterable<String>): void
            +writeToActualFile(byte[]): void
            +readStringFromResource(): String
        }

        class MoreAnswers [[MoreAnswers.html]] {
            {static} +CALLS_DEFAULT_METHODS: Answer<?>
        }

        org.testng.TestListenerAdapter <|-- TestLogger
        it.tidalwave.util.TimeProvider <|.. MockTimeProvider
        BaseTestHelper <|-- SpringTestHelper
        BaseTestHelper +-- it.tidalwave.util.test.BaseTestHelper.TestResource
    }

    namespace org.testng {
        class TestListenerAdapter {
        }
    }

    namespace it.tidalwave.util {
        interface TimeProvider [[../TimeProvider.html]] {
            {static} +__INSTANCE: AtomicReference<TimeProvider>
            {abstract} +currentInstant(): Instant
            +get(): Instant
            +currentZonedDateTime(): ZonedDateTime
            +currentLocalDateTime(): LocalDateTime
            {static} +getInstance(): TimeProvider
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
