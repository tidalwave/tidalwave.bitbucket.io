@startuml
    namespace it.tidalwave.actor {

        class CollaborationStartedMessage [[CollaborationStartedMessage.html]] {
            {static} +forCollaboration(Collaboration): CollaborationStartedMessage
            +getStartTime(): DateTime
        }

        interface MessageDecorator [[MessageDecorator.html]] {
            {static} +_MessageDecorator_: Class<MessageDecorator>
            {abstract} +getDecoratedMessage(): MessageSupport
        }

        class it.tidalwave.actor.MessageDecorator.Same<T extends MessageSupport> [[MessageDecorator.Same.html]] {
        }

        class CollaborationCompletedMessage [[CollaborationCompletedMessage.html]] {
            {static} +forCollaboration(Collaboration): CollaborationCompletedMessage
            +getStartTime(): DateTime
            +getEndTime(): DateTime
            +getDuration(): Duration
        }

        interface Collaboration [[Collaboration.html]] {
            {abstract} +getOriginatingMessage(): Object
            {abstract} +isCompleted(): boolean
            {abstract} +waitForCompletion(): void
            {abstract} +getStartTime(): DateTime
            {abstract} +getDuration(): Duration
            {abstract} +suspend(): Object
            {abstract} +resume(Object, Runnable): void
            {abstract} +resumeAndDie(Object): void
            {abstract} +isSuspended(): boolean
            {abstract} +getDeliveringMessagesCount(): int
            {abstract} +getPendingMessagesCount(): int
            {abstract} +getRunningThreadsCount(): int
        }

        interface it.tidalwave.actor.Collaboration.Provider [[Collaboration.Provider.html]] {
            {abstract} +getCollaboration(): Collaboration
        }

        abstract class MessageSupport [[MessageSupport.html]] {
            #MessageSupport()
            #MessageSupport(Collaboration)
            +send(): Collaboration
            +sendDirectly(): Collaboration
            +sendLater(int, TimeUnit): Collaboration
            +as(Class<T>): T
        }

        MessageSupport <|-- CollaborationStartedMessage
        MessageDecorator <|.. it.tidalwave.actor.MessageDecorator.Same
        MessageDecorator +-- it.tidalwave.actor.MessageDecorator.Same
        MessageSupport <|-- CollaborationCompletedMessage
        Collaboration --> Collaboration: NULL_COLLABORATION
        Collaboration +-- it.tidalwave.actor.Collaboration.Provider
        it.tidalwave.actor.Collaboration.Provider <|.. MessageSupport
        it.tidalwave.util.As <|.. MessageSupport
        java.io.Serializable <|.. MessageSupport
        MessageSupport --> it.tidalwave.actor.impl.DefaultCollaboration: collaboration
        MessageSupport --> Collaboration: collaboration
    }

    namespace it.tidalwave.util {
        interface As [[../util/As.html]] {
            {static} +forObject(Object): As
            {static} +forObject(Object, Object): As
            {static} +forObject(Object, Collection<Object>): As
            +as(Class<T>): T
            +--as--(Class<T>, NotFoundBehaviour<T>): T
            {abstract} +maybeAs(Class<T>): Optional<T>
            {abstract} +asMany(Class<T>): Collection<T>
            {static} +type(Class<?>): Type<T>
            +as(Type<T>): T
            +maybeAs(Type<T>): Optional<T>
            +asMany(Type<T>): Collection<T>
        }
    }

    namespace java.io {
        interface Serializable
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
