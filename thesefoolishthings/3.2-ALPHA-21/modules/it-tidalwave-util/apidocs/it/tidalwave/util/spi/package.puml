@startuml
    namespace it.tidalwave.util.spi {

        class FinderWithIdMapSupport<T, I extends T, F extends ExtendedFinderSupport<T, F>> [[FinderWithIdMapSupport.html]] {
            +FinderWithIdMapSupport()
            +FinderWithIdMapSupport(FinderWithIdMapSupport<T, I extends T, F extends ExtendedFinderSupport<T, F>>, Object)
            #findAll(): List<T>
            #findById(Id): Optional<T>
        }

        abstract class ArrayListCollectorSupport<COLLECTED_TYPE, COLLECTING_TYPE> [[ArrayListCollectorSupport.html]] {
            +supplier(): Supplier<List<COLLECTED_TYPE>>
            +accumulator(): BiConsumer<List<COLLECTED_TYPE>, COLLECTED_TYPE>
            +combiner(): BinaryOperator<List<COLLECTED_TYPE>>
            +characteristics(): Set<Characteristics>
        }

        class HierarchicFinderSupport<T, F extends Finder<T>> [[HierarchicFinderSupport.html]] {
            #firstResult: int
            #maxResults: int
            #HierarchicFinderSupport(String)
            #HierarchicFinderSupport()
            #HierarchicFinderSupport(HierarchicFinderSupport<T, F extends Finder<T>>, Object)
            #clonedWith(Object): F extends Finder<T>
            #--clone--(Object): F extends Finder<T>
            +from(int): F extends Finder<T>
            +max(int): F extends Finder<T>
            +withContext(Object): F extends Finder<T>
            +ofType(Class<​U>): Finder<​U>
            +sort(SortCriterion, SortDirection): F extends Finder<T>
            +sort(SortCriterion): F extends Finder<T>
            +results(): List<T>
            +count(): int
            #computeResults(): List<T>
            #computeNeededResults(): List<T>
            {static} #getSource(Class<? extends U>, U, Object): U
        }

        class DefaultProcessExecutor [[DefaultProcessExecutor.html]] {
            {static} +forExecutable(String): DefaultProcessExecutor
            +withArgument(String): DefaultProcessExecutor
            +withArguments(String...): DefaultProcessExecutor
            +start(): DefaultProcessExecutor
            +stop(): void
            +waitForCompletion(): DefaultProcessExecutor
            +send(String): DefaultProcessExecutor
        }

        class it.tidalwave.util.spi.DefaultProcessExecutor.DefaultConsoleOutput [[DefaultProcessExecutor.DefaultConsoleOutput.html]] {
            +start(): ConsoleOutput
            +latestLineMatches(String): boolean
            +filteredAndSplitBy(String, String): Scanner
            +filteredBy(String): List<String>
            +waitFor(String): ConsoleOutput
            +clear(): void
        }

        interface ExtendedFinderSupport<T, F extends Finder<T>> [[ExtendedFinderSupport.html]] {
            {abstract} +from(int): F extends Finder<T>
            {abstract} +max(int): F extends Finder<T>
            {abstract} +sort(SortCriterion): F extends Finder<T>
            {abstract} +sort(SortCriterion, SortDirection): F extends Finder<T>
            {abstract} +withContext(Object): F extends Finder<T>
        }

        interface FinderWithId<T, F extends ExtendedFinderSupport<T, F>> [[FinderWithId.html]] {
            {abstract} +withId(Id): F extends ExtendedFinderSupport<T, F extends ExtendedFinderSupport<T,F>>
        }

        class FinderWithIdSupport<T, I extends T, F extends ExtendedFinderSupport<T, F>> [[FinderWithIdSupport.html]] {
            +FinderWithIdSupport()
            +FinderWithIdSupport(FinderWithIdSupport<T, I extends T, F extends ExtendedFinderSupport<T, F>>, Object)
            +withId(Id): F extends ExtendedFinderSupport<T, F extends ExtendedFinderSupport<T,F>>
            #computeResults(): List<T>
            #findById(Id): Optional<T>
            #findAll(): List<T>
            #streamImpl(): Stream<I extends T>
        }

        abstract class SimpleFinderSupport<T> [[SimpleFinderSupport.html]] {
            #SimpleFinderSupport(String)
            #SimpleFinderSupport(SimpleFinderSupport<T>, Object)
        }

        FinderWithIdSupport <|-- FinderWithIdMapSupport
        java.util.stream.Collector <|.. ArrayListCollectorSupport
        it.tidalwave.util.Finder <|.. HierarchicFinderSupport
        it.tidalwave.util.ProcessExecutor <|.. DefaultProcessExecutor
        it.tidalwave.util.ProcessExecutor.ConsoleOutput <|.. it.tidalwave.util.spi.DefaultProcessExecutor.DefaultConsoleOutput
        DefaultProcessExecutor +-- it.tidalwave.util.spi.DefaultProcessExecutor.DefaultConsoleOutput
        it.tidalwave.util.Finder <|-- ExtendedFinderSupport
        it.tidalwave.util.Finder <|-- FinderWithId
        ExtendedFinderSupport <|-- FinderWithId
        HierarchicFinderSupport <|-- FinderWithIdSupport
        FinderWithId <|.. FinderWithIdSupport
        HierarchicFinderSupport <|-- SimpleFinderSupport
    }

    namespace java.util.stream {
        interface Collector<T, A, R> {
            {abstract} +supplier(): Supplier<A>
            {abstract} +accumulator(): BiConsumer<A, T>
            {abstract} +combiner(): BinaryOperator<A>
            {abstract} +finisher(): Function<A, R>
            {abstract} +characteristics(): Set<Characteristics>
            {static} +of(Supplier<R>, BiConsumer<R, T>, BinaryOperator<R>, Characteristics...): Collector<T, R, R>
            {static} +of(Supplier<A>, BiConsumer<A, T>, BinaryOperator<A>, Function<A, R>, Characteristics...): Collector<T, A, R>
        }
    }

    namespace it.tidalwave.util {
        interface Finder<T> [[../Finder.html]] {
            {abstract} +from(int): Finder<T>
            +from(Optional<Integer>): Finder<T>
            {abstract} +max(int): Finder<T>
            +max(Optional<Integer>): Finder<T>
            +withContext(Object): Finder<T>
            +ofType(Class<​U>): Finder<​U>
            +sort(SortCriterion): Finder<T>
            {abstract} +sort(SortCriterion, SortDirection): Finder<T>
            {abstract} +results(): List<T>
            {abstract} +count(): int
            +optionalResult(): Optional<T>
            +optionalFirstResult(): Optional<T>
            +stream(): Stream<T>
            +iterator(): Iterator<T>
            +--result--(): T
            +--firstResult--(): T
            {static} +empty(): Finder<​U>
            {static} +ofCloned(Collection<? extends U>): Finder<​U>
            {static} +ofSupplier(Supplier<? extends Collection<? extends U>>): Finder<​U>
            {static} +ofProvider(BiFunction<Integer, Integer, ? extends Collection<? extends U>>): Finder<​U>
            {static} +mapping(Finder<V>, Function<? super V, ? extends U>): Finder<​U>
        }
        interface ProcessExecutor [[../ProcessExecutor.html]] {
            {abstract} +withArguments(String...): ProcessExecutor
            {abstract} +withArgument(String): ProcessExecutor
            {abstract} +start(): ProcessExecutor
            {abstract} +stop(): void
            {abstract} +waitForCompletion(): ProcessExecutor
            {abstract} +send(String): ProcessExecutor
            {abstract} +getStdout(): ConsoleOutput
            {abstract} +getStderr(): ConsoleOutput
        }
        interface it.tidalwave.util.ProcessExecutor.ConsoleOutput [[../ProcessExecutor.ConsoleOutput.html]] {
            {abstract} +latestLineMatches(String): boolean
            {abstract} +filteredBy(String): List<String>
            {abstract} +filteredAndSplitBy(String, String): Scanner
            {abstract} +waitFor(String): ConsoleOutput
            {abstract} +clear(): void
            {abstract} +setListener(Listener): void
            {abstract} +getListener(): Listener
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
