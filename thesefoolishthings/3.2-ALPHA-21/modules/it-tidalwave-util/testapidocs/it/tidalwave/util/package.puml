@startuml
    namespace it.tidalwave.util {

        class KeyTest [[KeyTest.html]] {
            +test(): void
            +must_return_the_correct_dynamic_type(Key<T>, Class<T>): void
        }

        class ShortNamesTest [[ShortNamesTest.html]] {
            +test_shortName(): void
            +test_shortNames(): void
            +test_shortName_expand_interface(): void
            +test_shortId_1(): void
            +test_shortId_2(): void
            +test_shortId_withNull(): void
            +test_shortIds(): void
        }

        class ParameterTest [[ParameterTest.html]] {
            +test(): void
        }

        class BundleUtilitiesTest [[BundleUtilitiesTest.html]] {
            +test(Locale, String, Object[], String): void
        }

        class ConcurrentHashMapWithOptionalsTest [[ConcurrentHashMapWithOptionalsTest.html]] {
            +setup(): void
            +must_return_the_new_key_when_a_new_pair_is_put_1(): void
            +must_return_empty_Optional_when_no_new_pair_is_put_1(): void
            +must_return_the_new_key_when_a_new_pair_is_put_2(): void
            +must_return_empty_Optional_when_no_new_pair_is_put_2(): void
            +must_return_the_new_key_when_a_new_pair_is_put_3(): void
            +must_return_empty_Optional_when_no_new_pair_is_put_3(): void
        }

        class IdTest [[IdTest.html]] {
            +setUp(): void
            +testStringValue(): void
            +testEquals(): void
            +testHashCode(): void
            +testCompareTo(): void
            +testToString(): void
        }

        class CollectionUtilsTest [[CollectionUtilsTest.html]] {
            +test_concat(): void
            +test_concat_list(): void
            +test_reversed(): void
            +test_optionalHead(): void
            +test_optionalHead_with_empty_list(): void
            +test_head(): void
            +test_head_with_empty_list(): void
            +test_tail(): void
            +test_sorted(): void
            +test_sorted_with_comparator(): void
            +test_safeSubList(): void
            +test_split(): void
        }

        class TypeSafeMultiMapTest [[TypeSafeMultiMapTest.html]] {
            {static} +LOCAL_DATE: LocalDateTime
            +test_newInstance(): void
            +test_ofCloned(): void
            +test_with_from_empty_map(): void
            +test_with(): void
            +test_with_and_existing_key(): void
            +asMap_must_return_different_mutable_instances_detached_from_internal_state(): void
            +getKeys_must_return_different_mutable_instances_detached_from_internal_state(): void
            +test_forEach(): void
            +codeSamples(): void
        }

        class AsExtensionsTest [[AsExtensionsTest.html]] {
            +setupTerminal(): void
            +test1(): void
            +test2(): void
            +test3(): void
        }

        class FinderTest [[FinderTest.html]] {
            +ofClone_must_behave_correctly(): void
            +ofClone_result_must_be_a_modifiable_list(): void
            +test_Finder_ofSupplier(): void
            +test_Finder_ofProvider(): void
            +test_Finder_mapping(): void
            +test1(): void
            +test2(): void
        }

        class PairTest [[PairTest.html]] {
            +test_Pair(): void
            +test_pairRange(): void
            +test_pairRangeClosed(): void
            +testDoubleNestedLoops(): void
            +test_indexedPairStream_from_array(): void
            +test_indexedPairStream_from_array_and_rebaser(): void
            +test_indexedPairStream__from_array_with_index_transformer(): void
            +test_indexedPairStream_from_list_as_iterable(): void
            +test_indexedPairStream_from_list_as_iterable_and_rebaser(): void
            +test_indexedPairStream__from_list_as_iterable_with_index_transformer(): void
            +test_indexedPairStream_from_stream(): void
            +test_indexedPairStream_from_stream_and_rebaser(): void
            +test_indexedPairStream__from_stream_with_index_transformer(): void
            +test_indexedPairStream_with_range_and_supplier(): void
            +test_indexedPairStream_with_range_and_supplier_and_rebaser(): void
            +test_indexedPairStream_with_range_and_supplier_and_index_transformer(): void
            +test_collector_to_map(): void
            +test_pairStream(): void
            +zipPairTest1(): void
        }

        class ReflectionUtilsTest [[ReflectionUtilsTest.html]] {
            +test_instantiateWithDependencies(): void
            +test_injectDependencies(): void
            +test_getClass(Type, Class<?>): void
            {static} +dataProvider(): Object[]
        }

        class FunctionalCheckedExceptionWrappersTest [[FunctionalCheckedExceptionWrappersTest.html]] {
            +test_function_wrapper(): void
            +test_function_wrapper_with_exception(): void
            +test_consumer_wrapper(): void
            +test_consumer_wrapper_with_exception(): void
            +test_supplier_wrapper(): void
            +test_supplier_wrapper_with_exception(): void
            +test_predicate_wrapper(): void
            +test_predicate_wrapper_with_exception(): void
            +test_with_Stream(): void
            +test_with_Stream_with_exception(): void
            +must_not_wrap_RuntimeException(): void
            +must_wrap_checked_exceptions(): void
            +must_wrap_IOException_with_UncheckedIOException(): void
        }

        class TypeSafeMapTest [[TypeSafeMapTest.html]] {
            {static} +LOCAL_DATE: LocalDateTime
            +test_newInstance(): void
            +test_ofCloned(): void
            +test_with(): void
            +asMap_must_return_different_mutable_instances_detached_from_internal_state(): void
            +getKeys_must_return_different_mutable_instances_detached_from_internal_state(): void
            +codeSamples(): void
            +test_forEach(): void
        }

        class AsTest [[AsTest.html]] {
            +must_return_a_filled_Optional_when_the_role_is_present(): void
            +must_return_an_empty_Optional_when_the_role_is_not_present(): void
            +as_with_ref_must_properly_work(): void
            +maybeAs_with_ref_must_properly_work(): void
            +asMany_with_ref_must_properly_work(): void
        }

        class TripleTest [[TripleTest.html]] {
            +test_Triple(): void
            +test_tripleStream(): void
            +test_tripleRange(): void
            +test_tripleRangeClosed(): void
            +testTripleNestedLoops(): void
        }

        class TimeProviderTest [[TimeProviderTest.html]] {
            +test(): void
        }

        class StreamUtilsTest [[StreamUtilsTest.html]] {
            +zipTest1(): void
            +test_randomLocalDateTimeStream(long, LocalDateTime, LocalDateTime, List<LocalDateTime>): void
        }

        class LazySupplierTest [[LazySupplierTest.html]] {
            +must_not_call_supplier_before_get(): void
            +must_call_supplier_only_once(): void
            +must_call_supplier_only_once_multithreaded(): void
        }

        class LocalizedDateTimeFormattersTest [[LocalizedDateTimeFormattersTest.html]] {
            +must_properly_format_date_and_time(ZonedDateTime, Locale, FormatStyle, String): void
        }

    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
