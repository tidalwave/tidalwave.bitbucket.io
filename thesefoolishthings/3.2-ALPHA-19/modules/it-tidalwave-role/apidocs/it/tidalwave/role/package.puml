@startuml
    namespace it.tidalwave.role {

        interface Identifiable [[Identifiable.html]] {
            {static} +_Identifiable_: Class<Identifiable>
            {abstract} +getId(): Id
            {static} +of(Id): Identifiable
        }

        interface SimpleComposite<T> [[SimpleComposite.html]] {
            {static} +_SimpleComposite_: Class<SimpleComposite>
            {static} +of(Finder<​U>): SimpleComposite<​U>
            {static} +ofCloned(Collection<? extends U>): SimpleComposite<​U>
        }

        interface PlainTextRenderable [[PlainTextRenderable.html]] {
            {static} +_PlainTextRenderable_: Class<PlainTextRenderable>
            +renderTo(StringBuilder, Object...): void
            +renderTo(PrintWriter, Object...): void
        }

        interface Removable [[Removable.html]] {
            {static} +_Removable_: Class<Removable>
            {abstract} +remove(): void
        }

        interface IdFactory [[IdFactory.html]] {
            {static} +_IdFactory_: Class<IdFactory>
            {abstract} +createId(): Id
            +createId(Class<?>): Id
            +createId(Class<?>, Object): Id
        }

        class it.tidalwave.role.IdFactory.Private [[IdFactory.Private.html]] {
        }

        interface Aggregate<T> [[Aggregate.html]] {
            {static} +_Aggregate_: Class<Aggregate>
            {abstract} +getByName(String): Optional<T>
            +getNames(): Collection<String>
            {static} +of(Map<String, T>): Aggregate<T>
            {static} +newInstance(): Aggregate<T>
            {static} +of(String, T): Aggregate<T>
            +with(String, T): Aggregate<T>
        }

        interface Composite<T, F extends Finder<? extends T>> [[Composite.html]] {
            {static} +_Composite_: Class<Composite>
            {abstract} +findChildren(): F extends Finder<? extends T>
        }

        interface it.tidalwave.role.Composite.Visitor<T, R> [[Composite.Visitor.html]] {
            +preVisit(T): void
            +visit(T): void
            +postVisit(T): void
            +getValue(): Optional<R>
        }

        interface Sortable [[Sortable.html]] {
            {static} +_Sortable_: Class<Sortable>
            {abstract} +setSortCriterion(SortCriterion): void
            {abstract} +setSortDirection(SortDirection): void
            {abstract} +getSortCriterion(): SortCriterion
            {abstract} +getSortDirection(): SortDirection
        }

        interface StringRenderable [[StringRenderable.html]] {
            {static} +_StringRenderable_: Class<StringRenderable>
            {abstract} +render(Object...): String
            +renderTo(StringBuilder, Object...): void
            +renderTo(PrintWriter, Object...): void
        }

        interface HtmlRenderable [[HtmlRenderable.html]] {
            {static} +_HtmlRenderable_: Class<HtmlRenderable>
        }

        Composite <|-- SimpleComposite
        StringRenderable <|-- PlainTextRenderable
        Removable --> Removable: DEFAULT
        IdFactory --> IdFactory: DEFAULT\nMOCK
        IdFactory +-- it.tidalwave.role.IdFactory.Private
        Composite --> Composite: DEFAULT
        Composite +-- it.tidalwave.role.Composite.Visitor
        Sortable --> Sortable: DEFAULT
        StringRenderable <|-- HtmlRenderable
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
