@startuml
    namespace it.tidalwave.role.ui.spi {

        class DefaultUserActionProvider [[DefaultUserActionProvider.html]] {
            +getActions(): Collection<? extends UserAction>
            +getDefaultAction(): UserAction
        }

        class ActionProviderSupport [[ActionProviderSupport.html]] {
            +getActions(): Collection<? extends Action>
            +getDefaultAction(): Action
        }

        class PresentationModelCollectors [[PresentationModelCollectors.html]] {
            {static} +toCompositePresentationModel(Collection<Object>): PresentationModelCollectors
            {static} +toCompositePresentationModel(): PresentationModelCollectors
            {static} +toCompositePresentationModel(Iterable<? extends As>, Function<? super As, Object>): PresentationModel
            {static} +toCompositePresentationModel(Iterable<T extends As>): PresentationModel
            +finisher(): Function<List<PresentationModel>, PresentationModel>
        }

        class SimpleCompositePresentable [[SimpleCompositePresentable.html]] {
            +SimpleCompositePresentable(As)
            +SimpleCompositePresentable(As, PresentationModelFactory)
            +createPresentationModel(Collection<Object>): PresentationModel
        }

        class DefaultPresentationModelFactory [[DefaultPresentationModelFactory.html]] {
            +createPresentationModel(Object, Collection<Object>): PresentationModel
        }

        abstract class MutableIconProviderSupport [[MutableIconProviderSupport.html]] {
            +addPropertyChangeListener(PropertyChangeListener): void
            +removePropertyChangeListener(PropertyChangeListener): void
            +setIcon(Icon): void
            #fireIconChange(Icon, Icon): void
        }

        it.tidalwave.role.ui.UserActionProvider <|.. DefaultUserActionProvider
        it.tidalwave.role.ui.ActionProvider <|.. ActionProviderSupport
        it.tidalwave.util.spi.ArrayListCollectorSupport <|-- PresentationModelCollectors
        it.tidalwave.role.ui.Presentable <|.. SimpleCompositePresentable
        it.tidalwave.role.ui.PresentationModelFactory <|.. DefaultPresentationModelFactory
        it.tidalwave.role.ui.MutableIconProvider <|.. MutableIconProviderSupport
    }

    namespace it.tidalwave.role.ui {
        interface UserActionProvider [[../UserActionProvider.html]] {
            {static} +_UserActionProvider_: Class<UserActionProvider>
            {abstract} +getActions(): Collection<? extends UserAction>
            {abstract} +--getDefaultAction--(): UserAction
            +getOptionalDefaultAction(): Optional<UserAction>
            {static} +of(UserAction...): UserActionProvider
        }
        interface ActionProvider [[../ActionProvider.html]] {
            {static} +_ActionProvider_: Class<ActionProvider>
            {abstract} +getActions(): Collection<? extends Action>
            {abstract} +getDefaultAction(): Action
        }
        interface Presentable [[../Presentable.html]] {
            {static} +_Presentable_: Class<Presentable>
            +createPresentationModel(): PresentationModel
            {abstract} +createPresentationModel(Collection<Object>): PresentationModel
            +createPresentationModel(Object): PresentationModel
            {static} +of(Object): Presentable
        }
        interface PresentationModelFactory [[../PresentationModelFactory.html]] {
            {static} +_PresentationModelFactory_: Class<PresentationModelFactory>
            {abstract} +createPresentationModel(Object, Collection<Object>): PresentationModel
            +createPresentationModel(Object): PresentationModel
        }
        interface MutableIconProvider [[../MutableIconProvider.html]] {
            {static} +_MutableIconProvider_: Class<MutableIconProvider>
            {static} +PROP_ICON: String
            {abstract} +setIcon(Icon): void
            {abstract} +addPropertyChangeListener(PropertyChangeListener): void
            {abstract} +removePropertyChangeListener(PropertyChangeListener): void
        }
    }

    namespace it.tidalwave.util.spi {
        abstract class ArrayListCollectorSupport<COLLECTED_TYPE, COLLECTING_TYPE> [[../../../util/spi/ArrayListCollectorSupport.html]]
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
