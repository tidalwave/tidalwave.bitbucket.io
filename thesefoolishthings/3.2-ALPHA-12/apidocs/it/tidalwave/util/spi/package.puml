@startuml
    namespace it.tidalwave.util.spi {

        class AsSupport [[AsSupport.html]] {
            #AsSupport()
            +AsSupport(Object)
            +AsSupport(Object, Object)
            +AsSupport(Object, Collection<Object>)
            +as(Class<T>): T
            +as(Class<T>, NotFoundBehaviour<T>): T
            +asMany(Class<T>): Collection<T>
        }

        interface AsDelegateProvider [[AsDelegateProvider.html]] {
            {static} +_AsDelegateProvider_: Class<AsDelegateProvider>
            {abstract} +createAsDelegate(Object): AsDelegate
            {static} +empty(): AsDelegateProvider
        }

        class it.tidalwave.util.spi.AsDelegateProvider.Locator [[AsDelegateProvider.Locator.html]] {
            {static} +find(): AsDelegateProvider
            {static} +set(AsDelegateProvider): void
            {static} +reset(): void
        }

        class it.tidalwave.util.spi.AsDelegateProvider.EmptyAsDelegateProvider [[AsDelegateProvider.EmptyAsDelegateProvider.html]] {
            +createAsDelegate(Object): AsDelegate
        }

        interface AsDelegate [[AsDelegate.html]] {
            {abstract} +as(Class<T>): Collection<? extends T>
        }

        class FinderSupport<TYPE, EXTENDED_FINDER extends Finder<TYPE>> [[FinderSupport.html]] {
            #FinderSupport(String)
            #FinderSupport()
            #FinderSupport(FinderSupport<TYPE, EXTENDED_FINDER extends Finder<TYPE>>, Object)
            #clonedWith(Object): EXTENDED_FINDER extends Finder<TYPE>
            #--clone--(Object): EXTENDED_FINDER extends Finder<TYPE>
            +from(int): EXTENDED_FINDER extends Finder<TYPE>
            +max(int): EXTENDED_FINDER extends Finder<TYPE>
            +withContext(Object): EXTENDED_FINDER extends Finder<TYPE>
            +ofType(Class<ANOTHER_TYPE>): Finder<ANOTHER_TYPE>
            +sort(SortCriterion, SortDirection): EXTENDED_FINDER extends Finder<TYPE>
            +sort(SortCriterion): EXTENDED_FINDER extends Finder<TYPE>
            +results(): List<? extends TYPE>
            +count(): int
            #computeResults(): List<? extends TYPE>
            #computeNeededResults(): List<? extends TYPE>
            {static} #getSource(Class<T>, T, Object): T
        }

        class DefaultProcessExecutor [[DefaultProcessExecutor.html]] {
            {static} +forExecutable(String): DefaultProcessExecutor
            +withArgument(String): DefaultProcessExecutor
            +withArguments(String...): DefaultProcessExecutor
            +start(): DefaultProcessExecutor
            +stop(): void
            +waitForCompletion(): DefaultProcessExecutor
            +send(String): DefaultProcessExecutor
        }

        class it.tidalwave.util.spi.DefaultProcessExecutor.DefaultConsoleOutput [[DefaultProcessExecutor.DefaultConsoleOutput.html]] {
            +start(): ConsoleOutput
            +latestLineMatches(String): boolean
            +filteredAndSplitBy(String, String): Scanner
            +filteredBy(String): List<String>
            +waitFor(String): ConsoleOutput
            +clear(): void
        }

        interface ExtendedFinderSupport<TYPE, EXTENDED_FINDER extends Finder<TYPE>> [[ExtendedFinderSupport.html]] {
            {abstract} +from(int): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +max(int): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +sort(SortCriterion): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +sort(SortCriterion, SortDirection): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +withContext(Object): EXTENDED_FINDER extends Finder<TYPE>
        }

        abstract class ArrayListCollectorSupport<COLLECTED_TYPE, COLLECTING_TYPE> [[ArrayListCollectorSupport.html]] {
            +supplier(): Supplier<List<COLLECTED_TYPE>>
            +accumulator(): BiConsumer<List<COLLECTED_TYPE>, COLLECTED_TYPE>
            +combiner(): BinaryOperator<List<COLLECTED_TYPE>>
            +characteristics(): Set<Characteristics>
        }

        abstract class SimpleFinderSupport<T> [[SimpleFinderSupport.html]] {
            #SimpleFinderSupport(String)
            #SimpleFinderSupport(SimpleFinderSupport<T>, Object)
        }

        it.tidalwave.util.As <|.. AsSupport
        AsDelegateProvider +-- it.tidalwave.util.spi.AsDelegateProvider.Locator
        AsDelegateProvider <|.. it.tidalwave.util.spi.AsDelegateProvider.EmptyAsDelegateProvider
        AsDelegateProvider +-- it.tidalwave.util.spi.AsDelegateProvider.EmptyAsDelegateProvider
        it.tidalwave.util.Finder <|.. FinderSupport
        it.tidalwave.util.ProcessExecutor <|.. DefaultProcessExecutor
        it.tidalwave.util.ProcessExecutor.ConsoleOutput <|.. it.tidalwave.util.spi.DefaultProcessExecutor.DefaultConsoleOutput
        DefaultProcessExecutor +-- it.tidalwave.util.spi.DefaultProcessExecutor.DefaultConsoleOutput
        it.tidalwave.util.Finder <|-- ExtendedFinderSupport
        java.util.stream.Collector <|.. ArrayListCollectorSupport
        FinderSupport <|-- SimpleFinderSupport
    }

    namespace it.tidalwave.util {
        interface As [[../As.html]] {
            {abstract} +as(Class<T>): T
            {abstract} +as(Class<T>, NotFoundBehaviour<T>): T
            +maybeAs(Class<T>): Optional<T>
            +--asOptional--(Class<T>): Optional<T>
            {abstract} +asMany(Class<T>): Collection<T>
            {static} +ref(Class<?>): Ref<T>
            +as(Ref<T>): T
            +maybeAs(Ref<T>): Optional<T>
            +asMany(Ref<T>): Collection<T>
        }
        interface Finder<TYPE> [[../Finder.html]] {
            {abstract} +from(int): Finder<TYPE>
            {abstract} +max(int): Finder<TYPE>
            +withContext(Object): Finder<TYPE>
            +ofType(Class<ANOTHER_TYPE>): Finder<ANOTHER_TYPE>
            +sort(SortCriterion): Finder<TYPE>
            {abstract} +sort(SortCriterion, SortDirection): Finder<TYPE>
            {abstract} +results(): List<? extends TYPE>
            {abstract} +count(): int
            +optionalResult(): Optional<TYPE>
            +optionalFirstResult(): Optional<TYPE>
            +stream(): Stream<TYPE>
            +iterator(): Iterator<TYPE>
            +--result--(): TYPE
            +--firstResult--(): TYPE
            {static} +empty(): Finder<T>
            {static} +ofCloned(Collection<T>): Finder<T>
        }
        interface ProcessExecutor [[../ProcessExecutor.html]] {
            {abstract} +withArguments(String...): ProcessExecutor
            {abstract} +withArgument(String): ProcessExecutor
            {abstract} +start(): ProcessExecutor
            {abstract} +stop(): void
            {abstract} +waitForCompletion(): ProcessExecutor
            {abstract} +send(String): ProcessExecutor
            {abstract} +getStdout(): ConsoleOutput
            {abstract} +getStderr(): ConsoleOutput
        }
        interface it.tidalwave.util.ProcessExecutor.ConsoleOutput [[../ProcessExecutor.ConsoleOutput.html]] {
            {abstract} +latestLineMatches(String): boolean
            {abstract} +filteredBy(String): List<String>
            {abstract} +filteredAndSplitBy(String, String): Scanner
            {abstract} +waitFor(String): ConsoleOutput
            {abstract} +clear(): void
            {abstract} +setListener(Listener): void
            {abstract} +getListener(): Listener
        }
    }

    namespace java.util.stream {
        interface Collector<T, A, R> {
            {abstract} +supplier(): Supplier<A>
            {abstract} +accumulator(): BiConsumer<A, T>
            {abstract} +combiner(): BinaryOperator<A>
            {abstract} +finisher(): Function<A, R>
            {abstract} +characteristics(): Set<Characteristics>
            {static} +of(Supplier<R>, BiConsumer<R, T>, BinaryOperator<R>, Characteristics...): Collector<T, R, R>
            {static} +of(Supplier<A>, BiConsumer<A, T>, BinaryOperator<A>, Function<A, R>, Characteristics...): Collector<T, A, R>
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
