@startuml
    namespace it.tidalwave.role.spi {

        interface RoleManager [[RoleManager.html]] {
            {abstract} +findRoles(Object, Class<T>): List<? extends T>
        }

        interface ContextManagerProvider [[ContextManagerProvider.html]] {
            {abstract} +getContextManager(): ContextManager
        }

        class DefaultContextManager [[DefaultContextManager.html]] {
            +getContexts(): List<Object>
            +findContextOfType(Class<T>): T
            +addGlobalContext(Object): void
            +removeGlobalContext(Object): void
            +addLocalContext(Object): void
            +removeLocalContext(Object): void
            +runEWithContexts(SupplierWithException<T, E extends Throwable>, Object...): T
        }

        abstract class RoleManagerSupport [[RoleManagerSupport.html]] {
            +findRoles(Object, Class<ROLE_TYPE>): List<? extends ROLE_TYPE>
            #scan(Collection<Class<?>>): void
            {abstract} #getBean(Class<T>): T
            {abstract} #findContextTypeForRole(Class<?>): Class<?>
            {abstract} #findDatumTypesForRole(Class<?>): Class<?>[]
            +logRoles(): void
        }

        class DefaultContextManagerProvider [[DefaultContextManagerProvider.html]] {
        }

        class ContextSampler [[ContextSampler.html]] {
            +ContextSampler(Object)
            +getContexts(): List<Object>
            +runWithContexts(Task<V, T extends Throwable>): V
        }

        it.tidalwave.role.ContextManager <|.. DefaultContextManager
        RoleManager <|.. RoleManagerSupport
        ContextManagerProvider <|.. DefaultContextManagerProvider
    }

    namespace it.tidalwave.role {
        interface ContextManager [[../ContextManager.html]] {
            {abstract} +getContexts(): List<Object>
            {abstract} +findContextOfType(Class<T>): T
            {abstract} +addGlobalContext(Object): void
            {abstract} +removeGlobalContext(Object): void
            {abstract} +addLocalContext(Object): void
            {abstract} +removeLocalContext(Object): void
            +--runWithContext--(Object, Task<V, T extends Throwable>): V
            +--runWithContexts--(List<Object>, Task<V, T extends Throwable>): V
            +--runWithContext--(Object, Supplier<V>): V
            +--runWithContexts--(List<Object>, Supplier<V>): V
            +runWithContexts(Runnable, Object...): void
            +runWithContexts(Supplier<T>, Object...): T
            +runEWithContexts(RunnableWithException<E extends Throwable>, Object...): void
            {abstract} +runEWithContexts(SupplierWithException<T, E extends Throwable>, Object...): T
            +binder(Object...): Binder
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
