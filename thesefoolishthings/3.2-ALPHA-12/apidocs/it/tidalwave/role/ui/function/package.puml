@startuml
    namespace it.tidalwave.role.ui.function {

        interface BoundFunction<DOMAIN_TYPE, CODOMAIN_TYPE> [[BoundFunction.html]]

        class OrFunction [[OrFunction.html]] {
            +OrFunction(ChangingSource<Boolean>...)
            {static} +or(ChangingSource<Boolean>...): OrFunction
            #function(): boolean
        }

        class CopyIfEmptyOrConform [[CopyIfEmptyOrConform.html]] {
            +CopyIfEmptyOrConform(ChangingSource<String>)
            {static} +copyIfEmptyOrConform(ChangingSource<String>): CopyIfEmptyOrConform
            #shouldChange(String, String): boolean
        }

        class AndFunction [[AndFunction.html]] {
            +AndFunction(ChangingSource<Boolean>...)
            {static} +and(ChangingSource<Boolean>...): AndFunction
            #function(): boolean
        }

        class NonEmptyFunction [[NonEmptyFunction.html]] {
            +NonEmptyFunction(ChangingSource<String>)
            {static} +nonEmpty(ChangingSource<String>): NonEmptyFunction
            #onSourceChange(String, String): void
            #function(String): Boolean
        }

        abstract class UnaryBoundFunctionSupport<DOMAIN_TYPE, CODOMAIN_TYPE> [[UnaryBoundFunctionSupport.html]] {
            #source: ChangingSource<DOMAIN_TYPE>
            #value: CODOMAIN_TYPE
            #UnaryBoundFunctionSupport(ChangingSource<DOMAIN_TYPE>)
            #onSourceChange(DOMAIN_TYPE, DOMAIN_TYPE): void
            {abstract} #function(DOMAIN_TYPE): CODOMAIN_TYPE
            +get(): CODOMAIN_TYPE
        }

        abstract class BooleanBoundFunctionSupport [[BooleanBoundFunctionSupport.html]] {
            #sources: ChangingSource<Boolean>[]
            +BooleanBoundFunctionSupport(ChangingSource<Boolean>...)
            {abstract} #function(): boolean
            +get(): Boolean
        }

        abstract class BoundFunctionSupport<DOMAIN_TYPE, CODOMAIN_TYPE> [[BoundFunctionSupport.html]] {
            +unbindAll(): void
            #fireValueChanged(CODOMAIN_TYPE, CODOMAIN_TYPE): void
            #fireValueChanged(boolean, boolean): void
        }

        abstract class WeakCopyFunctionSupport<T> [[WeakCopyFunctionSupport.html]] {
            #targetValue: T
            +WeakCopyFunctionSupport(ChangingSource<T>)
            #onSourceChange(T, T): void
            +set(T): void
            {abstract} #shouldChange(T, T): boolean
            #function(T): T
        }

        it.tidalwave.role.ui.ChangingSource <|-- BoundFunction
        BooleanBoundFunctionSupport <|-- OrFunction
        WeakCopyFunctionSupport <|-- CopyIfEmptyOrConform
        BooleanBoundFunctionSupport <|-- AndFunction
        UnaryBoundFunctionSupport <|-- NonEmptyFunction
        BoundFunctionSupport <|-- UnaryBoundFunctionSupport
        BoundFunctionSupport <|-- BooleanBoundFunctionSupport
        BoundFunction <|.. BoundFunctionSupport
        UnaryBoundFunctionSupport <|-- WeakCopyFunctionSupport
        it.tidalwave.role.ui.Changeable <|.. WeakCopyFunctionSupport
    }

    namespace it.tidalwave.role.ui {
        interface ChangingSource<T> [[../ChangingSource.html]] {
            {abstract} +getPropertyChangeListeners(): PropertyChangeListener[]
            {abstract} +addPropertyChangeListener(PropertyChangeListener): void
            {abstract} +removePropertyChangeListener(PropertyChangeListener): void
            {abstract} +unbindAll(): void
            {abstract} +get(): T
        }
        interface Changeable<T> [[../Changeable.html]] {
            {abstract} +set(T): void
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
