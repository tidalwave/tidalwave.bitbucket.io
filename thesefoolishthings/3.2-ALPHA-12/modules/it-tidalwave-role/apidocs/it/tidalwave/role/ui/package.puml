@startuml
    namespace it.tidalwave.role.ui {

        interface UserActionProvider [[UserActionProvider.html]] {
            {static} +_UserActionProvider_: Class<UserActionProvider>
            {abstract} +getActions(): Collection<? extends UserAction>
            {abstract} +--getDefaultAction--(): UserAction
            {static} +of(UserAction...): UserActionProvider
        }

        interface PresentationModel [[PresentationModel.html]] {
            {static} +PresentationModel: Class<PresentationModel>
            {static} +PROPERTY_CHILDREN: String
            {static} +CALLBACK_DISPOSE: String
            {abstract} +dispose(): void
            {abstract} +addPropertyChangeListener(PropertyChangeListener): void
            {abstract} +addPropertyChangeListener(String, PropertyChangeListener): void
            {abstract} +removePropertyChangeListener(PropertyChangeListener): void
            {abstract} +removePropertyChangeListener(String, PropertyChangeListener): void
            {abstract} +hasListeners(String): boolean
            {abstract} +getPropertyChangeListeners(): PropertyChangeListener[]
            {abstract} +getPropertyChangeListeners(String): PropertyChangeListener[]
            {static} +of(Object): PresentationModel
            {static} +of(Object, Object): PresentationModel
            {static} +of(Object, Collection<Object>): PresentationModel
            {static} +empty(): PresentationModel
            {static} +ofMaybePresentable(As, Collection<Object>): PresentationModel
            {static} +ofMaybePresentable(As): PresentationModel
        }

        interface Styleable [[Styleable.html]] {
            {static} +_Styleable_: Class<Styleable>
            {abstract} +getStyles(): Collection<String>
            {static} +of(Collection<String>): Styleable
            {static} +of(String...): Styleable
        }

        interface ChangingSource<T> [[ChangingSource.html]] {
            {abstract} +getPropertyChangeListeners(): PropertyChangeListener[]
            {abstract} +addPropertyChangeListener(PropertyChangeListener): void
            {abstract} +removePropertyChangeListener(PropertyChangeListener): void
            {abstract} +unbindAll(): void
            {abstract} +get(): T
        }

        class BoundProperty<T> [[BoundProperty.html]] {
            {static} +PROP_VALUE: String
            {static} +of(T): BoundProperty<T>
            +set(T): void
            +get(): T
            +bind(ChangingSource<T>): void
            +unbindAll(): void
        }

        interface Displayable [[Displayable.html]] {
            {static} +_Displayable_: Class<Displayable>
            {abstract} +getDisplayName(): String
            {static} +of(String): Displayable
            {static} +of(String, String): Displayable
            {static} +of(Supplier<String>): Displayable
            {static} +of(Function<T, String>, T): Displayable
            {static} +fromBundle(Class<?>, String): LocalizedDisplayable
            {static} +comparing(): Comparator<Displayable>
            {static} +asComparing(): Comparator<As>
        }

        interface PresentationModelFactory [[PresentationModelFactory.html]] {
            {static} +_PresentationModelFactory_: Class<PresentationModelFactory>
            {abstract} +createPresentationModel(Object, Collection<Object>): PresentationModel
            +createPresentationModel(Object): PresentationModel
        }

        interface UserAction [[UserAction.html]] {
            {static} +_UserAction_: Class<UserAction>
            {abstract} +actionPerformed(): void
            {abstract} +enabled(): BoundProperty<Boolean>
            {static} +of(Callback, Collection<Object>): UserAction
            {static} +of(Callback, Object): UserAction
            {static} +of(Callback): UserAction
        }

        interface MutableDisplayable [[MutableDisplayable.html]] {
            {static} +_MutableDisplayable_: Class<MutableDisplayable>
            {static} +PROP_DISPLAY_NAME: String
            {static} +PROP_DISPLAY_NAMES: String
            {abstract} +setDisplayName(String): void
            {abstract} +setDisplayName(String, Locale): void
            {abstract} +setDisplayNames(Map<Locale, String>): void
            {abstract} +addPropertyChangeListener(PropertyChangeListener): void
            {abstract} +removePropertyChangeListener(PropertyChangeListener): void
            {static} +of(String): MutableDisplayable
            {static} +of(String, String): MutableDisplayable
        }

        interface MutableLocalizedDisplayable [[MutableLocalizedDisplayable.html]] {
            {static} +_MutableLocalizedDisplayable_: Class<MutableLocalizedDisplayable>
            {static} +of(String): MutableLocalizedDisplayable
            {static} +of(String, String): MutableLocalizedDisplayable
        }

        interface IconProvider [[IconProvider.html]] {
            {static} +_IconProvider_: Class<IconProvider>
            {abstract} +getIcon(int): Icon
        }

        interface Presentable [[Presentable.html]] {
            {static} +_Presentable_: Class<Presentable>
            +createPresentationModel(): PresentationModel
            {abstract} +createPresentationModel(Collection<Object>): PresentationModel
            +createPresentationModel(Object): PresentationModel
            {static} +of(Object): Presentable
        }

        interface Selectable [[Selectable.html]] {
            {static} +_Selectable_: Class<Selectable>
            {abstract} +select(): void
        }

        interface MutableIconProvider [[MutableIconProvider.html]] {
            {static} +_MutableIconProvider_: Class<MutableIconProvider>
            {static} +PROP_ICON: String
            {abstract} +setIcon(Icon): void
            {abstract} +addPropertyChangeListener(PropertyChangeListener): void
            {abstract} +removePropertyChangeListener(PropertyChangeListener): void
        }

        class PresentationModelAggregate [[PresentationModelAggregate.html]] {
            {static} +newInstance(): PresentationModelAggregate
            +withPmOf(String, Collection<Object>): PresentationModelAggregate
            +getByName(String): Optional<PresentationModel>
            +getNames(): Collection<String>
        }

        interface LocalizedDisplayable [[LocalizedDisplayable.html]] {
            {static} +_LocalizedDisplayable_: Class<LocalizedDisplayable>
            {abstract} +getDisplayName(Locale): String
            {abstract} +getDisplayNames(): Map<Locale, String>
            {abstract} +getLocales(): SortedSet<Locale>
        }

        interface Visible [[Visible.html]] {
            {static} +_Visible_: Class<Visible>
            {abstract} +isVisible(): boolean
        }

        interface Changeable<T> [[Changeable.html]] {
            {abstract} +set(T): void
        }

        interface ActionProvider [[ActionProvider.html]] {
            {static} +_ActionProvider_: Class<ActionProvider>
            {abstract} +getActions(): Collection<? extends Action>
            {abstract} +getDefaultAction(): Action
        }

        UserActionProvider --> "0..1" UserAction: optionalDefaultAction
        it.tidalwave.util.As <|-- PresentationModel
        ChangingSource <|.. BoundProperty
        Changeable <|.. BoundProperty
        Displayable --> Displayable: DEFAULT
        it.tidalwave.util.As <|-- UserAction
        Displayable <|-- MutableDisplayable
        MutableDisplayable <|-- MutableLocalizedDisplayable
        LocalizedDisplayable <|-- MutableLocalizedDisplayable
        IconProvider --> IconProvider: DEFAULT
        IconProvider <|-- MutableIconProvider
        it.tidalwave.role.Aggregate <|.. PresentationModelAggregate
        Displayable <|-- LocalizedDisplayable
        Visible --> Visible: VISIBLE\nINVISIBLE
    }

    namespace it.tidalwave.util {
        interface As [[http://tidalwave.it/projects/thesefoolishthings/modules/it-tidalwave-util/apidocs/it/tidalwave/util/As.html?is-external=true]] {
            {abstract} +as(Class<T>): T
            {abstract} +as(Class<T>, NotFoundBehaviour<T>): T
            +maybeAs(Class<T>): Optional<T>
            +--asOptional--(Class<T>): Optional<T>
            {abstract} +asMany(Class<T>): Collection<T>
            {static} +ref(Class<?>): Ref<T>
            +as(Ref<T>): T
            +maybeAs(Ref<T>): Optional<T>
            +asMany(Ref<T>): Collection<T>
        }
    }

    namespace it.tidalwave.role {
        interface Aggregate<T> [[../Aggregate.html]] {
            {static} +_Aggregate_: Class<Aggregate>
            {abstract} +getByName(String): Optional<T>
            +getNames(): Collection<String>
            {static} +of(Map<String, T>): Aggregate<T>
            {static} +newInstance(): Aggregate<T>
            {static} +of(String, T): Aggregate<T>
            +with(String, T): Aggregate<T>
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
