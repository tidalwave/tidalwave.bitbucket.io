@startuml
    namespace it.tidalwave.messagebus.spi {

        interface MessageDelivery [[MessageDelivery.html]] {
            {abstract} +initialize(SimpleMessageBus): void
            {abstract} +deliverMessage(Class<TOPIC>, TOPIC): void
        }

        class MultiQueue [[MultiQueue.html]] {
            +add(Class<TOPIC>, TOPIC): void
            +remove(): TopicAndMessage<TOPIC>
        }

        class RoundRobinAsyncMessageDelivery [[RoundRobinAsyncMessageDelivery.html]] {
            +initialize(SimpleMessageBus): void
            +deliverMessage(Class<TOPIC>, TOPIC): void
        }

        class SimpleAsyncMessageDelivery [[SimpleAsyncMessageDelivery.html]] {
            +initialize(SimpleMessageBus): void
            +deliverMessage(Class<TOPIC>, TOPIC): void
        }

        class SimpleMessageBus [[SimpleMessageBus.html]] {
            +SimpleMessageBus()
            +SimpleMessageBus(Executor)
            +SimpleMessageBus(Executor, MessageDelivery)
            +publish(TOPIC): void
            +publish(Class<TOPIC>, TOPIC): void
            +subscribe(Class<TOPIC>, Listener<TOPIC>): void
            +unsubscribe(Listener<?>): void
            #dispatchMessage(Class<TOPIC>, TOPIC): void
        }

        class ReflectionUtils [[ReflectionUtils.html]] {
            {static} +forEachMethodInTopDownHierarchy(Object, MethodProcessor): void
            {static} +forEachMethodInBottomUpHierarchy(Object, MethodProcessor): void
            {static} +containsAnnotation(Annotation[], Class<?>): boolean
        }

        interface it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessor [[ReflectionUtils.MethodProcessor.html]] {
            {abstract} +filter(Class<?>): FilterResult
            {abstract} +process(Method): void
        }

        enum it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessor.FilterResult [[ReflectionUtils.MethodProcessor.FilterResult.html]] {
            {static} +ACCEPT
            {static} +IGNORE
        }

        class it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessorSupport [[ReflectionUtils.MethodProcessorSupport.html]] {
            +filter(Class<?>): FilterResult
            +process(Method): void
        }

        MessageDelivery <|.. RoundRobinAsyncMessageDelivery
        MessageDelivery <|.. SimpleAsyncMessageDelivery
        it.tidalwave.messagebus.MessageBus <|.. SimpleMessageBus
        ReflectionUtils +-- it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessor
        it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessor +-- it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessor.FilterResult
        it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessor <|.. it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessorSupport
        ReflectionUtils +-- it.tidalwave.messagebus.spi.ReflectionUtils.MethodProcessorSupport
    }

    namespace it.tidalwave.messagebus {
        interface MessageBus [[../MessageBus.html]] {
            {abstract} +publish(TOPIC): void
            {abstract} +publish(Class<TOPIC>, TOPIC): void
            {abstract} +subscribe(Class<TOPIC>, Listener<TOPIC>): void
            {abstract} +unsubscribe(Listener<?>): void
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
