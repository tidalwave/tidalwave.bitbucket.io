#!/bin/bash

# We need a local Maven repository because we are going to rebuild artifacts that have been already released and that must not pollute other repositories.
MAVEN_REPOSITORY=$PWD/repository
# PROJECTS="thesefoolishthings-superpom"
PROJECTS="thesefoolishthings steelblue mistral northernwind northernwind-rca bluehour bluemarine2 solidblue3j mapview"
BASE=$PWD

for PROJECT in $PROJECTS; do
    echo "================"
    URL=https://bitbucket.org/tidalwave/$PROJECT-src.git
    VERSION=`git ls-remote --quiet --tags --sort version:refname $URL | grep -v archive | grep -v "{}" | sed 's/.*tags\///g' | tail -n 1`
    WORKAREA=$BASE/checkout/$PROJECT-src/$VERSION

    echo "Project: $PROJECT"
    echo "Version: $VERSION"

    TARGET=$BASE/$PROJECT/$VERSION

    if [ "$PROJECT" == "thesefoolishthings-superpom" ]; then
        TARGET=$BASE/superpom/$VERSION
        fi

    if [ "$PROJECT" == "northernwind-rca" ]; then
        TARGET=$BASE/zephyr/$VERSION
        fi

    if [ ! -d "$TARGET" ]; then
        echo "Cleaning $WORKAREA ..."
        rm -rf $WORKAREA
        echo "Cloning $URL into $WORKAREA ..."
        mkdir -p "$WORKAREA"
        git clone --quiet --depth 1 --branch $VERSION $URL $WORKAREA
        cd $WORKAREA
        # mvn -Dmaven.repo.local=$MAVEN_REPOSITORY -B -Dtft.profile.metrics.sonarGoal= -Pit.tidalwave-metrics-v2,it.tidalwave-pre-site-v1,it.tidalwave-ci-v1 
        # mvn -Dmaven.repo.local=$MAVEN_REPOSITORY -B -Pit.tidalwave-site-v1 
        mvn -B -Dmaven.repo.local=$MAVEN_REPOSITORY -Dtft.profile.metrics.sonarGoal= -Dtft.checkstyle.skip=true -Pinstallers,it.tidalwave-metrics-v2,it.tidalwave-metrics-pre-site-v1,it.tidalwave-ci-v1
        mvn -B -Dmaven.repo.local=$MAVEN_REPOSITORY -Dtft.profile.metrics.sonarGoal= -Dtft.checkstyle.skip=true -Pit.tidalwave-site-v1,it.tidalwave-ci-v1

        if [ -d "$WORKAREA/target/$PROJECT" ]; then
            mv $WORKAREA/target/$PROJECT $TARGET
            fi

        if [ -d "$WORKAREA/target/staging" ]; then
            mv $WORKAREA/target/staging $TARGET
            fi

        mvn -Dmaven.repo.local=$MAVEN_REPOSITORY -B -Pit.tidalwave-site-v1 javadoc:aggregate-no-fork 
        # mv $WORKAREA/target/site/apidocs $TARGET
        mv $WORKAREA/target/reports/apidocs/ $TARGET

        cd $BASE

        INDEX="$TARGET/../index.html"
        cat > "$INDEX" << EOF
<!DOCTYPE html>
<html>
    <style>
        .myclass {
            position: absolute;
            left: 50%;
            top: 50%;
            font-size: 24pt;
         }
    </style>
    <body>
        <div class="myclass">
EOF

        for V in `find $TARGET/.. -type d -depth 1 | sed 's/.*\///g' | sort -r | xargs echo`; do
            echo "            <a href=\"$V/index.html\">$V</a><br>" >> "$INDEX"
            done

        cat >> "$INDEX" << EOF
        </div>
    </body>
</html>
EOF

        git add $TARGET $INDEX
        git commit -a -m "Added $PROJECT $VERSION"

        fi
    done

# echo not doing git push origin master
git push --force
