@startuml
    namespace it.tidalwave.image.processor {

        class Statistics [[Statistics.html]] {
            +addSample(String, long): void
            +merge(Statistics): void
            +dump(): void
            +dump(PrintWriter): void
            +iterator(): Iterator<Item>
        }

        class it.tidalwave.image.processor.Statistics.Item [[Statistics.Item.html]] {
            +Item(String)
            +getName(): String
            +getMinimum(): long
            +getMaximum(): long
            +getAverage(): long
            +getAccumulator(): long
            +getSampleCount(): int
            +addSample(long): void
            +compareTo(Item): int
            #merge(Item): void
        }

        abstract class ImagingTaskProcessor [[ImagingTaskProcessor.html]] {
            {static} #maxWorkers: int
            #freeWorkers: int
            #lock: Object
            {static} +getInstance(): ImagingTaskProcessor
            {static} +setDefault(Class<? extends ImagingTaskProcessor>): void
            {static} +setMaxWorkers(int): void
            +getMaxWorkers(): int
            {abstract} +getWorkerCount(): int
            {abstract} +getWorkerIds(): Collection<Serializable>
            {abstract} +isDistributed(): boolean
            {abstract} +hasFileAccess(): boolean
            +addListener(ImagingTaskProcessorListener): void
            +removeListener(ImagingTaskProcessorListener): void
            +processingResourcesAvailable(): boolean
            +post(Collection<? extends ImagingTask>): void
            +post(ImagingTask): void
            +postWithPriority(Collection<? extends ImagingTask>): void
            +postWithPriority(ImagingTask): void
            +cancellPendingTasks(Class<T extends ImagingTask>): Collection<T extends ImagingTask>
            +getPendingTaskCount(Class<? extends ImagingTask>): int
            +getRunningTaskCount(Class<? extends ImagingTask>): int
            +getCompletedTaskCount(Class<? extends ImagingTask>): int
            +popCompletedTask(Class<T extends ImagingTask>): T extends ImagingTask
            #changeFreeWorkerCount(int): void
            #getNextTask(Serializable, boolean): ImagingTask
            #notifyTaskCompleted(ImagingTask): void
        }

        class LocalImagingTaskProcessor [[LocalImagingTaskProcessor.html]] {
            +isDistributed(): boolean
            +hasFileAccess(): boolean
            +getWorkerCount(): int
            +getWorkerIds(): Collection<Serializable>
        }

        class it.tidalwave.image.processor.LocalImagingTaskProcessor.PoolThread [[LocalImagingTaskProcessor.PoolThread.html]] {
            +PoolThread(String)
            +run(): void
        }

        abstract class ImagingTask [[ImagingTask.html]] {
            +latestExecutionTime: long
            +ImagingTask()
            +ImagingTask(String, int)
            +getName(): String
            +getId(): Serializable
            +prepare(ImagingTaskProcessor): void
            {abstract} #run(): void
            +getResult(): EditableImage
            #setResult(EditableImage): void
            +getThrowable(): Throwable
            +setThrowable(Throwable): void
            +isRemoteExecutionOk(): boolean
            +dispose(): void
            +execute(): void
            +addStatisticsSample(String, long): void
            #registerTime(String, EditableImage): void
            #execute(EditableImage, T extends Operation, String): T extends Operation
            #executeAndDispose(EditableImage, T extends Operation, String): T extends Operation
            #execute2(EditableImage, Operation, String): EditableImage
            #execute2AndDispose(EditableImage, Operation, String): EditableImage
        }

        java.io.Serializable <|.. Statistics
        java.lang.Iterable <|.. Statistics
        java.io.Serializable <|.. it.tidalwave.image.processor.Statistics.Item
        java.lang.Comparable <|.. it.tidalwave.image.processor.Statistics.Item
        Statistics +-- it.tidalwave.image.processor.Statistics.Item
        ImagingTaskProcessor --> "*" it.tidalwave.image.processor.Statistics.Item: statistics
        ImagingTaskProcessor <|-- LocalImagingTaskProcessor
        java.lang.Thread <|-- it.tidalwave.image.processor.LocalImagingTaskProcessor.PoolThread
        LocalImagingTaskProcessor +-- it.tidalwave.image.processor.LocalImagingTaskProcessor.PoolThread
        java.io.Serializable <|.. ImagingTask
        ImagingTask --> "*" it.tidalwave.image.processor.Statistics.Item: statistics
    }

    namespace java.io {
        interface Serializable
    }

    namespace java.lang {
        interface Iterable<T> {
            {abstract} +iterator(): Iterator<T>
            +forEach(Consumer<? super T>): void
            +spliterator(): Spliterator<T>
        }
        interface Comparable<T> {
            {abstract} +compareTo(T): int
        }
        class Thread {
            {static} +MIN_PRIORITY: int
            {static} +NORM_PRIORITY: int
            {static} +MAX_PRIORITY: int
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
