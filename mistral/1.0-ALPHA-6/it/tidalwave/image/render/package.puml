@startuml
    namespace it.tidalwave.image.render {

        class PreviewSettings [[PreviewSettings.html]] {
            +setLookupTable(short[], short[], short[], short[]): void
            +getLookupTable16bit(): ShortLookupTable
            +getLookupTable8bit(): ByteLookupTable
        }

        class MouseWheelZoomingController [[MouseWheelZoomingController.html]] {
            +MouseWheelZoomingController(ScaleController)
            +setEnabled(boolean): void
            +isEnabled(): boolean
            +setZoomFactor(double): void
            +getZoomFactor(): double
        }

        class ScaleController [[ScaleController.html]] {
            +ScaleController(EditableImageRenderer)
            +setScale(double): void
            +setScale(double, Point): void
            +getScale(): double
            +setZoomFactor(double): void
            +getZoomFactor(): double
            +fitToView(): void
            +fitToView(double): void
            +zoomOut(): void
            +zoomIn(): void
            +showActualPixels(): void
            #getImageRenderer(): EditableImageRenderer
        }

        class RotationController [[RotationController.html]] {
            +RotationController(EditableImageRenderer)
            +setAngle(double): void
            +getAngle(): double
        }

        class EditingTool [[EditingTool.html]] {
            {static} +CHANGED_ATTRIBUTE: String
            #EditingTool(EditableImageRenderer)
            +setEnabled(boolean): void
            +isEnabled(): boolean
            +isActive(): boolean
            +commitChanges(): void
            +activate(): void
            +deactivate(): void
            +imageChanged(): void
            +reset(): void
            +setIcon(Icon): void
            +isVisible(): boolean
            +connectButton(JToggleButton): void
            #setInitialState(Class<? extends State>): void
            #setState(Class<? extends State>): void
            #getCurrentState(): State
            #registerState(State): void
            #repaint(): void
            #makeCursor(Icon, String): Cursor
            +mouseClicked(MouseEvent): void
            +mousePressed(MouseEvent): void
            +mouseReleased(MouseEvent): void
            +mouseEntered(MouseEvent): void
            +mouseExited(MouseEvent): void
            +mouseDragged(MouseEvent): void
            +mouseMoved(MouseEvent): void
            +keyTyped(KeyEvent): void
            +keyPressed(KeyEvent): void
            +keyReleased(KeyEvent): void
            +paint(Graphics2D, EditableImageRenderer): void
        }

        class it.tidalwave.image.render.EditingTool.State [[EditingTool.State.html]] {
            +mouseClicked(MouseEvent): void
            +mousePressed(MouseEvent): void
            +mouseReleased(MouseEvent): void
            +mouseEntered(MouseEvent): void
            +mouseExited(MouseEvent): void
            +mouseDragged(MouseEvent): void
            +mouseMoved(MouseEvent): void
            +keyTyped(KeyEvent): void
            +keyPressed(KeyEvent): void
            +keyReleased(KeyEvent): void
            +paint(Graphics2D, EditableImageRenderer): void
            +start(): void
            +stop(): void
        }

        interface Overlay [[Overlay.html]] {
            {abstract} +paint(Graphics2D, EditableImageRenderer): void
            {abstract} +isVisible(): boolean
        }

        class EditableImageRenderer [[EditableImageRenderer.html]] {
            {static} +MAX_SCALE: double
            {static} +MIN_SCALE: double
            #image: EditableImage
            #scale: double
            #angle: double
            +setImage(EditableImage): void
            +getImage(): EditableImage
            +getOptimizedImage(): EditableImage
            +setRepaintEnabled(boolean): void
            +isRepaintEnabled(): boolean
            +setOrigin(Point): void
            +getOrigin(): Point
            +getScale(): double
            +setAngle(double): void
            +getAngle(): double
            +setImageBorder(Border): void
            +getImageBorder(): Border
            +getPositionOverImage(Point): Point
            +convertImagePointToComponentPoint(Point): Point
            +setPositionOverImage(Point, Point): void
            +setMargin(Insets): void
            +getMargin(): Insets
            +setScrollBarsVisible(boolean): void
            +isScrollBarsVisible(): boolean
            #computeOrigin(Point, Point, double): Point
            +setScaleQuality(Quality): void
            +getScaleQuality(): Quality
            +setRotateQuality(Quality): void
            +getRotateQuality(): Quality
            +setScaledImageCachingEnabled(boolean): void
            +isScaledImageCachingEnabled(): boolean
            +setOptimizedImageEnabled(boolean): void
            +isOptimizedImageEnabled(): boolean
            +setClippingShape(Shape): void
            +addOverlay(Overlay): void
            +removeOverlay(Overlay): void
            +update(Graphics): void
            +paint(Graphics): void
            +flushAllCaches(): void
            +flushScaledImageCache(): void
            +moveOrigin(int, int): void
            +setScale(double): void
            +setScale(double, Point): void
            +setMaxScale(double): void
            +getMaxScale(): double
            +setMinScale(double): void
            +getMinScale(): double
            +getFitScale(): double
            +centerImage(): void
            +fitToDisplaySize(): void
            +setFitToDisplaySize(boolean): void
            +addImageRendererListener(EditableImageRendererListener): void
            +removeImageRendererListener(EditableImageRendererListener): void
            #fireEditingToolActivated(EditingTool): void
            #fireEditingToolDeactivated(EditingTool): void
        }

        class AnimatedScaleController [[AnimatedScaleController.html]] {
            +AnimatedScaleController(EditableImageRenderer)
            +isRunning(): boolean
            +setScale(double, Point): void
        }

        class DragPanningController [[DragPanningController.html]] {
            +DragPanningController(EditableImageRenderer)
            +setEnabled(boolean): void
            +isEnabled(): boolean
            +centerImage(): void
        }

        class MouseClickZoomingController [[MouseClickZoomingController.html]] {
            +MouseClickZoomingController(ScaleController)
            +setClickCountToZoom(int): void
            +getClickCountToZoom(): int
            +setEnabled(boolean): void
            +isEnabled(): boolean
            +setFactor(double): void
            +getFactor(): double
        }

        ScaleController --> EditableImageRenderer: imageRenderer
        RotationController --> EditableImageRenderer: imageRenderer
        java.awt.event.MouseListener <|.. EditingTool
        java.awt.event.MouseMotionListener <|.. EditingTool
        java.awt.event.KeyListener <|.. EditingTool
        Overlay <|.. EditingTool
        EditingTool --> EditableImageRenderer: imageRenderer
        EditingTool +-- it.tidalwave.image.render.EditingTool.State
        javax.swing.JComponent <|-- EditableImageRenderer
        EditableImageRenderer --> EditingTool: editingTool
        EditableImageRenderer --> PreviewSettings: previewSettings
        ScaleController <|-- AnimatedScaleController
    }

    namespace java.awt.event {
        interface MouseListener {
            {abstract} +mouseClicked(MouseEvent): void
            {abstract} +mousePressed(MouseEvent): void
            {abstract} +mouseReleased(MouseEvent): void
            {abstract} +mouseEntered(MouseEvent): void
            {abstract} +mouseExited(MouseEvent): void
        }
        interface MouseMotionListener {
            {abstract} +mouseDragged(MouseEvent): void
            {abstract} +mouseMoved(MouseEvent): void
        }
        interface KeyListener {
            {abstract} +keyTyped(KeyEvent): void
            {abstract} +keyPressed(KeyEvent): void
            {abstract} +keyReleased(KeyEvent): void
        }
    }

    namespace javax.swing {
        abstract class JComponent {
            #ui: ComponentUI
            #listenerList: EventListenerList
            {static} +WHEN_FOCUSED: int
            {static} +WHEN_ANCESTOR_OF_FOCUSED_COMPONENT: int
            {static} +WHEN_IN_FOCUSED_WINDOW: int
            {static} +UNDEFINED_CONDITION: int
            {static} +TOOL_TIP_TEXT_KEY: String
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
