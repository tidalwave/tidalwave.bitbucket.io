@startuml
    namespace it.tidalwave.image.java2d {

        class ScaleJ2DOp [[ScaleJ2DOp.html]] {
            #execute(ScaleOp, EditableImage, BufferedImage): BufferedImage
        }

        class RotateQuadrantJ2DOp [[RotateQuadrantJ2DOp.html]] {
            #execute(RotateQuadrantOp, EditableImage, BufferedImage): BufferedImage
        }

        class WriteJ2DOp [[WriteJ2DOp.html]] {
            #execute(WriteOp, EditableImage, BufferedImage): BufferedImage
        }

        class CropJ2DOp [[CropJ2DOp.html]] {
            #execute(CropOp, EditableImage, BufferedImage): BufferedImage
        }

        class CaptureJ2DOp [[CaptureJ2DOp.html]] {
            #execute(CaptureOp, EditableImage, BufferedImage): BufferedImage
        }

        class PrintJ2DOp [[PrintJ2DOp.html]] {
            #execute(PrintOp, EditableImage, BufferedImage): BufferedImage
        }

        class WrapJ2DOp [[WrapJ2DOp.html]] {
            #execute(WrapOp, EditableImage, BufferedImage): BufferedImage
        }

        class RotateJ2DOp [[RotateJ2DOp.html]] {
            #execute(RotateOp, EditableImage, BufferedImage): BufferedImage
        }

        class PaintJ2DOp [[PaintJ2DOp.html]] {
            #execute(PaintOp, EditableImage, BufferedImage): BufferedImage
        }

        class Java2DUtils [[Java2DUtils.html]] {
            {static} +ZERO: Float
            {static} +getProperties(BufferedImage): Properties
            {static} +createOptimizedImage(int, int): BufferedImage
            {static} +getGraphicsConfiguration(): GraphicsConfiguration
            {static} +createSimilarImage(BufferedImage, int, int): BufferedImage
            {static} +getICCProfileName(RenderedImage): String
            {static} +convertToSinglePixelPackedSampleModel(BufferedImage): BufferedImage
            {static} +scaleWithAffineTransform(BufferedImage, double, double, Quality): BufferedImage
            {static} +scaleWithDrawImage(BufferedImage, double, double, Quality): BufferedImage
            {static} +rotateWithDrawImage(BufferedImage, double, Quality): BufferedImage
            {static} #toString(int[], int): String
            {static} +logImage(Logger, String, RenderedImage): void
            {static} +createOptimizedImage(BufferedImage, double, double, Quality): BufferedImage
            {static} +findRenderingHintsInterpolation(Quality): Object
            {static} +findAffineTransformInterpolation(Quality): int
            {static} +getAveragingKernel(int): Kernel
        }

        class OptimizeJ2DOp [[OptimizeJ2DOp.html]] {
            #execute(OptimizeOp, EditableImage, BufferedImage): BufferedImage
        }

        class ImplementationFactoryJ2D [[ImplementationFactoryJ2D.html]] {
            {static} +getDefault(): ImplementationFactory
            +createImageModel(BufferedImage): ImageModel
            +canConvertFrom(Class): boolean
            +convertFrom(Object): ImageModel
            +canConvertTo(Class): boolean
            +convertTo(Object): Object
        }

        class ConvertToBufferedImageJ2DOp [[ConvertToBufferedImageJ2DOp.html]] {
            #execute(ConvertToBufferedImageOp, EditableImage, BufferedImage): BufferedImage
        }

        class ImageModelJ2D [[ImageModelJ2D.html]] {
            +ImageModelJ2D(Object)
            +getFactory(): ImplementationFactory
            {static} +createImage(BufferedImage): EditableImage
            +getWidth(): int
            +getHeight(): int
            +getDataType(): DataType
            +getBandCount(): int
            +getColorModel(): ColorModel
            +getMemorySize(): long
            +createCopy(boolean): EditableImage
            +getInnerProperty(Class<T>): T
            #toRenderedImageForSerialization(): RenderedImage
            #toObjectForDeserialization(RenderedImage): Object
        }

        class CreateJ2DOp [[CreateJ2DOp.html]] {
            +canHandle(CreateOp): boolean
            #execute(CreateOp, EditableImage, BufferedImage): BufferedImage
        }

        class DrawJ2DOp [[DrawJ2DOp.html]] {
            #execute(DrawOp, EditableImage, BufferedImage): BufferedImage
        }

        class ConvolveJ2DOp [[ConvolveJ2DOp.html]] {
            #execute(ConvolveOp, EditableImage, BufferedImage): BufferedImage
        }

        class AdditionalOperations [[AdditionalOperations.html]] {
            {static} +register(ImplementationFactoryJ2D): void
        }

        class HistogramJava2D [[HistogramJava2D.html]] {
            +getFrequencies(int): int[]
            +getMin(int): int
            +getMax(int): int
            +getPreview(PreviewSettings): Histogram
        }

        class AssignColorProfileJ2DOp [[AssignColorProfileJ2DOp.html]] {
            #execute(AssignColorProfileOp, EditableImage, BufferedImage): BufferedImage
        }

        class ConvertColorProfileJ2DOp [[ConvertColorProfileJ2DOp.html]] {
            #execute(ConvertColorProfileOp, EditableImage, BufferedImage): BufferedImage
        }

        class ApplyUnsharpMaskJ2DOp [[ApplyUnsharpMaskJ2DOp.html]] {
            #execute(ApplyUnsharpMaskOp, EditableImage, BufferedImage): BufferedImage
        }

        class HistogramJ2DOp [[HistogramJ2DOp.html]] {
            #execute(HistogramOp, EditableImage, BufferedImage): BufferedImage
        }

        class ChangeBufferTypeJ2DOp [[ChangeBufferTypeJ2DOp.html]] {
            #execute(ChangeBufferTypeOp, EditableImage, BufferedImage): BufferedImage
        }

        it.tidalwave.image.op.OperationImplementation <|-- ScaleJ2DOp
        it.tidalwave.image.op.OperationImplementation <|-- RotateQuadrantJ2DOp
        it.tidalwave.image.op.OperationImplementation <|-- WriteJ2DOp
        it.tidalwave.image.op.OperationImplementation <|-- CropJ2DOp
        it.tidalwave.image.op.OperationImplementation <|-- CaptureJ2DOp
        it.tidalwave.image.op.OperationImplementation <|-- PrintJ2DOp
        it.tidalwave.image.op.OperationImplementation <|-- WrapJ2DOp
        it.tidalwave.image.op.OperationImplementation <|-- RotateJ2DOp
        it.tidalwave.image.op.OperationImplementation <|-- PaintJ2DOp
        it.tidalwave.image.op.OperationImplementation <|-- OptimizeJ2DOp
        it.tidalwave.image.op.ImplementationFactory <|-- ImplementationFactoryJ2D
        it.tidalwave.image.op.OperationImplementation <|-- ConvertToBufferedImageJ2DOp
        it.tidalwave.image.ImageModel <|-- ImageModelJ2D
        it.tidalwave.image.op.OperationImplementation <|-- CreateJ2DOp
        it.tidalwave.image.op.OperationImplementation <|-- DrawJ2DOp
        it.tidalwave.image.op.OperationImplementation <|-- ConvolveJ2DOp
        it.tidalwave.image.Histogram <|-- HistogramJava2D
        it.tidalwave.image.op.OperationImplementation <|-- AssignColorProfileJ2DOp
        it.tidalwave.image.op.OperationImplementation <|-- ConvertColorProfileJ2DOp
        it.tidalwave.image.op.OperationImplementation <|-- ApplyUnsharpMaskJ2DOp
        it.tidalwave.image.op.OperationImplementation <|-- HistogramJ2DOp
        it.tidalwave.image.op.OperationImplementation <|-- ChangeBufferTypeJ2DOp
    }

    namespace it.tidalwave.image.op {
        abstract class OperationImplementation<Op extends Operation, Model> [[../op/OperationImplementation.html]] {
            {abstract} #execute(Op extends Operation, EditableImage, Model): Model
        }
        abstract class ImplementationFactory [[../op/ImplementationFactory.html]] {
            {abstract} +canConvertFrom(Class): boolean
            {abstract} +convertFrom(Object): ImageModel
            {abstract} +canConvertTo(Class<?>): boolean
            {abstract} +convertTo(Object): Object
        }
    }

    namespace it.tidalwave.image {
        abstract class ImageModel [[../ImageModel.html]] {
            #model: Object
            {abstract} +getWidth(): int
            {abstract} +getHeight(): int
            {abstract} +getDataType(): DataType
            {abstract} +getBandCount(): int
            {abstract} +getFactory(): ImplementationFactory
            {abstract} +getColorModel(): ColorModel
            {abstract} +createCopy(boolean): EditableImage
            {abstract} +getInnerProperty(Class<T>): T
            {abstract} #toRenderedImageForSerialization(): RenderedImage
            {abstract} #toObjectForDeserialization(RenderedImage): Object
        }
        abstract class Histogram [[../Histogram.html]] {
            #image: Object
            #bandCount: int
            #bitsPerBand: int
            {abstract} +getFrequencies(int): int[]
            {abstract} +getMin(int): int
            {abstract} +getMax(int): int
            {abstract} +getPreview(PreviewSettings): Histogram
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
