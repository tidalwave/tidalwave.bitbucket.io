@startuml
    namespace it.tidalwave.bluemarine2.model.audio {

        interface Track [[Track.html]] {
            {static} +_Track_: Class<Track>
            {abstract} +getDiskNumber(): Optional<Integer>
            {abstract} +getDiskCount(): Optional<Integer>
            {abstract} +getTrackNumber(): Optional<Integer>
            {abstract} +getDuration(): Optional<Duration>
            {abstract} +getMetadata(): Metadata
            {abstract} +getRecord(): Optional<Record>
            {abstract} +getPerformance(): Optional<Performance>
        }

        interface AudioFile [[AudioFile.html]] {
            {abstract} +findMakers(): MusicArtistFinder
            {abstract} +findComposers(): MusicArtistFinder
            {abstract} +getRecord(): Optional<Record>
            {abstract} +getSize(): long
            {abstract} +getContent(): Optional<Resource>
        }

        interface Performance [[Performance.html]] {
            {static} +_Performance_: Class<Performance>
            {abstract} +findPerformers(): MusicPerformerFinder
        }

        interface MusicPerformer [[MusicPerformer.html]] {
            {static} +_MusicPerformer_: Class<MusicPerformer>
            {abstract} +getMusicArtist(): MusicArtist
            {abstract} +getRole(): Optional<Entity>
        }

        interface Record [[Record.html]] {
            {static} +_Record_: Class<Record>
            {abstract} +getDiskNumber(): Optional<Integer>
            {abstract} +getDiskCount(): Optional<Integer>
            {abstract} +getTrackCount(): Optional<Integer>
            {abstract} +findTracks(): TrackFinder
            {abstract} +getAsin(): Optional<String>
            {abstract} +getGtin(): Optional<String>
            {abstract} +getImageUrl(): Optional<URL>
        }

        interface MusicArtist [[MusicArtist.html]] {
            {static} +_MusicArtist_: Class<MusicArtist>
            {abstract} +findTracks(): TrackFinder
            {abstract} +findRecords(): RecordFinder
            {abstract} +findPerformances(): PerformanceFinder
            {abstract} +getType(): int
            {abstract} +getSource(): Optional<Id>
        }

        it.tidalwave.bluemarine2.model.spi.Entity <|-- Track
        it.tidalwave.bluemarine2.model.spi.SourceAware <|-- Track
        it.tidalwave.role.Identifiable <|-- Track
        it.tidalwave.bluemarine2.model.MediaItem <|-- AudioFile
        it.tidalwave.role.Identifiable <|-- AudioFile
        it.tidalwave.bluemarine2.model.spi.Entity <|-- Performance
        it.tidalwave.bluemarine2.model.spi.SourceAware <|-- Performance
        it.tidalwave.role.Identifiable <|-- Performance
        it.tidalwave.bluemarine2.model.spi.Entity <|-- MusicPerformer
        it.tidalwave.role.Identifiable <|-- MusicPerformer
        it.tidalwave.bluemarine2.model.spi.Entity <|-- Record
        it.tidalwave.bluemarine2.model.spi.SourceAware <|-- Record
        it.tidalwave.role.Identifiable <|-- Record
        it.tidalwave.bluemarine2.model.spi.Entity <|-- MusicArtist
        it.tidalwave.role.Identifiable <|-- MusicArtist
    }

    namespace it.tidalwave.bluemarine2.model.spi {
        interface Entity [[../spi/Entity.html]]
        interface SourceAware [[../spi/SourceAware.html]] {
            {abstract} +getSource(): Optional<Id>
        }
    }

    namespace it.tidalwave.role {
        interface Identifiable {
            {static} +_Identifiable_: Class<Identifiable>
            {abstract} +getId(): Id
            {static} +of(Id): Identifiable
        }
    }

    namespace it.tidalwave.bluemarine2.model {
        interface MediaItem [[../MediaItem.html]] {
            {abstract} +getMetadata(): Metadata
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
