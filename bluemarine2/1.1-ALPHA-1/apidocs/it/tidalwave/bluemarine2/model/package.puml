@startuml
    namespace it.tidalwave.bluemarine2.model {

        class VirtualMediaFolder [[VirtualMediaFolder.html]] {
            +VirtualMediaFolder(MediaFolder, Path, String, EntityCollectionFactory)
            +VirtualMediaFolder(Optional<? extends MediaFolder>, Path, String, EntityCollectionFactory)
            +VirtualMediaFolder(MediaFolder, Path, String, EntityFinderFactory)
            +VirtualMediaFolder(Optional<? extends MediaFolder>, Path, String, EntityFinderFactory)
            +findChildren(): PathAwareFinder
            +toDumpString(): String
        }

        interface it.tidalwave.bluemarine2.model.VirtualMediaFolder.EntityCollectionFactory [[VirtualMediaFolder.EntityCollectionFactory.html]]

        interface it.tidalwave.bluemarine2.model.VirtualMediaFolder.EntityFinderFactory [[VirtualMediaFolder.EntityFinderFactory.html]]

        interface MediaCatalog [[MediaCatalog.html]] {
            {static} +_MediaCatalog_: Class<MediaCatalog>
            {abstract} +findArtists(): MusicArtistFinder
            {abstract} +findRecords(): RecordFinder
            {abstract} +findTracks(): TrackFinder
            {abstract} +findPerformances(): PerformanceFinder
            {abstract} +findAudioFiles(): AudioFileFinder
        }

        class ModelPropertyNames [[ModelPropertyNames.html]] {
            {static} +ROOT_PATH: Key<Path>
        }

        interface MediaFolder [[MediaFolder.html]] {
            {abstract} +findChildren(): PathAwareFinder
            +finderOf(Finder<PathAwareEntity>): PathAwareFinder
            +finderOf(Function<MediaFolder, Collection<? extends PathAwareEntity>>): PathAwareFinder
        }

        interface MediaItem [[MediaItem.html]] {
            {abstract} +getMetadata(): Metadata
        }

        interface it.tidalwave.bluemarine2.model.MediaItem.Metadata [[MediaItem.Metadata.html]] {
            {static} +FILE_SIZE: Key<Long>
            {static} +DURATION: Key<Duration>
            {static} +BIT_RATE: Key<Integer>
            {static} +SAMPLE_RATE: Key<Integer>
            {static} +ARTIST: Key<String>
            {static} +COMPOSER: Key<String>
            {static} +PUBLISHER: Key<String>
            {static} +TITLE: Key<String>
            {static} +YEAR: Key<Integer>
            {static} +ALBUM: Key<String>
            {static} +TRACK_NUMBER: Key<Integer>
            {static} +DISK_NUMBER: Key<Integer>
            {static} +DISK_COUNT: Key<Integer>
            {static} +COMMENT: Key<List<String>>
            {static} +BITS_PER_SAMPLE: Key<Integer>
            {static} +FORMAT: Key<String>
            {static} +ENCODING_TYPE: Key<String>
            {static} +CHANNELS: Key<Integer>
            {static} +ARTWORK: Key<List<byte[]>>
            {static} +MBZ_TRACK_ID: Key<Id>
            {static} +MBZ_WORK_ID: Key<Id>
            {static} +MBZ_DISC_ID: Key<Id>
            {static} +MBZ_ARTIST_ID: Key<List<Id>>
            {static} +ENCODER: Key<List<String>>
            {static} +ITUNES_COMMENT: Key<ITunesComment>
            {static} +CDDB: Key<Cddb>
            {abstract} +get(Key<T>): Optional<T>
            {abstract} +getAll(Key<T>): T
            {abstract} +containsKey(Key<?>): boolean
            {abstract} +getKeys(): Set<Key<?>>
            {abstract} +getEntries(): Set<Entry<Key<?>, ?>>
            {abstract} +with(Key<T>, T): Metadata
            {abstract} +with(Key<T>, Optional<T>): Metadata
            {abstract} +withFallback(Function<Key<?>, Metadata>): Metadata
        }

        class it.tidalwave.bluemarine2.model.MediaItem.Metadata.Cddb [[MediaItem.Metadata.Cddb.html]] {
            +getToc(): String
            +getTrackCount(): int
            +matches(Cddb, int): boolean
            +sameTrackCountOf(Cddb): boolean
            +computeDifference(Cddb): int
        }

        class it.tidalwave.bluemarine2.model.MediaItem.Metadata.ITunesComment [[MediaItem.Metadata.ITunesComment.html]] {
            +getTrackId(): String
            {static} +from(Metadata): Optional<ITunesComment>
            {static} +fromToString(String): ITunesComment
        }

        class PlayList<ENTITY> [[PlayList.html]] {
            +PlayList(ENTITY, Collection<ENTITY>)
            {static} +empty(): PlayList<T>
            +hasPrevious(): boolean
            +hasNext(): boolean
            +previous(): Optional<ENTITY>
            +next(): Optional<ENTITY>
            +peekNext(): Optional<ENTITY>
            +getSize(): int
        }

        interface MediaFileSystem [[MediaFileSystem.html]] {
            {abstract} +getRoot(): MediaFolder
            {abstract} +getRootPath(): Path
        }

        it.tidalwave.bluemarine2.model.spi.EntityWithRoles <|-- VirtualMediaFolder
        MediaFolder <|.. VirtualMediaFolder
        VirtualMediaFolder --> "0..1" it.tidalwave.bluemarine2.model.spi.PathAwareEntity: parent
        java.util.function.Function <|-- it.tidalwave.bluemarine2.model.VirtualMediaFolder.EntityCollectionFactory
        VirtualMediaFolder +-- it.tidalwave.bluemarine2.model.VirtualMediaFolder.EntityCollectionFactory
        java.util.function.Function <|-- it.tidalwave.bluemarine2.model.VirtualMediaFolder.EntityFinderFactory
        VirtualMediaFolder +-- it.tidalwave.bluemarine2.model.VirtualMediaFolder.EntityFinderFactory
        it.tidalwave.bluemarine2.model.spi.PathAwareEntity <|-- MediaFolder
        it.tidalwave.role.SimpleComposite <|-- MediaFolder
        it.tidalwave.bluemarine2.model.spi.PathAwareEntity <|-- MediaItem
        it.tidalwave.bluemarine2.model.role.AudioFileSupplier <|-- MediaItem
        MediaItem +-- it.tidalwave.bluemarine2.model.MediaItem.Metadata
        it.tidalwave.bluemarine2.model.MediaItem.Metadata +-- it.tidalwave.bluemarine2.model.MediaItem.Metadata.Cddb
        it.tidalwave.bluemarine2.model.MediaItem.Metadata +-- it.tidalwave.bluemarine2.model.MediaItem.Metadata.ITunesComment
        it.tidalwave.bluemarine2.model.MediaItem.Metadata.ITunesComment --> it.tidalwave.bluemarine2.model.MediaItem.Metadata.Cddb: cddb
        it.tidalwave.bluemarine2.model.spi.Entity <|-- MediaFileSystem
        it.tidalwave.bluemarine2.model.role.EntityBrowser <|-- MediaFileSystem
    }

    namespace java.util.function {
        interface Function<T, R> {
            {abstract} +apply(T): R
            +compose(Function<? super V, ? extends T>): Function<V, R>
            +andThen(Function<? super R, ? extends V>): Function<T, V>
            {static} +identity(): Function<T, T>
        }
    }

    namespace it.tidalwave.role {
        interface SimpleComposite<TYPE> {
            {static} +_SimpleComposite_: Class<SimpleComposite>
            {static} +of(Finder<TYPE>): SimpleComposite<TYPE>
            {static} +ofCloned(Collection<TYPE>): SimpleComposite<TYPE>
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
