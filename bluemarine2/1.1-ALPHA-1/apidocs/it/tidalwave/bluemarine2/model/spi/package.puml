@startuml
    namespace it.tidalwave.bluemarine2.model.spi {

        class MetadataSupport [[MetadataSupport.html]] {
            #path: Path
            #fallback: Optional<Function<Key<?>, Metadata>>
            #properties: Map<Key<?>, Object>
            +MetadataSupport(Path)
            +get(Key<T>): Optional<T>
            +getAll(Key<T>): T
            +containsKey(Key<?>): boolean
            +getKeys(): Set<Key<?>>
            +getEntries(): Set<Entry<Key<?>, ?>>
            +with(Key<T>, T): Metadata
            +withFallback(Function<Key<?>, Metadata>): Metadata
            +with(Key<T>, Optional<T>): Metadata
            #put(Key<V>, V): void
        }

        class EntityWithRoles [[EntityWithRoles.html]] {
            +EntityWithRoles()
            +EntityWithRoles(Collection<Object>)
            #EntityWithRoles(RoleProvider, Collection<Object>)
        }

        interface Entity [[Entity.html]]

        interface PathAwareEntity [[PathAwareEntity.html]] {
            {static} +_PathAwareEntity_: Class<PathAwareEntity>
            {abstract} +getParent(): Optional<PathAwareEntity>
            {abstract} +getPath(): Path
            +getRelativePath(): Path
        }

        interface CacheManager [[CacheManager.html]] {
            {abstract} +getCache(Object): Cache
        }

        interface it.tidalwave.bluemarine2.model.spi.CacheManager.Cache [[CacheManager.Cache.html]] {
            {abstract} +getCachedObject(Object, Supplier<T>): T
        }

        interface SourceAwareFinder<ENTITY, FINDER> [[SourceAwareFinder.html]] {
            {abstract} +withId(Id): FINDER
            {abstract} +importedFrom(Id): FINDER
            {abstract} +importedFrom(Optional<Id>): FINDER
            {abstract} +withFallback(Id): FINDER
            {abstract} +withFallback(Optional<Id>): FINDER
        }

        interface SourceAware [[SourceAware.html]] {
            {abstract} +getSource(): Optional<Id>
        }

        class NamedEntity [[NamedEntity.html]] {
            +NamedEntity(String)
        }

        interface PathAwareFinder [[PathAwareFinder.html]] {
            {abstract} +withPath(Path): PathAwareFinder
            +withPath(String): PathAwareFinder
        }

        it.tidalwave.bluemarine2.model.MediaItem.Metadata <|.. MetadataSupport
        Entity <|.. EntityWithRoles
        it.tidalwave.util.As <|-- Entity
        it.tidalwave.bluemarine2.util.Dumpable <|-- Entity
        Entity <|-- PathAwareEntity
        CacheManager +-- it.tidalwave.bluemarine2.model.spi.CacheManager.Cache
        EntityWithRoles <|-- NamedEntity
        it.tidalwave.util.spi.ExtendedFinderSupport <|-- PathAwareFinder
    }

    namespace it.tidalwave.bluemarine2.model {
        interface it.tidalwave.bluemarine2.model.MediaItem.Metadata [[../MediaItem.Metadata.html]] {
            {static} +FILE_SIZE: Key<Long>
            {static} +DURATION: Key<Duration>
            {static} +BIT_RATE: Key<Integer>
            {static} +SAMPLE_RATE: Key<Integer>
            {static} +ARTIST: Key<String>
            {static} +COMPOSER: Key<String>
            {static} +PUBLISHER: Key<String>
            {static} +TITLE: Key<String>
            {static} +YEAR: Key<Integer>
            {static} +ALBUM: Key<String>
            {static} +TRACK_NUMBER: Key<Integer>
            {static} +DISK_NUMBER: Key<Integer>
            {static} +DISK_COUNT: Key<Integer>
            {static} +COMMENT: Key<List<String>>
            {static} +BITS_PER_SAMPLE: Key<Integer>
            {static} +FORMAT: Key<String>
            {static} +ENCODING_TYPE: Key<String>
            {static} +CHANNELS: Key<Integer>
            {static} +ARTWORK: Key<List<byte[]>>
            {static} +MBZ_TRACK_ID: Key<Id>
            {static} +MBZ_WORK_ID: Key<Id>
            {static} +MBZ_DISC_ID: Key<Id>
            {static} +MBZ_ARTIST_ID: Key<List<Id>>
            {static} +ENCODER: Key<List<String>>
            {static} +ITUNES_COMMENT: Key<ITunesComment>
            {static} +CDDB: Key<Cddb>
            {abstract} +get(Key<T>): Optional<T>
            {abstract} +getAll(Key<T>): T
            {abstract} +containsKey(Key<?>): boolean
            {abstract} +getKeys(): Set<Key<?>>
            {abstract} +getEntries(): Set<Entry<Key<?>, ?>>
            {abstract} +with(Key<T>, T): Metadata
            {abstract} +with(Key<T>, Optional<T>): Metadata
            {abstract} +withFallback(Function<Key<?>, Metadata>): Metadata
        }
    }

    namespace it.tidalwave.util {
        interface As {
            {abstract} +as(Class<T>): T
            {abstract} +as(Class<T>, NotFoundBehaviour<T>): T
            +maybeAs(Class<T>): Optional<T>
            +--asOptional--(Class<T>): Optional<T>
            {abstract} +asMany(Class<T>): Collection<T>
        }
    }

    namespace it.tidalwave.bluemarine2.util {
        interface Dumpable [[../../util/Dumpable.html]] {
            +toDumpString(): String
        }
    }

    namespace it.tidalwave.util.spi {
        interface ExtendedFinderSupport<TYPE, EXTENDED_FINDER extends Finder<TYPE>> {
            {abstract} +from(int): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +max(int): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +sort(SortCriterion): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +sort(SortCriterion, SortDirection): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +withContext(Object): EXTENDED_FINDER extends Finder<TYPE>
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
