@startuml
    namespace it.tidalwave.bluemarine2.util {

        class Miscellaneous [[Miscellaneous.html]] {
            {static} +normalizedToNativeForm(String): String
            {static} +normalizedPath(Path): Path
            {static} +toFileBMT46(Path): File
        }

        class RdfUtilities [[RdfUtilities.html]] {
            {static} +exportToFile(Model, Path): void
            {static} +streamOf(Iteration<T, X extends RuntimeException>): Stream<T>
            {static} +literalFor(Path): Value
            {static} +literalFor(String): Value
            {static} +literalFor(Optional<String>): Optional<Value>
            {static} +literalFor(Id): Value
            {static} +literalFor(int): Value
            {static} +literalForInt(Optional<Integer>): Optional<Value>
            {static} +literalFor(long): Value
            {static} +literalForLong(Optional<Long>): Optional<Value>
            {static} +literalFor(short): Value
            {static} +literalFor(float): Value
            {static} +literalForFloat(Optional<Float>): Optional<Value>
            {static} +literalFor(Instant): Value
            {static} +uriFor(Id): IRI
            {static} +uriFor(String): IRI
            {static} +uriFor(URL): IRI
            {static} +urlFor(IRI): URL
            {static} +emptyWhenNull(String): String
            {static} +createSha1Id(String): Id
            {static} +createSha1IdNew(String): Id
        }

        class Formatters [[Formatters.html]] {
            {static} +xmlPrettyPrinted(String): String
            {static} +format(Duration): String
            {static} +toHexString(byte[]): String
            {static} +toBase64String(byte[]): String
        }

        interface Dumpable [[Dumpable.html]] {
            +toDumpString(): String
        }

        class ModelBuilder [[ModelBuilder.html]] {
            +ModelBuilder(Resource...)
            +toModel(): Model
            +with(Resource, IRI, Value, Resource...): ModelBuilder
            +with(Resource, IRI, Optional<Value>, Resource...): ModelBuilder
            +withOptional(Optional<? extends Resource>, IRI, Value): ModelBuilder
            +withOptional(Resource, IRI, Optional<? extends Value>): ModelBuilder
            +withOptional(Optional<? extends Resource>, IRI, Optional<? extends Value>): ModelBuilder
            +with(List<? extends Resource>, IRI, Value): ModelBuilder
            +with(List<? extends Resource>, IRI, List<? extends Value>): ModelBuilder
            +with(Resource, IRI, Stream<? extends Value>): ModelBuilder
            +withOptional(Optional<? extends Resource>, IRI, Stream<? extends Value>): ModelBuilder
            +with(Statement): ModelBuilder
            +with(Optional<ModelBuilder>): ModelBuilder
            +with(ModelBuilder): ModelBuilder
            +with(Model): ModelBuilder
            +with(List<ModelBuilder>): ModelBuilder
        }

        class ImmutableTupleQueryResult [[ImmutableTupleQueryResult.html]] {
            +ImmutableTupleQueryResult(TupleQueryResult)
            +ImmutableTupleQueryResult(ImmutableTupleQueryResult)
            +getBindingNames(): List<String>
            +hasNext(): boolean
            +next(): BindingSet
            +close(): void
            +remove(): void
        }

        class SortingRDFHandler [[SortingRDFHandler.html]] {
            +handleStatement(Statement): void
            +endRDF(): void
        }

        class SystemConfigurer [[SystemConfigurer.html]] {
            {static} +setupSlf4jBridgeHandler(): void
            {static} +setSystemProperties(): void
        }

        org.eclipse.rdf4j.query.TupleQueryResult <|.. ImmutableTupleQueryResult
        org.eclipse.rdf4j.rio.RDFHandler <|.. SortingRDFHandler
    }

    namespace org.eclipse.rdf4j.query {
        interface TupleQueryResult {
            {abstract} +getBindingNames(): List<String>
        }
    }

    namespace org.eclipse.rdf4j.rio {
        interface RDFHandler {
            {abstract} +startRDF(): void
            {abstract} +endRDF(): void
            {abstract} +handleNamespace(String, String): void
            {abstract} +handleStatement(Statement): void
            {abstract} +handleComment(String): void
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
