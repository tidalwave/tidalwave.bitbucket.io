@startuml
    namespace it.tidalwave.bluemarine2.model.finder.audio {

        interface MusicArtistFinder [[MusicArtistFinder.html]] {
            {abstract} +makerOf(Id): MusicArtistFinder
            +makerOf(Identifiable): MusicArtistFinder
        }

        interface AudioFileFinder [[AudioFileFinder.html]]

        interface RecordFinder [[RecordFinder.html]] {
            {abstract} +madeBy(Id): RecordFinder
            +madeBy(MusicArtist): RecordFinder
            {abstract} +containingTrack(Id): RecordFinder
            +containingTrack(Track): RecordFinder
        }

        interface TrackFinder [[TrackFinder.html]] {
            {abstract} +madeBy(Id): TrackFinder
            +madeBy(MusicArtist): TrackFinder
            {abstract} +inRecord(Id): TrackFinder
            +inRecord(Record): TrackFinder
        }

        interface PerformanceFinder [[PerformanceFinder.html]] {
            {abstract} +ofTrack(Id): PerformanceFinder
            +ofTrack(Track): PerformanceFinder
            {abstract} +performedBy(Id): PerformanceFinder
            +performedBy(MusicArtist): PerformanceFinder
        }

        interface MusicPerformerFinder [[MusicPerformerFinder.html]] {
            {abstract} +performerOf(Id): MusicPerformerFinder
            +performerOf(Performance): MusicPerformerFinder
        }

        it.tidalwave.bluemarine2.model.spi.SourceAwareFinder <|-- MusicArtistFinder
        it.tidalwave.util.spi.ExtendedFinderSupport <|-- MusicArtistFinder
        it.tidalwave.bluemarine2.model.spi.SourceAwareFinder <|-- AudioFileFinder
        it.tidalwave.util.spi.ExtendedFinderSupport <|-- AudioFileFinder
        it.tidalwave.bluemarine2.model.spi.SourceAwareFinder <|-- RecordFinder
        it.tidalwave.util.spi.ExtendedFinderSupport <|-- RecordFinder
        it.tidalwave.bluemarine2.model.spi.SourceAwareFinder <|-- TrackFinder
        it.tidalwave.util.spi.ExtendedFinderSupport <|-- TrackFinder
        it.tidalwave.bluemarine2.model.spi.SourceAwareFinder <|-- PerformanceFinder
        it.tidalwave.util.spi.ExtendedFinderSupport <|-- PerformanceFinder
        it.tidalwave.bluemarine2.model.spi.SourceAwareFinder <|-- MusicPerformerFinder
        it.tidalwave.util.spi.ExtendedFinderSupport <|-- MusicPerformerFinder
    }

    namespace it.tidalwave.bluemarine2.model.spi {
        interface SourceAwareFinder<ENTITY, FINDER> [[../../spi/SourceAwareFinder.html]] {
            {abstract} +withId(Id): FINDER
            {abstract} +importedFrom(Id): FINDER
            {abstract} +importedFrom(Optional<Id>): FINDER
            {abstract} +withFallback(Id): FINDER
            {abstract} +withFallback(Optional<Id>): FINDER
        }
    }

    namespace it.tidalwave.util.spi {
        interface ExtendedFinderSupport<TYPE, EXTENDED_FINDER extends Finder<TYPE>> [[http://blueMarine.tidalwave.it/bluemarine2-modules/it-tidalwave-bluemarine2-commons/apidocs/it/tidalwave/util/spi/ExtendedFinderSupport.html?is-external=true]] {
            {abstract} +from(int): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +max(int): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +sort(SortCriterion): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +sort(SortCriterion, SortDirection): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +withContext(Object): EXTENDED_FINDER extends Finder<TYPE>
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
