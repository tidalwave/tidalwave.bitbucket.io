@startuml
    namespace it.tidalwave.northernwind.model.impl.admin {

        class AdminModelFactory [[AdminModelFactory.html]] {
            +build(Builder): Resource
            +build(Builder): Content
            +createSiteNode(Site, ResourceFile): SiteNode
            +build(Builder): ResourceProperties
        }

        class PatchedTextResourcePropertyResolver [[PatchedTextResourcePropertyResolver.html]] {
            +resolveProperty(Id, Key<Type>): Type
        }

        class AdminContent [[AdminContent.html]] {
            +AdminContent(Builder)
            +getExposedUri(): Optional<ResourcePath>
            +findChildren(): Finder<Content>
            +getProperties(): ResourceProperties
        }

        class AdminIdFactory [[AdminIdFactory.html]] {
            +createId(): Id
            +createId(Class<?>): Id
            +createId(Class<?>, Object): Id
        }

        class ResourceFinder<T extends Resource> [[ResourceFinder.html]] {
            +ResourceFinder(ResourceFinder<T extends Resource>, Object)
            #computeResults(): List<? extends Resource>
        }

        class AdminMimeTypeResolver [[AdminMimeTypeResolver.html]] {
            +getMimeType(String): String
        }

        class AdminResource [[AdminResource.html]] {
            +AdminResource(Builder)
            +getProperties(): ResourceProperties
            +isPlaceHolder(): boolean
        }

        class AdminSiteNode [[AdminSiteNode.html]] {
            +AdminSiteNode(Site, ModelFactory, ResourceFile)
            +getLayout(): Layout
            +getRelativeUri(): ResourcePath
            +findChildren(): Finder<SiteNode>
        }

        class AdminSiteProvider [[AdminSiteProvider.html]] {
            +reload(): void
            +isSiteAvailable(): boolean
            +getVersionString(): String
        }

        class AdminSite [[AdminSite.html]] {
            +getContextPath(): String
            +createLink(ResourcePath): String
            +find(Class<Type>): SiteFinder<Type>
            +getFileSystemProvider(): ResourceFileSystemProvider
            +getConfiguredLocales(): List<Locale>
            +getTemplate(Class<?>, Optional<ResourcePath>, String): Template
            +getTemplate(Class<?>, ResourcePath): Optional<String>
        }

        it.tidalwave.northernwind.core.model.spi.ModelFactorySupport <|-- AdminModelFactory
        it.tidalwave.northernwind.core.model.ResourceProperties.PropertyResolver <|.. PatchedTextResourcePropertyResolver
        it.tidalwave.northernwind.core.model.spi.ContentSupport <|-- AdminContent
        it.tidalwave.role.IdFactory <|.. AdminIdFactory
        it.tidalwave.util.spi.SimpleFinderSupport <|-- ResourceFinder
        it.tidalwave.northernwind.core.model.MimeTypeResolver <|.. AdminMimeTypeResolver
        it.tidalwave.northernwind.core.model.spi.ResourceSupport <|-- AdminResource
        it.tidalwave.northernwind.core.model.spi.SiteNodeSupport <|-- AdminSiteNode
        it.tidalwave.northernwind.core.model.SiteProvider <|.. AdminSiteProvider
        it.tidalwave.northernwind.core.model.Site <|.. AdminSite
    }

    namespace it.tidalwave.northernwind.core.model.spi {
        class ModelFactorySupport
        abstract class ContentSupport {
            #modelFactory: ModelFactory
        }
        abstract class ResourceSupport {
            #modelFactory: ModelFactory
        }
        abstract class SiteNodeSupport {
            #modelFactory: ModelFactory
        }
    }

    namespace it.tidalwave.northernwind.core.model {
        interface it.tidalwave.northernwind.core.model.ResourceProperties.PropertyResolver {
            {static} +DEFAULT: PropertyResolver
            {abstract} +resolveProperty(Id, Key<T>): T
        }
        interface MimeTypeResolver {
            {abstract} +getMimeType(String): String
        }
        interface SiteProvider {
            {static} +SiteProvider: Class<SiteProvider>
            {abstract} +getSite(): Site
            {abstract} +reload(): void
            {abstract} +isSiteAvailable(): boolean
            {abstract} +getVersionString(): String
        }
        interface Site {
            {abstract} +getContextPath(): String
            {abstract} +createLink(ResourcePath): String
            {abstract} +find(Class<TYPE>): SiteFinder<TYPE>
            {abstract} +getFileSystemProvider(): ResourceFileSystemProvider
            {abstract} +getConfiguredLocales(): List<Locale>
            {abstract} +getTemplate(Class<?>, Optional<ResourcePath>, String): Template
            {abstract} +--getTemplate--(Class<?>, ResourcePath): Optional<String>
        }
    }

    namespace it.tidalwave.role {
        interface IdFactory {
            {static} +IdFactory: Class<IdFactory>
            {abstract} +createId(): Id
            {abstract} +createId(Class<?>): Id
            {abstract} +createId(Class<?>, Object): Id
        }
    }

    namespace it.tidalwave.util.spi {
        abstract class SimpleFinderSupport<T>
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
