@startuml
    namespace it.tidalwave.northernwind.frontend.ui.spi {

        class DefaultRenderContext [[DefaultRenderContext.html]] {
        }

        class VirtualSiteNode [[VirtualSiteNode.html]] {
            +getSite(): Site
            +getLayout(): Layout
            +getFile(): ResourceFile
            +getPropertyGroup(Id): ResourceProperties
            +isPlaceHolder(): boolean
            +findChildren(): Finder<SiteNode>
        }

        class NodeViewRenderer<T> [[NodeViewRenderer.html]] {
            +NodeViewRenderer(Request, RequestContext, ViewAndControllerLayoutBuilder, BiConsumer<? super T, ? super T>)
            +preVisit(Layout): void
            +postVisit(Layout): void
        }

        class ViewAndControllerLayoutBuilder [[ViewAndControllerLayoutBuilder.html]] {
            +visit(Layout): void
            +getViewAndControllerFor(Layout): Optional<ViewAndController>
        }

        class DefaultSiteViewController [[DefaultSiteViewController.html]] {
            +processRequest(Request): RESPONSE_TYPE
        }

        it.tidalwave.northernwind.frontend.ui.RenderContext <|.. DefaultRenderContext
        it.tidalwave.northernwind.core.model.SiteNode <|.. VirtualSiteNode
        it.tidalwave.role.Composite.Visitor <|.. NodeViewRenderer
        it.tidalwave.role.Composite.Visitor <|.. ViewAndControllerLayoutBuilder
        ViewAndControllerLayoutBuilder --> "0..1" ViewAndControllerLayoutBuilder: value
        it.tidalwave.northernwind.frontend.ui.SiteViewController <|.. DefaultSiteViewController
    }

    namespace it.tidalwave.northernwind.frontend.ui {
        interface RenderContext [[../RenderContext.html]] {
            {abstract} +getRequest(): Request
            {abstract} +getRequestContext(): RequestContext
            +setDynamicNodeProperty(Key<T>, T): void
            +getQueryParam(String): Optional<String>
            +getPathParams(SiteNode): ResourcePath
        }
        interface SiteViewController [[../SiteViewController.html]] {
            {abstract} +processRequest(Request): ResponseType
        }
    }

    namespace it.tidalwave.northernwind.core.model {
        interface SiteNode [[../../../core/model/SiteNode.html]] {
            {static} +_SiteNode_: Class<SiteNode>
            {static} +P_NAVIGATION_LABEL: Key<String>
            {static} +P_MANAGES_PATH_PARAMS: Key<Boolean>
            {abstract} +getSite(): Site
            {abstract} +getLayout(): Layout
            {abstract} +getRelativeUri(): ResourcePath
        }
    }

    namespace it.tidalwave.role {
        interface it.tidalwave.role.Composite.Visitor<T, R> {
            +preVisit(T): void
            +visit(T): void
            +postVisit(T): void
            +getValue(): Optional<R>
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
