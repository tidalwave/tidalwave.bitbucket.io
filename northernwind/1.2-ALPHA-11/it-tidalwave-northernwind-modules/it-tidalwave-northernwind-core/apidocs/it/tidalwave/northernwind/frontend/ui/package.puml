@startuml
    namespace it.tidalwave.northernwind.frontend.ui {

        interface LayoutFinder [[LayoutFinder.html]] {
            {abstract} +withId(Id): LayoutFinder
        }

        interface ViewFactory [[ViewFactory.html]] {
            {abstract} +createViewAndController(String, Id, SiteNode): ViewAndController
        }

        class it.tidalwave.northernwind.frontend.ui.ViewFactory.ViewAndController [[ViewFactory.ViewAndController.html]] {
            +renderView(RenderContext): Object
        }

        interface RenderContext [[RenderContext.html]] {
            {abstract} +getRequest(): Request
            {abstract} +getRequestContext(): RequestContext
            +setDynamicNodeProperty(Key<T>, T): void
            +getQueryParam(String): Optional<String>
            +getPathParams(SiteNode): ResourcePath
        }

        interface Layout [[Layout.html]] {
            {abstract} +getTypeUri(): String
            {abstract} +withChild(Layout): Layout
            {abstract} +withOverride(Layout): Layout
            {abstract} +accept(Visitor<? super Layout, T>): Optional<T>
            {abstract} +createViewAndController(SiteNode): ViewAndController
        }

        class it.tidalwave.northernwind.frontend.ui.Layout.Builder [[Layout.Builder.html]] {
            +build(): Layout
        }

        interface it.tidalwave.northernwind.frontend.ui.Layout.Builder.CallBack [[Layout.Builder.CallBack.html]] {
            {abstract} +build(Builder): Layout
        }

        interface SiteViewController [[SiteViewController.html]] {
            {abstract} +processRequest(Request): ResponseType
        }

        interface ViewController [[ViewController.html]] {
            +initialize(): void
            +prepareRendering(RenderContext): void
            +renderView(RenderContext): void
            +findVirtualSiteNodes(): Finder<SiteNode>
        }

        interface SiteView [[SiteView.html]] {
            {static} +NW: String
            {abstract} +renderSiteNode(Request, SiteNode): void
        }

        it.tidalwave.util.spi.ExtendedFinderSupport <|-- LayoutFinder
        ViewFactory +-- it.tidalwave.northernwind.frontend.ui.ViewFactory.ViewAndController
        it.tidalwave.util.As <|-- Layout
        it.tidalwave.role.Identifiable <|-- Layout
        it.tidalwave.role.Composite <|-- Layout
        Layout +-- it.tidalwave.northernwind.frontend.ui.Layout.Builder
        it.tidalwave.northernwind.frontend.ui.Layout.Builder +-- it.tidalwave.northernwind.frontend.ui.Layout.Builder.CallBack
    }

    namespace it.tidalwave.util.spi {
        interface ExtendedFinderSupport<T, F extends Finder<T>> {
            {abstract} +from(int): F extends Finder<T>
            {abstract} +max(int): F extends Finder<T>
            {abstract} +sort(SortCriterion): F extends Finder<T>
            {abstract} +sort(SortCriterion, SortDirection): F extends Finder<T>
            {abstract} +withContext(Object): F extends Finder<T>
        }
    }

    namespace it.tidalwave.util {
        interface As {
            {static} +forObject(Object): As
            {static} +forObject(Object, Object): As
            {static} +forObject(Object, Collection<Object>): As
            +as(Class<? extends T>): T
            {abstract} +maybeAs(Class<? extends T>): Optional<T>
            {abstract} +asMany(Class<? extends T>): Collection<T>
            {static} +type(Class<?>): Type<T>
            +as(Type<? extends T>): T
            +maybeAs(Type<? extends T>): Optional<T>
            +asMany(Type<? extends T>): Collection<T>
        }
    }

    namespace it.tidalwave.role {
        interface Identifiable {
            {static} +_Identifiable_: Class<Identifiable>
            {abstract} +getId(): Id
            {static} +of(Id): Identifiable
        }
        interface Composite<T, F extends Finder<? extends T>> {
            {static} +_Composite_: Class<Composite>
            {static} +DEFAULT: Composite<Object, Finder<Object>>
            {abstract} +findChildren(): F extends Finder<? extends T>
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
