@startuml
    set namespaceSeparator none
    hide empty fields
    hide empty methods

    abstract class "<size:14>DecoratedResourceFileSupport\n<size:10>it.tidalwave.northernwind.core.model.spi" as it.tidalwave.northernwind.core.model.spi.DecoratedResourceFileSupport [[DecoratedResourceFileSupport.html]] {
        #fileSystem: DecoratedResourceFileSystem
        +delete(): void
        +getParent(): ResourceFile
        +createFolder(String): ResourceFile
        +findChildren(): Finder
    }

    interface "<size:14>ResourceFile\n<size:10>it.tidalwave.northernwind.core.model" as it.tidalwave.northernwind.core.model.ResourceFile [[../ResourceFile.html]] {
        {abstract} +getFileSystem(): ResourceFileSystem
        {abstract} +getName(): String
        {abstract} +getPath(): ResourcePath
        {abstract} +isFolder(): boolean
        {abstract} +isData(): boolean
        {abstract} +getMimeType(): String
        {abstract} +getInputStream(): InputStream
        {abstract} +asText(String): String
        {abstract} +asBytes(): byte[]
        {abstract} +getLatestModificationTime(): ZonedDateTime
        {abstract} +getParent(): ResourceFile
        {abstract} +toFile(): File
        {abstract} +delete(): void
        {abstract} +createFolder(String): ResourceFile
        {abstract} +copyTo(ResourceFile): void
    }

    it.tidalwave.northernwind.core.model.ResourceFile <|.. it.tidalwave.northernwind.core.model.spi.DecoratedResourceFileSupport

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
