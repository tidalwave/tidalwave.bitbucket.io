@startuml
    set namespaceSeparator none
    hide empty fields
    hide empty methods

    abstract class "<size:14>ResponseBuilderSupport\n<size:10>it.tidalwave.northernwind.core.model.spi" as it.tidalwave.northernwind.core.model.spi.ResponseBuilderSupport<RESPONSE_TYPE> [[ResponseBuilderSupport.html]] {
        {static} #HEADER_CONTENT_LENGTH: String
        {static} #HEADER_ETAG: String
        {static} #HEADER_CONTENT_TYPE: String
        {static} #HEADER_CONTENT_DISPOSITION: String
        {static} #HEADER_LAST_MODIFIED: String
        {static} #HEADER_EXPIRES: String
        {static} #HEADER_LOCATION: String
        {static} #HEADER_IF_MODIFIED_SINCE: String
        {static} #HEADER_IF_NONE_MATCH: String
        {static} #HEADER_CACHE_CONTROL: String
        {static} #PATTERN_RFC1123: String
        #body: Object
        #httpStatus: int
        #requestIfNoneMatch: Optional<String>
        #requestIfModifiedSince: Optional<ZonedDateTime>
        {abstract} +withHeader(String, String): ResponseBuilder<RESPONSE_TYPE>
        +withHeaders(Map<String, String>): ResponseBuilder<RESPONSE_TYPE>
        +withContentType(String): ResponseBuilder<RESPONSE_TYPE>
        +withContentLength(long): ResponseBuilder<RESPONSE_TYPE>
        +withContentDisposition(String): ResponseBuilder<RESPONSE_TYPE>
        +withExpirationTime(Duration): ResponseBuilder<RESPONSE_TYPE>
        +withLatestModifiedTime(ZonedDateTime): ResponseBuilder<RESPONSE_TYPE>
        +withBody(Object): ResponseBuilder<RESPONSE_TYPE>
        +fromFile(ResourceFile): ResponseBuilder<RESPONSE_TYPE>
        +forRequest(Request): ResponseBuilder<RESPONSE_TYPE>
        +forException(NotFoundException): ResponseBuilder<RESPONSE_TYPE>
        +forException(Throwable): ResponseBuilder<RESPONSE_TYPE>
        +forException(HttpStatusException): ResponseBuilder<RESPONSE_TYPE>
        +withStatus(int): ResponseBuilder<RESPONSE_TYPE>
        +permanentRedirect(String): ResponseBuilder<RESPONSE_TYPE>
        +build(): RESPONSE_TYPE
        +put(): void
        {abstract} #doBuild(): RESPONSE_TYPE
        {abstract} #getHeader(String): Optional<String>
        #getDateTimeHeader(String): Optional<ZonedDateTime>
        #cacheSupport(): ResponseBuilder<RESPONSE_TYPE>
    }

    interface "<size:14>ResponseBuilder\n<size:10>it.tidalwave.northernwind.core.model.spi" as it.tidalwave.northernwind.core.model.spi.ResponseBuilder<RESPONSE_TYPE> [[ResponseBuilder.html]] {
        {abstract} +withHeader(String, String): ResponseBuilder<RESPONSE_TYPE>
        {abstract} +withHeaders(Map<String, String>): ResponseBuilder<RESPONSE_TYPE>
        {abstract} +withContentType(String): ResponseBuilder<RESPONSE_TYPE>
        {abstract} +withContentLength(long): ResponseBuilder<RESPONSE_TYPE>
        {abstract} +withContentDisposition(String): ResponseBuilder<RESPONSE_TYPE>
        {abstract} +withExpirationTime(Duration): ResponseBuilder<RESPONSE_TYPE>
        {abstract} +withLatestModifiedTime(ZonedDateTime): ResponseBuilder<RESPONSE_TYPE>
        {abstract} +withBody(Object): ResponseBuilder<RESPONSE_TYPE>
        {abstract} +fromFile(ResourceFile): ResponseBuilder<RESPONSE_TYPE>
        {abstract} +forRequest(Request): ResponseBuilder<RESPONSE_TYPE>
        {abstract} +forException(NotFoundException): ResponseBuilder<RESPONSE_TYPE>
        {abstract} +forException(HttpStatusException): ResponseBuilder<RESPONSE_TYPE>
        {abstract} +forException(Throwable): ResponseBuilder<RESPONSE_TYPE>
        {abstract} +withStatus(int): ResponseBuilder<RESPONSE_TYPE>
        {abstract} +permanentRedirect(String): ResponseBuilder<RESPONSE_TYPE>
        {abstract} +build(): RESPONSE_TYPE
        {abstract} +put(): void
    }

    it.tidalwave.northernwind.core.model.spi.ResponseBuilder <|.. it.tidalwave.northernwind.core.model.spi.ResponseBuilderSupport

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
