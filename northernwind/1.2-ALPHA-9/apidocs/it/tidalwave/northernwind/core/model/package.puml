@startuml
    namespace it.tidalwave.northernwind.core.model {

        interface ResourceFileSystem [[ResourceFileSystem.html]] {
            {abstract} +getRoot(): ResourceFile
            {abstract} +findFileByPath(String): ResourceFile
        }

        interface ModelFactory [[ModelFactory.html]] {
            {abstract} +createResource(): Builder
            {abstract} +createContent(): Builder
            {abstract} +createMedia(): Builder
            {abstract} +createSiteNode(Site, ResourceFile): SiteNode
            {abstract} +createLayout(): Builder
            {abstract} +createRequest(): Request
            {abstract} +createRequestFrom(HttpServletRequest): Request
            {abstract} +createProperties(): Builder
            {abstract} +createSite(): Builder
        }

        class ResourceFileSystemChangedEvent [[ResourceFileSystemChangedEvent.html]] {
        }

        class ResourcePath [[ResourcePath.html]] {
            {static} +of(String): ResourcePath
            {static} +of(List<String>): ResourcePath
            +relativeTo(ResourcePath): ResourcePath
            +getLeading(): String
            +getTrailing(): String
            +getSegment(int): String
            +getExtension(): String
            +withoutLeading(): ResourcePath
            +withoutTrailing(): ResourcePath
            +startsWith(String): boolean
            +getSegmentCount(): int
            +isEmpty(): boolean
            +prependedWith(ResourcePath): ResourcePath
            +prependedWith(String): ResourcePath
            +appendedWith(ResourcePath): ResourcePath
            +appendedWith(String): ResourcePath
            +urlDecoded(): ResourcePath
            +asString(): String
        }

        interface RequestLocaleManager [[RequestLocaleManager.html]] {
            {abstract} +setLocale(Locale): boolean
            {abstract} +getLocales(): List<Locale>
            {abstract} +getLocaleSuffixes(): List<String>
            {abstract} +getDateTimeFormatter(): DateTimeFormatter
        }

        interface ResourceFile [[ResourceFile.html]] {
            {abstract} +getFileSystem(): ResourceFileSystem
            {abstract} +getName(): String
            {abstract} +getPath(): ResourcePath
            {abstract} +isFolder(): boolean
            {abstract} +isData(): boolean
            {abstract} +getMimeType(): String
            {abstract} +getInputStream(): InputStream
            {abstract} +asText(String): String
            {abstract} +asBytes(): byte[]
            {abstract} +getLatestModificationTime(): ZonedDateTime
            {abstract} +getParent(): ResourceFile
            {abstract} +toFile(): File
            {abstract} +delete(): void
            {abstract} +createFolder(String): ResourceFile
            {abstract} +copyTo(ResourceFile): void
        }

        interface it.tidalwave.northernwind.core.model.ResourceFile.Finder [[ResourceFile.Finder.html]] {
            {abstract} +withRecursion(boolean): Finder
            {abstract} +withName(String): Finder
            {abstract} +getName(): String
            {abstract} +isRecursive(): boolean
        }

        interface SiteNode [[SiteNode.html]] {
            {static} +_SiteNode_: Class<SiteNode>
            {static} +P_NAVIGATION_LABEL: Key<String>
            {static} +P_MANAGES_PATH_PARAMS: Key<Boolean>
            {abstract} +getSite(): Site
            {abstract} +getLayout(): Layout
            {abstract} +getRelativeUri(): ResourcePath
        }

        interface Template [[Template.html]] {
            {abstract} +addAttribute(String, Object): Template
            {abstract} +render(Aggregates...): String
            +render(List<Aggregates>): String
        }

        class it.tidalwave.northernwind.core.model.Template.Aggregate [[Template.Aggregate.html]] {
            {static} +of(String, Object): Aggregate
            {static} +of(String, Optional<?>): Aggregate
            +with(String, Object): Aggregate
            +with(String, Optional<?>): Aggregate
            +get(String): Optional<Object>
        }

        class it.tidalwave.northernwind.core.model.Template.Aggregates [[Template.Aggregates.html]] {
            +Aggregates(String, List<Aggregate>)
            +isEmpty(): boolean
            +iterator(): Iterator<Aggregate>
            +stream(): Stream<Aggregate>
            {static} +toAggregates(String): Collector<Aggregate, ?, Aggregates>
            +getSize(): int
        }

        interface Resource [[Resource.html]] {
            {static} +_Resource_: Class<Resource>
            {static} +P_EXPOSED_URI: Key<String>
            {static} +P_PLACE_HOLDER: Key<Boolean>
            {abstract} +getFile(): ResourceFile
            {abstract} +getProperties(): ResourceProperties
            +getProperty(Key<T>): Optional<T>
            +getProperty(List<Key<T>>): Optional<T>
            {abstract} +getPropertyGroup(Id): ResourceProperties
            {abstract} +isPlaceHolder(): boolean
        }

        class it.tidalwave.northernwind.core.model.Resource.Builder [[Resource.Builder.html]] {
            +build(): Resource
        }

        interface it.tidalwave.northernwind.core.model.Resource.Builder.CallBack [[Resource.Builder.CallBack.html]] {
            {abstract} +build(Builder): Resource
        }

        interface ResourceProperties [[ResourceProperties.html]] {
            {abstract} +getProperty(Key<T>): Optional<T>
            +getProperty(List<Key<T>>): Optional<T>
            {abstract} +getGroup(Id): ResourceProperties
            {abstract} +getKeys(): Collection<Key<?>>
            {abstract} +getGroupIds(): Collection<Id>
            {abstract} +withProperty(Key<T>, T): ResourceProperties
            {abstract} +withoutProperty(Key<?>): ResourceProperties
            {abstract} +withProperties(ResourceProperties): ResourceProperties
            {abstract} +merged(ResourceProperties): ResourceProperties
            {abstract} +withId(Id): ResourceProperties
        }

        class it.tidalwave.northernwind.core.model.ResourceProperties.Builder [[ResourceProperties.Builder.html]] {
            +build(): ResourceProperties
            +withSafeValues(TypeSafeMap): Builder
        }

        interface it.tidalwave.northernwind.core.model.ResourceProperties.Builder.CallBack [[ResourceProperties.Builder.CallBack.html]] {
            {abstract} +build(Builder): ResourceProperties
        }

        interface it.tidalwave.northernwind.core.model.ResourceProperties.PropertyResolver [[ResourceProperties.PropertyResolver.html]] {
            {abstract} +resolveProperty(Id, Key<T>): T
        }

        interface RequestContext [[RequestContext.html]] {
            {abstract} +setContent(Content): void
            {abstract} +getContentProperties(): ResourceProperties
            {abstract} +clearContent(): void
            {abstract} +setNode(SiteNode): void
            {abstract} +getNodeProperties(): ResourceProperties
            {abstract} +clearNode(): void
            {abstract} +setDynamicNodeProperty(Key<T>, T): void
        }

        interface Media [[Media.html]] {
            {static} +_Media_: Class<Media>
        }

        class it.tidalwave.northernwind.core.model.Media.Builder [[Media.Builder.html]] {
            +build(): Media
        }

        interface it.tidalwave.northernwind.core.model.Media.Builder.CallBack [[Media.Builder.CallBack.html]] {
            {abstract} +build(Builder): Media
        }

        interface Site [[Site.html]] {
            {abstract} +getContextPath(): String
            {abstract} +createLink(ResourcePath): String
            {abstract} +find(Class<TYPE>): SiteFinder<TYPE>
            {abstract} +getFileSystemProvider(): ResourceFileSystemProvider
            {abstract} +getConfiguredLocales(): List<Locale>
            {abstract} +getTemplate(Class<?>, Optional<ResourcePath>, String): Template
            {abstract} +--getTemplate--(Class<?>, ResourcePath): Optional<String>
        }

        class it.tidalwave.northernwind.core.model.Site.Builder [[Site.Builder.html]] {
            +build(): Site
        }

        interface it.tidalwave.northernwind.core.model.Site.Builder.CallBack [[Site.Builder.CallBack.html]] {
            {abstract} +build(Builder): Site
        }

        interface SiteProvider [[SiteProvider.html]] {
            {static} +_SiteProvider_: Class<SiteProvider>
            {abstract} +getSite(): Site
            {abstract} +reload(): void
            {abstract} +isSiteAvailable(): boolean
            {abstract} +getVersionString(): String
        }

        interface RequestProcessor [[RequestProcessor.html]] {
            {abstract} +process(Request): Status
        }

        enum it.tidalwave.northernwind.core.model.RequestProcessor.Status [[RequestProcessor.Status.html]] {
            {static} +CONTINUE
            {static} +BREAK
        }

        interface Request [[Request.html]] {
            {abstract} +withRelativeUri(String): Request
            {abstract} +getBaseUrl(): String
            {abstract} +getRelativeUri(): String
            {abstract} +getOriginalRelativeUri(): String
            {abstract} +getPreferredLocales(): List<Locale>
            {abstract} +getHeader(String): Optional<String>
            {abstract} +getMultiValuedHeader(String): List<String>
            {abstract} +getParameter(String): Optional<String>
            {abstract} +getMultiValuedParameter(String): List<String>
            {abstract} +getPathParams(SiteNode): ResourcePath
        }

        interface Content [[Content.html]] {
            {static} +_Content_: Class<Content>
            {static} +P_TITLE: Key<String>
            {static} +P_ID: Key<String>
            {static} +P_FULL_TEXT: Key<String>
            {static} +P_LEADIN_TEXT: Key<String>
            {static} +P_DESCRIPTION: Key<String>
            {static} +P_TEMPLATE: Key<String>
            {static} +P_CREATION_DATE: Key<ZonedDateTime>
            {static} +P_LATEST_MODIFICATION_DATE: Key<ZonedDateTime>
            {static} +P_PUBLISHING_DATE: Key<ZonedDateTime>
            {static} +P_TAGS: Key<List<String>>
            {abstract} +getExposedUri(): Optional<ResourcePath>
        }

        class it.tidalwave.northernwind.core.model.Content.Builder [[Content.Builder.html]] {
            +build(): Content
        }

        interface it.tidalwave.northernwind.core.model.Content.Builder.CallBack [[Content.Builder.CallBack.html]] {
            {abstract} +build(Builder): Content
        }

        class HttpStatusException [[HttpStatusException.html]] {
            +HttpStatusException(int)
            {static} +temporaryRedirect(Site, String): HttpStatusException
            {static} +permanentRedirect(Site, String): HttpStatusException
            +withHeader(String, String): HttpStatusException
            +isError(): boolean
        }

        interface SiteFinder<T> [[SiteFinder.html]] {
            {abstract} +withRelativePath(String): SiteFinder<T>
            {abstract} +withRelativeUri(String): SiteFinder<T>
            +withRelativePath(ResourcePath): SiteFinder<T>
            +withRelativeUri(ResourcePath): SiteFinder<T>
        }

        interface MimeTypeResolver [[MimeTypeResolver.html]] {
            {abstract} +getMimeType(String): String
        }

        interface ResourceFileSystemProvider [[ResourceFileSystemProvider.html]] {
            {abstract} +getFileSystem(): ResourceFileSystem
        }

        it.tidalwave.util.As <|-- ResourceFileSystem
        java.io.Serializable <|.. ResourcePath
        ResourcePath --> ResourcePath: EMPTY
        it.tidalwave.util.As <|-- ResourceFile
        it.tidalwave.role.Composite <|-- ResourceFile
        it.tidalwave.util.spi.ExtendedFinderSupport <|-- it.tidalwave.northernwind.core.model.ResourceFile.Finder
        ResourceFile +-- it.tidalwave.northernwind.core.model.ResourceFile.Finder
        Resource <|-- SiteNode
        it.tidalwave.role.SimpleComposite <|-- SiteNode
        Template +-- it.tidalwave.northernwind.core.model.Template.Aggregate
        java.lang.Iterable <|.. it.tidalwave.northernwind.core.model.Template.Aggregates
        Template +-- it.tidalwave.northernwind.core.model.Template.Aggregates
        it.tidalwave.northernwind.core.model.Template.Aggregates --> "*" it.tidalwave.northernwind.core.model.Template.Aggregate: EMPTY
        it.tidalwave.util.As <|-- Resource
        Resource +-- it.tidalwave.northernwind.core.model.Resource.Builder
        it.tidalwave.northernwind.core.model.Resource.Builder +-- it.tidalwave.northernwind.core.model.Resource.Builder.CallBack
        it.tidalwave.util.As <|-- ResourceProperties
        it.tidalwave.role.Identifiable <|-- ResourceProperties
        ResourceProperties +-- it.tidalwave.northernwind.core.model.ResourceProperties.Builder
        it.tidalwave.northernwind.core.model.ResourceProperties.Builder +-- it.tidalwave.northernwind.core.model.ResourceProperties.Builder.CallBack
        ResourceProperties +-- it.tidalwave.northernwind.core.model.ResourceProperties.PropertyResolver
        it.tidalwave.northernwind.core.model.ResourceProperties.PropertyResolver --> it.tidalwave.northernwind.core.model.ResourceProperties.PropertyResolver: DEFAULT
        it.tidalwave.northernwind.core.model.spi.RequestResettable <|-- RequestContext
        Resource <|-- Media
        Media +-- it.tidalwave.northernwind.core.model.Media.Builder
        it.tidalwave.northernwind.core.model.Media.Builder +-- it.tidalwave.northernwind.core.model.Media.Builder.CallBack
        Site +-- it.tidalwave.northernwind.core.model.Site.Builder
        it.tidalwave.northernwind.core.model.Site.Builder +-- it.tidalwave.northernwind.core.model.Site.Builder.CallBack
        RequestProcessor +-- it.tidalwave.northernwind.core.model.RequestProcessor.Status
        Resource <|-- Content
        it.tidalwave.role.SimpleComposite <|-- Content
        Content +-- it.tidalwave.northernwind.core.model.Content.Builder
        it.tidalwave.northernwind.core.model.Content.Builder +-- it.tidalwave.northernwind.core.model.Content.Builder.CallBack
        java.lang.Exception <|-- HttpStatusException
        it.tidalwave.util.spi.ExtendedFinderSupport <|-- SiteFinder
    }

    namespace it.tidalwave.util {
        interface As {
            {static} +forObject(Object): As
            {static} +forObject(Object, Object): As
            {static} +forObject(Object, Collection<Object>): As
            +as(Class<T>): T
            +--as--(Class<T>, NotFoundBehaviour<T>): T
            {abstract} +maybeAs(Class<T>): Optional<T>
            {abstract} +asMany(Class<T>): Collection<T>
            {static} +type(Class<?>): Type<T>
            +as(Type<T>): T
            +maybeAs(Type<T>): Optional<T>
            +asMany(Type<T>): Collection<T>
        }
    }

    namespace java.io {
        interface Serializable
    }

    namespace it.tidalwave.role {
        interface Composite<TYPE, SPECIALIZED_FINDER extends Finder<? extends TYPE>> {
            {static} +_Composite_: Class<Composite>
            {static} +DEFAULT: Composite<Object, Finder<Object>>
            {abstract} +findChildren(): SPECIALIZED_FINDER extends Finder<? extends TYPE>
        }
        interface SimpleComposite<TYPE> {
            {static} +_SimpleComposite_: Class<SimpleComposite>
            {static} +of(Finder<TYPE>): SimpleComposite<TYPE>
            {static} +ofCloned(Collection<TYPE>): SimpleComposite<TYPE>
        }
        interface Identifiable {
            {static} +_Identifiable_: Class<Identifiable>
            {abstract} +getId(): Id
            {static} +of(Id): Identifiable
        }
    }

    namespace it.tidalwave.util.spi {
        interface ExtendedFinderSupport<TYPE, EXTENDED_FINDER extends Finder<TYPE>> {
            {abstract} +from(int): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +max(int): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +sort(SortCriterion): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +sort(SortCriterion, SortDirection): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +withContext(Object): EXTENDED_FINDER extends Finder<TYPE>
        }
    }

    namespace java.lang {
        interface Iterable<T> {
            {abstract} +iterator(): Iterator<T>
            +forEach(Consumer<? super T>): void
            +spliterator(): Spliterator<T>
        }
        class Exception {
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
