@startuml
    namespace it.tidalwave.northernwind.frontend.filesystem.scm.spi {

        abstract class ScmPreparer [[ScmPreparer.html]] {
            {static} +REPOSITORY_FOLDER: Path
            {static} +createTagNames(int): List<String>
            +prepareAtTag(Tag): void
            {abstract} #stripChangesetsAfter(String): void
        }

        class ScmWorkingDirectoryTestSupport [[ScmWorkingDirectoryTestSupport.html]] {
            +createSourceRepository(): void
            +setup(): void
            +must_properly_clone_a_repository(): void
            +must_properly_enumerate_tags(String, List<Tag>): void
            +must_return_no_tag_when_empty_working_directory(): void
            +must_properly_checkout(Tag): void
            +must_throw_exception_when_try_to_update_to_an_invalid_tag(Tag): void
            +must_properly_fetch_changesets(String, List<Tag>): void
            +changesets(): Object[]
            +tagSequenceUpTo0_8(): Object[]
            +invalidTags(): Object[]
        }

        class ScmFileSystemProviderTest [[ScmFileSystemProviderTest.html]]

        class MockScmWorkingDirectory [[MockScmWorkingDirectory.html]] {
            {static} +MOCKSCM: String
            +MockScmWorkingDirectory(Path)
            +checkOut(Tag): void
            +listTags(): List<String>
            +cloneFrom(URI): void
            +fetchChangesets(): void
        }

        class it.tidalwave.northernwind.frontend.filesystem.scm.spi.MockScmWorkingDirectory.Preparer [[MockScmWorkingDirectory.Preparer.html]] {
            #stripChangesetsAfter(String): void
        }

        class ScmFileSystemProviderTestSupport [[ScmFileSystemProviderTestSupport.html]] {
            +setup(): void
            +must_properly_initialize(): void
            +checkForUpdates_must_do_nothing_when_there_are_no_updates(): void
            +checkForUpdates_must_update_and_fire_event_when_there_are_updates(): void
            #populateWorkingDirectory(Path, Tag): void
        }

        class MockScmFileSystemProvider [[MockScmFileSystemProvider.html]] {
            +createWorkingDirectory(Path): ScmWorkingDirectory
        }

        class ScmWorkingDirectoryTest [[ScmWorkingDirectoryTest.html]]

        class ResourceFileSystemChangedEventMatcher [[ResourceFileSystemChangedEventMatcher.html]] {
            +matches(ResourceFileSystemChangedEvent): boolean
        }

        ScmPreparer --> Tag: TAG_PUBLISHED_0_8\nTAG_PUBLISHED_0_9
        ScmPreparer --> "*" Tag: ALL_TAGS_UP_TO_PUBLISHED_0_8\nALL_TAGS_UP_TO_PUBLISHED_0_9
        ScmFileSystemProviderTestSupport <|-- ScmFileSystemProviderTest
        ScmWorkingDirectorySupport <|-- MockScmWorkingDirectory
        MockScmWorkingDirectory --> "0..1" Tag: currentTag
        ScmPreparer <|-- it.tidalwave.northernwind.frontend.filesystem.scm.spi.MockScmWorkingDirectory.Preparer
        MockScmWorkingDirectory +-- it.tidalwave.northernwind.frontend.filesystem.scm.spi.MockScmWorkingDirectory.Preparer
        ScmFileSystemProvider <|-- MockScmFileSystemProvider
        ScmWorkingDirectoryTestSupport <|-- ScmWorkingDirectoryTest
        org.mockito.ArgumentMatcher <|.. ResourceFileSystemChangedEventMatcher
    }

    namespace org.mockito {
        interface ArgumentMatcher<T> {
            {abstract} +matches(T): boolean
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
