@startuml
    namespace it.tidalwave.northernwind.frontend.springmvc {

        class SpringMvcRestController [[SpringMvcRestController.html]] {
            +get(HttpServletRequest): ResponseEntity<?>
        }

        class SpringMvcResponseHolder [[SpringMvcResponseHolder.html]] {
            +response(): SpringMvcResponseBuilder
        }

        class SpringMvcResponseBuilder [[SpringMvcResponseBuilder.html]] {
            +withHeader(String, String): ResponseBuilder
            #getHeader(String): Optional<String>
            +withContentType(String): ResponseBuilder
            +withContentLength(long): ResponseBuilder
            #doBuild(): ResponseEntity<?>
        }

        class ThreadNameChangerAspect [[ThreadNameChangerAspect.html]] {
            +advice(ProceedingJoinPoint): Object
        }

        it.tidalwave.northernwind.core.model.spi.ResponseHolder <|-- SpringMvcResponseHolder
        it.tidalwave.northernwind.core.model.spi.ResponseBuilderSupport <|-- SpringMvcResponseBuilder
    }

    namespace it.tidalwave.northernwind.core.model.spi {
        abstract class ResponseHolder<RESPONSE_TYPE> {
            {abstract} +response(): ResponseBuilder<RESPONSE_TYPE>
        }
        abstract class ResponseBuilderSupport<RESPONSE_TYPE> {
            {static} #HEADER_CONTENT_LENGTH: String
            {static} #HEADER_ETAG: String
            {static} #HEADER_CONTENT_TYPE: String
            {static} #HEADER_CONTENT_DISPOSITION: String
            {static} #HEADER_LAST_MODIFIED: String
            {static} #HEADER_EXPIRES: String
            {static} #HEADER_LOCATION: String
            {static} #HEADER_IF_MODIFIED_SINCE: String
            {static} #HEADER_IF_NONE_MATCH: String
            {static} #HEADER_CACHE_CONTROL: String
            {static} #PATTERN_RFC1123: String
            #body: Object
            #httpStatus: int
            #requestIfNoneMatch: Optional<String>
            #requestIfModifiedSince: Optional<ZonedDateTime>
            {abstract} +withHeader(String, String): ResponseBuilder<RESPONSE_TYPE>
            {abstract} #doBuild(): RESPONSE_TYPE
            {abstract} #getHeader(String): Optional<String>
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
