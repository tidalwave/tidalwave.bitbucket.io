@startuml
    namespace it.tidalwave.northernwind.core.model.spi {

        abstract class ResourceSupport [[ResourceSupport.html]] {
            #modelFactory: ModelFactory
            +ResourceSupport(Builder)
            +getPropertyGroup(Id): ResourceProperties
        }

        class ResourceFileFinderSupport [[ResourceFileFinderSupport.html]] {
            +ResourceFileFinderSupport(ResourceFileFinderSupport, Object)
            {static} +withComputeResults(Function<Finder, List<ResourceFile>>): Finder
            {static} +withComputeResults(String, Function<Finder, List<ResourceFile>>): Finder
            +withRecursion(boolean): Finder
            +withName(String): Finder
            #computeResults(): List<? extends ResourceFile>
        }

        abstract class MediaSupport [[MediaSupport.html]] {
            #modelFactory: ModelFactory
            +MediaSupport(Builder)
        }

        abstract class SiteNodeSupport [[SiteNodeSupport.html]] {
            #modelFactory: ModelFactory
            +SiteNodeSupport(ModelFactory, ResourceFile)
        }

        interface DecoratedResourceFileSystem [[DecoratedResourceFileSystem.html]] {
            {abstract} +createDecoratorFile(ResourceFile): ResourceFile
        }

        class RequestHolder [[RequestHolder.html]] {
            +requestReset(): void
            +set(Request): void
            +get(): Request
        }

        interface RequestResettable [[RequestResettable.html]] {
            {abstract} +requestReset(): void
        }

        abstract class ResponseBuilderSupport<RESPONSE_TYPE> [[ResponseBuilderSupport.html]] {
            {static} #HEADER_CONTENT_LENGTH: String
            {static} #HEADER_ETAG: String
            {static} #HEADER_CONTENT_TYPE: String
            {static} #HEADER_CONTENT_DISPOSITION: String
            {static} #HEADER_LAST_MODIFIED: String
            {static} #HEADER_EXPIRES: String
            {static} #HEADER_LOCATION: String
            {static} #HEADER_IF_MODIFIED_SINCE: String
            {static} #HEADER_IF_NONE_MATCH: String
            {static} #HEADER_CACHE_CONTROL: String
            {static} #PATTERN_RFC1123: String
            #body: Object
            #httpStatus: int
            #requestIfNoneMatch: Optional<String>
            #requestIfModifiedSince: Optional<ZonedDateTime>
            {abstract} +withHeader(String, String): ResponseBuilder<RESPONSE_TYPE>
            +withHeaders(Map<String, String>): ResponseBuilder<RESPONSE_TYPE>
            +withContentType(String): ResponseBuilder<RESPONSE_TYPE>
            +withContentLength(long): ResponseBuilder<RESPONSE_TYPE>
            +withContentDisposition(String): ResponseBuilder<RESPONSE_TYPE>
            +withExpirationTime(Duration): ResponseBuilder<RESPONSE_TYPE>
            +withLatestModifiedTime(ZonedDateTime): ResponseBuilder<RESPONSE_TYPE>
            +withBody(Object): ResponseBuilder<RESPONSE_TYPE>
            +fromFile(ResourceFile): ResponseBuilder<RESPONSE_TYPE>
            +forRequest(Request): ResponseBuilder<RESPONSE_TYPE>
            +forException(NotFoundException): ResponseBuilder<RESPONSE_TYPE>
            +forException(Throwable): ResponseBuilder<RESPONSE_TYPE>
            +forException(HttpStatusException): ResponseBuilder<RESPONSE_TYPE>
            +withStatus(int): ResponseBuilder<RESPONSE_TYPE>
            +permanentRedirect(String): ResponseBuilder<RESPONSE_TYPE>
            +build(): RESPONSE_TYPE
            +put(): void
            {abstract} #doBuild(): RESPONSE_TYPE
            {abstract} #getHeader(String): Optional<String>
            #getDateTimeHeader(String): Optional<ZonedDateTime>
            #cacheSupport(): ResponseBuilder<RESPONSE_TYPE>
        }

        interface LinkPostProcessor [[LinkPostProcessor.html]] {
            {abstract} +postProcess(String): String
        }

        abstract class ContentSupport [[ContentSupport.html]] {
            #modelFactory: ModelFactory
            +ContentSupport(Builder)
        }

        class ModelFactorySupport [[ModelFactorySupport.html]] {
            +createResource(): Builder
            +createContent(): Builder
            +createSiteNode(Site, ResourceFile): SiteNode
            +createMedia(): Builder
            +createRequest(): Request
            +createRequestFrom(HttpServletRequest): Request
            +createLayout(): Builder
            +createProperties(): Builder
            +createSite(): Builder
            +build(Builder): ResourceProperties
            +build(Builder): Site
            +build(Builder): Layout
            +build(Builder): Resource
            +build(Builder): Content
            +build(Builder): Media
        }

        abstract class ResponseHolder<RESPONSE_TYPE> [[ResponseHolder.html]] {
            {abstract} +response(): ResponseBuilder<RESPONSE_TYPE>
            +get(): RESPONSE_TYPE
            +requestReset(): void
        }

        abstract class DecoratedResourceFileSupport [[DecoratedResourceFileSupport.html]] {
            +delete(): void
            +getParent(): ResourceFile
            +createFolder(String): ResourceFile
            +findChildren(): Finder
        }

        interface ResponseBuilder<RESPONSE_TYPE> [[ResponseBuilder.html]] {
            {abstract} +withHeader(String, String): ResponseBuilder<RESPONSE_TYPE>
            {abstract} +withHeaders(Map<String, String>): ResponseBuilder<RESPONSE_TYPE>
            {abstract} +withContentType(String): ResponseBuilder<RESPONSE_TYPE>
            {abstract} +withContentLength(long): ResponseBuilder<RESPONSE_TYPE>
            {abstract} +withContentDisposition(String): ResponseBuilder<RESPONSE_TYPE>
            {abstract} +withExpirationTime(Duration): ResponseBuilder<RESPONSE_TYPE>
            {abstract} +withLatestModifiedTime(ZonedDateTime): ResponseBuilder<RESPONSE_TYPE>
            {abstract} +withBody(Object): ResponseBuilder<RESPONSE_TYPE>
            {abstract} +fromFile(ResourceFile): ResponseBuilder<RESPONSE_TYPE>
            {abstract} +forRequest(Request): ResponseBuilder<RESPONSE_TYPE>
            {abstract} +forException(NotFoundException): ResponseBuilder<RESPONSE_TYPE>
            {abstract} +forException(HttpStatusException): ResponseBuilder<RESPONSE_TYPE>
            {abstract} +forException(Throwable): ResponseBuilder<RESPONSE_TYPE>
            {abstract} +withStatus(int): ResponseBuilder<RESPONSE_TYPE>
            {abstract} +permanentRedirect(String): ResponseBuilder<RESPONSE_TYPE>
            {abstract} +build(): RESPONSE_TYPE
            {abstract} +put(): void
        }

        class ParameterLanguageOverrideLinkPostProcessor [[ParameterLanguageOverrideLinkPostProcessor.html]] {
            +postProcess(String): String
            +postProcess(String, String): String
        }

        class ParameterLanguageOverrideRequestProcessor [[ParameterLanguageOverrideRequestProcessor.html]] {
            +process(Request): Status
            +getParameterValue(): Optional<String>
            +requestReset(): void
        }

        class AvailabilityEnforcerRequestProcessor [[AvailabilityEnforcerRequestProcessor.html]] {
            +process(Request): Status
        }

        class DefaultRedirectProcessor [[DefaultRedirectProcessor.html]] {
            +process(Request): Status
        }

        class DefaultMediaRequestProcessor<ResponseType> [[DefaultMediaRequestProcessor.html]] {
            +process(Request): Status
        }

        class HeaderLanguageOverrideRequestProcessor [[HeaderLanguageOverrideRequestProcessor.html]] {
            +process(Request): Status
        }

        class DefaultLibraryRequestProcessor [[DefaultLibraryRequestProcessor.html]] {
            +process(Request): Status
        }

        class SiteResetterOnFileSystemChange [[SiteResetterOnFileSystemChange.html]] {
        }

        class DefaultContentRequestProcessor [[DefaultContentRequestProcessor.html]] {
            +process(Request): Status
        }

        it.tidalwave.northernwind.core.model.Resource <|.. ResourceSupport
        it.tidalwave.util.spi.FinderSupport <|-- ResourceFileFinderSupport
        it.tidalwave.northernwind.core.model.ResourceFile.Finder <|.. ResourceFileFinderSupport
        it.tidalwave.northernwind.core.model.Media <|.. MediaSupport
        it.tidalwave.northernwind.core.model.SiteNode <|.. SiteNodeSupport
        it.tidalwave.northernwind.core.model.ResourceFileSystem <|-- DecoratedResourceFileSystem
        RequestResettable <|.. RequestHolder
        ResponseBuilder <|.. ResponseBuilderSupport
        it.tidalwave.northernwind.core.model.Content <|.. ContentSupport
        it.tidalwave.northernwind.core.model.ModelFactory <|.. ModelFactorySupport
        it.tidalwave.northernwind.core.model.Resource.Builder.CallBack <|.. ModelFactorySupport
        it.tidalwave.northernwind.core.model.Content.Builder.CallBack <|.. ModelFactorySupport
        it.tidalwave.northernwind.core.model.Media.Builder.CallBack <|.. ModelFactorySupport
        it.tidalwave.northernwind.core.model.ResourceProperties.Builder.CallBack <|.. ModelFactorySupport
        it.tidalwave.northernwind.core.model.Site.Builder.CallBack <|.. ModelFactorySupport
        it.tidalwave.northernwind.frontend.ui.Layout.Builder.CallBack <|.. ModelFactorySupport
        RequestResettable <|.. ResponseHolder
        it.tidalwave.northernwind.core.model.ResourceFile <|.. DecoratedResourceFileSupport
        DecoratedResourceFileSupport --> DecoratedResourceFileSystem: fileSystem
        LinkPostProcessor <|.. ParameterLanguageOverrideLinkPostProcessor
        it.tidalwave.northernwind.core.model.RequestProcessor <|.. ParameterLanguageOverrideRequestProcessor
        RequestResettable <|.. ParameterLanguageOverrideRequestProcessor
        it.tidalwave.northernwind.core.model.RequestProcessor <|.. AvailabilityEnforcerRequestProcessor
        it.tidalwave.northernwind.core.model.RequestProcessor <|.. DefaultRedirectProcessor
        it.tidalwave.northernwind.core.model.RequestProcessor <|.. DefaultMediaRequestProcessor
        DefaultMediaRequestProcessor --> ResponseHolder: responseHolder
        it.tidalwave.northernwind.core.model.RequestProcessor <|.. HeaderLanguageOverrideRequestProcessor
        it.tidalwave.northernwind.core.model.RequestProcessor <|.. DefaultLibraryRequestProcessor
        it.tidalwave.northernwind.core.model.RequestProcessor <|.. DefaultContentRequestProcessor
    }

    namespace it.tidalwave.northernwind.core.model {
        interface Resource [[../Resource.html]] {
            {static} +_Resource_: Class<Resource>
            {static} +P_EXPOSED_URI: Key<String>
            {static} +P_PLACE_HOLDER: Key<Boolean>
            {abstract} +getFile(): ResourceFile
            {abstract} +getProperties(): ResourceProperties
            +getProperty(Key<T>): Optional<T>
            +getProperty(List<Key<T>>): Optional<T>
            {abstract} +getPropertyGroup(Id): ResourceProperties
            {abstract} +isPlaceHolder(): boolean
        }
        interface it.tidalwave.northernwind.core.model.ResourceFile.Finder [[../ResourceFile.Finder.html]] {
            {abstract} +withRecursion(boolean): Finder
            {abstract} +withName(String): Finder
            {abstract} +getName(): String
            {abstract} +isRecursive(): boolean
        }
        interface Media [[../Media.html]] {
            {static} +_Media_: Class<Media>
        }
        interface SiteNode [[../SiteNode.html]] {
            {static} +_SiteNode_: Class<SiteNode>
            {static} +P_NAVIGATION_LABEL: Key<String>
            {static} +P_MANAGES_PATH_PARAMS: Key<Boolean>
            {abstract} +getSite(): Site
            {abstract} +getLayout(): Layout
            {abstract} +getRelativeUri(): ResourcePath
        }
        interface ResourceFileSystem [[../ResourceFileSystem.html]] {
            {abstract} +getRoot(): ResourceFile
            {abstract} +findFileByPath(String): ResourceFile
        }
        interface Content [[../Content.html]] {
            {static} +_Content_: Class<Content>
            {static} +P_TITLE: Key<String>
            {static} +P_ID: Key<String>
            {static} +P_FULL_TEXT: Key<String>
            {static} +P_LEADIN_TEXT: Key<String>
            {static} +P_DESCRIPTION: Key<String>
            {static} +P_TEMPLATE: Key<String>
            {static} +P_CREATION_DATE: Key<ZonedDateTime>
            {static} +P_LATEST_MODIFICATION_DATE: Key<ZonedDateTime>
            {static} +P_PUBLISHING_DATE: Key<ZonedDateTime>
            {static} +P_TAGS: Key<List<String>>
            {abstract} +getExposedUri(): Optional<ResourcePath>
        }
        interface ModelFactory [[../ModelFactory.html]] {
            {abstract} +createResource(): Builder
            {abstract} +createContent(): Builder
            {abstract} +createMedia(): Builder
            {abstract} +createSiteNode(Site, ResourceFile): SiteNode
            {abstract} +createLayout(): Builder
            {abstract} +createRequest(): Request
            {abstract} +createRequestFrom(HttpServletRequest): Request
            {abstract} +createProperties(): Builder
            {abstract} +createSite(): Builder
        }
        interface it.tidalwave.northernwind.core.model.Resource.Builder.CallBack [[../Resource.Builder.CallBack.html]] {
            {abstract} +build(Builder): Resource
        }
        interface it.tidalwave.northernwind.core.model.Content.Builder.CallBack [[../Content.Builder.CallBack.html]] {
            {abstract} +build(Builder): Content
        }
        interface it.tidalwave.northernwind.core.model.Media.Builder.CallBack [[../Media.Builder.CallBack.html]] {
            {abstract} +build(Builder): Media
        }
        interface it.tidalwave.northernwind.core.model.ResourceProperties.Builder.CallBack [[../ResourceProperties.Builder.CallBack.html]] {
            {abstract} +build(Builder): ResourceProperties
        }
        interface it.tidalwave.northernwind.core.model.Site.Builder.CallBack [[../Site.Builder.CallBack.html]] {
            {abstract} +build(Builder): Site
        }
        interface ResourceFile [[../ResourceFile.html]] {
            {abstract} +getFileSystem(): ResourceFileSystem
            {abstract} +getName(): String
            {abstract} +getPath(): ResourcePath
            {abstract} +isFolder(): boolean
            {abstract} +isData(): boolean
            {abstract} +getMimeType(): String
            {abstract} +getInputStream(): InputStream
            {abstract} +asText(String): String
            {abstract} +asBytes(): byte[]
            {abstract} +getLatestModificationTime(): ZonedDateTime
            {abstract} +getParent(): ResourceFile
            {abstract} +toFile(): File
            {abstract} +delete(): void
            {abstract} +createFolder(String): ResourceFile
            {abstract} +copyTo(ResourceFile): void
        }
        interface RequestProcessor [[../RequestProcessor.html]] {
            {abstract} +process(Request): Status
        }
    }

    namespace it.tidalwave.util.spi {
        class FinderSupport<TYPE, EXTENDED_FINDER extends Finder<TYPE>> {
        }
    }

    namespace it.tidalwave.northernwind.frontend.ui {
        interface it.tidalwave.northernwind.frontend.ui.Layout.Builder.CallBack [[../../../frontend/ui/Layout.Builder.CallBack.html]] {
            {abstract} +build(Builder): Layout
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
