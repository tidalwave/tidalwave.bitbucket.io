@startuml
    namespace it.tidalwave.northernwind.frontend.ui {

        interface ViewController [[ViewController.html]] {
            +initialize(): void
            +prepareRendering(RenderContext): void
            +renderView(RenderContext): void
            +findVirtualSiteNodes(): Finder<SiteNode>
        }

        interface SiteViewController [[SiteViewController.html]] {
            {abstract} +processRequest(Request): ResponseType
        }

        interface ViewFactory [[ViewFactory.html]] {
            {abstract} +createViewAndController(String, Id, SiteNode): ViewAndController
        }

        class it.tidalwave.northernwind.frontend.ui.ViewFactory.ViewAndController [[ViewFactory.ViewAndController.html]] {
            +renderView(RenderContext): Object
        }

        interface SiteView [[SiteView.html]] {
            {static} +NW: String
            {abstract} +renderSiteNode(Request, SiteNode): void
        }

        interface LayoutFinder [[LayoutFinder.html]] {
            {abstract} +withId(Id): LayoutFinder
        }

        interface Layout [[Layout.html]] {
            {abstract} +getTypeUri(): String
            {abstract} +withChild(Layout): Layout
            {abstract} +withOverride(Layout): Layout
            {abstract} +accept(Visitor<Layout, T>): Optional<T>
            {abstract} +createViewAndController(SiteNode): ViewAndController
        }

        class it.tidalwave.northernwind.frontend.ui.Layout.Builder [[Layout.Builder.html]] {
            +build(): Layout
        }

        interface it.tidalwave.northernwind.frontend.ui.Layout.Builder.CallBack [[Layout.Builder.CallBack.html]] {
            {abstract} +build(Builder): Layout
        }

        interface RenderContext [[RenderContext.html]] {
            {abstract} +getRequest(): Request
            {abstract} +getRequestContext(): RequestContext
            +setDynamicNodeProperty(Key<T>, T): void
            +getQueryParam(String): Optional<String>
            +getPathParams(SiteNode): ResourcePath
        }

        ViewFactory +-- it.tidalwave.northernwind.frontend.ui.ViewFactory.ViewAndController
        it.tidalwave.util.spi.ExtendedFinderSupport <|-- LayoutFinder
        it.tidalwave.util.As <|-- Layout
        it.tidalwave.role.Identifiable <|-- Layout
        it.tidalwave.role.Composite <|-- Layout
        Layout +-- it.tidalwave.northernwind.frontend.ui.Layout.Builder
        it.tidalwave.northernwind.frontend.ui.Layout.Builder +-- it.tidalwave.northernwind.frontend.ui.Layout.Builder.CallBack
    }

    namespace it.tidalwave.util.spi {
        interface ExtendedFinderSupport<TYPE, EXTENDED_FINDER extends Finder<TYPE>> {
            {abstract} +from(int): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +max(int): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +withContext(Object): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +sort(SortCriterion): EXTENDED_FINDER extends Finder<TYPE>
            {abstract} +sort(SortCriterion, SortDirection): EXTENDED_FINDER extends Finder<TYPE>
        }
    }

    namespace it.tidalwave.util {
        interface As {
            {abstract} +as(Class<T>): T
            {abstract} +as(Class<T>, NotFoundBehaviour<T>): T
            +maybeAs(Class<T>): Optional<T>
            +--asOptional--(Class<T>): Optional<T>
            {abstract} +asMany(Class<T>): Collection<T>
        }
    }

    namespace it.tidalwave.role {
        interface Identifiable {
            {static} +_Identifiable_: Class<Identifiable>
            {abstract} +getId(): Id
            {static} +of(Id): Identifiable
        }
        interface Composite<TYPE, SPECIALIZED_FINDER extends Finder<? extends TYPE>> {
            {static} +_Composite_: Class<Composite>
            {static} +DEFAULT: Composite<Object, Finder<Object>>
            {abstract} +findChildren(): SPECIALIZED_FINDER extends Finder<? extends TYPE>
        }
    }

    center footer UMLDoclet 2.0.12, PlantUML 1.2020.16
@enduml
